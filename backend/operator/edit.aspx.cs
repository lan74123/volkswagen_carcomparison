﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_operator_edit : BackendBasePage
{
    public string html_panelTitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Request.Params["serno"]))
            {
                //---------------------------------------------------------新增
                html_panelTitle = "新增";
                DetailsView1.ChangeMode(DetailsViewMode.Insert);
            }
            else
            {
                //---------------------------------------------------------編輯
                html_panelTitle = "編輯";
                DetailsView1.ChangeMode(DetailsViewMode.Edit);
            }
        }
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("list.aspx");
    }

    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;

            //控制項
            Backend_DBOperator DBOperator = new Backend_DBOperator();
            TextBox txtpassword = (TextBox)DetailsView1.FindControl("password");
            CheckBox cbvalid = (CheckBox)DetailsView1.FindControl("valid");

            //變數
            string in_name = ValidateHelper.FilteXSSValue(e.NewValues["name"] as string);
            string in_password = ValidateHelper.FilteXSSValue(txtpassword.Text);
            string in_valid = cbvalid.Checked ? "1" : "0";

            //檢查
            //必填欄位檢查
            if (string.IsNullOrEmpty(in_name))
            {
                sys_msg += "請輸入「操作員姓名」!\\r";
            }
            if (string.IsNullOrEmpty(in_password))
            {
                sys_msg += "請輸入「密碼」!\\r";
            }

            //提交
            if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
                e.NewValues["password"] = ValidateHelper.EncryptString(in_password);
                e.NewValues["valid"] = in_valid;
            }
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInEditMode = true;
            JSOutPut("修改成功!");
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;
            Backend_DBOperator DBOperator = new Backend_DBOperator();

            //控制項
            TextBox txtpassword = (TextBox)DetailsView1.FindControl("password");
            CheckBox cbvalid = (CheckBox)DetailsView1.FindControl("valid");

            //變數
            string in_name = ValidateHelper.FilteXSSValue(e.Values["name"] as string);
            string in_username = ValidateHelper.FilteXSSValue(e.Values["username"] as string);
            string in_password = ValidateHelper.FilteXSSValue(txtpassword.Text);
            string in_valid = cbvalid.Checked ? "1" : "0";

            //檢查
            //必填欄位檢查
            if (string.IsNullOrEmpty(in_name))
            {
                sys_msg += "請輸入「操作員姓名」!\\r";
            }
            if (string.IsNullOrEmpty(in_username))
            {
                sys_msg += "請輸入「帳號」!\\r";
            }
            else
            {
                if (DBOperator.IsExistAccount(in_username, "0")) 
                {
                    sys_msg += "此「帳號」已經存在，請重新輸入!\\r";
                }
            }

            if (string.IsNullOrEmpty(in_password))
            {
                sys_msg += "請輸入「密碼」!\\r";
            }

            //提交
            if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
                e.Values["password"] = ValidateHelper.EncryptString(in_password);
                e.Values["valid"] = in_valid;
            }
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInInsertMode = true;
            JSOutPutAndRedirect("新增成功!", "list.aspx");
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        //因為textmode為password時，資料沒辦法bind，所以靠一個隱藏的塞值
        if (DetailsView1.CurrentMode == DetailsViewMode.Edit)
        {
            TextBox txtpass = (TextBox)DetailsView1.FindControl("pass");
            ((TextBox)DetailsView1.FindControl("password")).Attributes["value"] = txtpass.Text;
        }
    }
    
}