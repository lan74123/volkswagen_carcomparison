﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_operatorgroup_list : BackendBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnInsert_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("edit.aspx");
    }

    protected void GridView1_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
    {
        if (!IsRefresh)
        {
            Backend_DBOperator DBOperator = new Backend_DBOperator();

            string Serno = GridView1.Rows[e.RowIndex].Cells[0].Text;
            bool vaild = false;
            vaild = DBOperator.IsValid(Serno);
            if (vaild == false)
            {
                JSOutPut("此筆資料早已為無效");
            }
            qValid.SelectedValue = "1";
        }
    }

    protected void GridView1_PreRender(object sender, EventArgs e)
    {
        //為了產生 Table 的 thead
        GridView1.UseAccessibleHeader = true;
        if (GridView1.HeaderRow != null)
        {
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            // 取得控制項
            GridView gv = sender as GridView;
            PlaceHolder placeholder = e.Row.FindControl("placeholderPager") as PlaceHolder;

            //一次要顯示的頁數
            int showRange = 5;
            //分頁的數量
            int pageCount = gv.PageCount;
            //目前頁碼 (起始值為0)
            int pageIndex = gv.PageIndex;
            //顯示的第一個頁碼
            int startIndex = (pageIndex + 1 < showRange) ?
                0 : (pageIndex + 1 + showRange / 2 >= pageCount) ? pageCount - showRange : pageIndex - showRange / 2;
            //顯示的最後一個頁碼
            int endIndex = (startIndex >= pageCount - showRange) ? pageCount : startIndex + showRange;

            LinkButton lbtnFirst = new LinkButton();
            LinkButton lbtnLast = new LinkButton();

            LinkButton lbtnPrev = new LinkButton();
            LinkButton lbtnNext = new LinkButton();

            LinkButton lbtnPage;
            TableRow tr;
            TableCell td;

            Table table = new Table();

            //新增第一頁 Start
            lbtnFirst.CommandName = "Page";
            lbtnFirst.CommandArgument = "1";
            lbtnFirst.Text = "&lt;&lt;";
            //新增第一頁 End

            //新增上一頁 Start
            lbtnPrev.Text = "&lt;";
            //新增上一頁 End

            #region 往前處理 (可否點選以及加入至Table)
            if (gv.PageIndex > 0)
            {
                lbtnPrev.Click += (obj, args) =>
                {
                    gv.PageIndex = gv.PageIndex - 1;
                };
            }
            else
            {
                lbtnPrev.Enabled = false;
                lbtnPrev.CssClass = "disabled";
                lbtnFirst.Enabled = false;
                lbtnFirst.CssClass = "disabled";
            }

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnFirst);
            tr.Cells.Add(td);
            table.Rows.Add(tr);

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnPrev);
            tr.Cells.Add(td);
            table.Rows.Add(tr);
            #endregion

            #region 新增頁碼數字
            for (int i = startIndex; i < endIndex; i++)
            {
                lbtnPage = new LinkButton();
                lbtnPage.Text = (i + 1).ToString();
                lbtnPage.CommandName = "Page";
                lbtnPage.CommandArgument = (i + 1).ToString();
                lbtnPage.Font.Overline = false;
                if (i == pageIndex)
                {
                    lbtnPage.Font.Bold = true;
                    lbtnPage.Enabled = false;
                }
                else
                {
                    lbtnPage.Font.Bold = false;
                }

                tr = new TableRow();
                td = new TableCell();

                td.Controls.Add(lbtnPage);
                tr.Cells.Add(td);
                table.Rows.Add(tr);
            }
            #endregion

            //新增下一頁 Start            
            lbtnNext.Text = "&gt;";
            //新增下一頁 End

            //新增末頁 Start
            lbtnLast.CommandName = "Page";
            lbtnLast.CommandArgument = GridView1.PageCount.ToString();
            lbtnLast.Text = "&gt;&gt;";
            //新增末頁 End

            #region 往後處理 (可否點選以及加入至Table)
            if (gv.PageIndex < gv.PageCount)
            {
                lbtnNext.Click += (obj, args) =>
                {
                    gv.PageIndex = gv.PageIndex + 1;
                };
            }

            if (gv.PageIndex + 1 == gv.PageCount)
            {
                lbtnNext.Enabled = false;
                lbtnNext.CssClass = "disabled";
                lbtnLast.Enabled = false;
                lbtnLast.CssClass = "disabled";
            }

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnNext);
            tr.Cells.Add(td);
            table.Rows.Add(tr);

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnLast);
            tr.Cells.Add(td);
            table.Rows.Add(tr);
            #endregion

            // 動態加入控制項
            //phdPageNumber.Controls.Add(
            //    new LiteralControl(string.Format("總頁數  {0} / {1}", pageIndex + 1, pageCount)));
            placeholder.Controls.Add(table);
        }
    }
    protected void drpShowCount_SelectedIndexChanged(object sender, EventArgs e)
    {
        //在GridView.PageIndex 無法正確被計算出的情況，切換顯示數目會有問題，所以切換至第一頁就沒問題
        DropDownList ddl = (DropDownList)sender;
        GridView1.PageSize = Convert.ToInt16(ddl.SelectedValue);
        GridView1.PageIndex = 0;
    }
    protected void ObjectDataSource1_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        //依序進入的事件
        //SelectMethod
        //SelectCountMethod
        //利用型別判斷將第一次非數字的回傳值繞過
        if (e.ReturnValue.GetType() != typeof(DataTable))
        {
            string intCount = e.ReturnValue.ToString();
            lblTotalCount.Text = "共" + intCount + "筆";
        }
    }
}