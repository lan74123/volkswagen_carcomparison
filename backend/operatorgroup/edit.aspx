﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="backend_operatorgroup_edit" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>






<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <script>
        $(function () {
            function resetFunc() {
                var $hidFunc = $("#hidFunc");
                var tmp = '';

                $("#funlist option").each(function () {
                    tmp += $(this).val() + ",";
                });
                $hidFunc.val(tmp);
            }

            function resetUsr() {
                var $hidUsr = $("#hidUsr");
                var tmp = '';

                $("#usrlist option").each(function () {
                    tmp += $(this).val() + ",";
                });
                $hidUsr.val(tmp);
            }

            // Add Function - Single
            $("#btnSelect").click(function () {
                $("#fFunAll option:selected").each(function () {
                    $("#funlist").append('<option value="' + $(this).val() + '" >' + $(this).text() + '</option>');
                });
                $("#fFunAll option:selected").remove();
                resetFunc();
            });

            // Remove Function - Single
            $("#btnRomove").click(function () {
                $("#funlist option:selected").each(function () {
                    $("#fFunAll").append('<option value="' + $(this).val() + '" >' + $(this).text() + '</option>');
                });
                $("#funlist option:selected").remove();
                resetFunc();
            });

            // Add Function - All
            $("#btnAllSelect").click(function () {
                $("#fFunAll option").each(function () {
                    $("#funlist").append('<option value="' + $(this).val() + '" >' + $(this).text() + '</option>');
                });
                $("#fFunAll option").remove();
                resetFunc();
            });

            // Remove Function - All
            $("#btnAllRomove").click(function () {
                $("#funlist option").each(function () {
                    $("#fFunAll").append('<option value="' + $(this).val() + '" >' + $(this).text() + '</option>');
                });
                $("#funlist option").remove();
                resetFunc();
            });

            // Add Function - Single
            $("#ubtnSelect").click(function () {
                $("#fUsrAll option:selected").each(function () {
                    $("#usrlist").append('<option value="' + $(this).val() + '" >' + $(this).text() + '</option>');
                });
                $("#fUsrAll option:selected").remove();
                resetUsr();
            });

            // Remove Function - Single
            $("#ubtnRomove").click(function () {
                $("#usrlist option:selected").each(function () {
                    $("#fUsrAll").append('<option value="' + $(this).val() + '" >' + $(this).text() + '</option>');
                });
                $("#usrlist option:selected").remove();
                resetUsr();
            });

            // Add Function - All
            $("#ubtnAllSelect").click(function () {
                $("#fUsrAll option").each(function () {
                    $("#usrlist").append('<option value="' + $(this).val() + '" >' + $(this).text() + '</option>');
                });
                $("#fUsrAll option").remove();
                resetUsr();
            });

            // Remove Function - All
            $("#ubtnAllRomove").click(function () {
                $("#usrlist option").each(function () {
                    $("#fUsrAll").append('<option value="' + $(this).val() + '" >' + $(this).text() + '</option>');
                });
                $("#usrlist option").remove();
                resetUsr();
            });
        });
    </script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">
                    <div class="col-md-12">
                        <form id="EditForm" runat="server" class="form-horizontal">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><%= html_panelTitle %></h4>
                                </div>
                                <div class="panel-body">
                                    <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="serno" CssClass="table"
                                        AutoGenerateRows="false" DefaultMode="Edit" DataSourceID="ObjectDataSource1"
                                        OnItemUpdating="DetailsView1_ItemUpdating" OnItemUpdated="DetailsView1_ItemUpdated"
                                        OnItemInserted="DetailsView1_ItemInserted" OnItemInserting="DetailsView1_ItemInserting" GridLines="None">
                                        <Fields>
                                            <asp:TemplateField>
                                                <InsertItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnInsert" runat="server" Text="確定新增" CommandName="Insert" CssClass="btn btn-primary btn-xs" />
                                                        <asp:Button ID="btnBackTo1" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                    </div>
                                                </InsertItemTemplate>
                                                <EditItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnUpdate" runat="server" Text="確定修改" CommandName="Update" CssClass="btn btn-primary btn-xs" />
                                                        <asp:Button ID="btnBackTo2" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                    </div>
                                                </EditItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="serno" HeaderText="群組代碼" ReadOnly="True" InsertVisible="False" HeaderStyle-CssClass="col-md-2" />

                                            <asp:TemplateField HeaderText="群組名" HeaderStyle-CssClass="col-md-2">
                                                <EditItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="name" runat="server" Text='<%# Bind("name") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="name" runat="server" Text='<%# Bind("name") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="有效否">
                                                <EditItemTemplate>
                                                    <asp:CheckBox ID="valid" runat="server" Checked='<%# Eval("valid").ToString()=="0" ? false :true %>' />
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:CheckBox ID="valid" runat="server" Checked='true' />
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CDate" HeaderText="建立日期" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                                ReadOnly="True" InsertVisible="False"></asp:BoundField>
                                            <asp:BoundField DataField="MDate" HeaderText="最後修改日期" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                                ReadOnly="True" InsertVisible="False"></asp:BoundField>
                                            <asp:TemplateField HeaderText="最後修改人員" InsertVisible="false">
                                                <EditItemTemplate>
                                                    <asp:Label ID="fOid" runat="server" Text='<%#  Backend_DBOperator.GetNameBySerno(Convert.ToString(Eval("oid"))) %>' />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="功能 List">
                                                <EditItemTemplate>
                                                    <table>
                                                        <tr style="text-align: center">
                                                            <td style="background-color: #cccccc" class="nopadding">來源
                                                            </td>
                                                            <td style="width: 80px" class="nopadding">&nbsp;
                                                            </td>
                                                            <td style="background-color: #cccccc" class="nopadding">所屬功能
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nopadding">
                                                                <asp:ListBox ID="fFunAll" runat="server" SelectionMode="Multiple" DataSourceID="odsFunAll" ClientIDMode="Static"
                                                                    DataTextField="FunName" DataValueField="serno" Width="160px" Height="160px"></asp:ListBox>
                                                            </td>
                                                            <td style="text-align: center">
                                                                <input type="button" name="btnAllSelect" value=" ＞＞ " id="btnAllSelect" class="btn btn-default form-control btn-xs">
                                                                <br>
                                                                <input type="button" name="btnSelect" value="  ＞  " id="btnSelect" class="btn btn-default form-control btn-xs">
                                                                <br>
                                                                <input type="button" name="btnRomove" value="  ＜  " id="btnRomove" class="btn btn-default form-control btn-xs">
                                                                <br>
                                                                <input type="button" name="btnAllRomove" value=" ＜＜ " id="btnAllRomove" class="btn btn-default form-control btn-xs">
                                                                <br>
                                                            </td>
                                                            <td class="nopadding">
                                                                <asp:ListBox ID="funlist" runat="server" SelectionMode="Multiple" DataSourceID="odsFunList" ClientIDMode="Static"
                                                                    DataTextField="FunName" DataValueField="serno" Width="160px" Height="160px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditItemTemplate>
                                            </asp:TemplateField>      
                                            <asp:TemplateField HeaderText="對象 List">
                                                <EditItemTemplate>
                                                    <table>
                                                        <tr style="text-align: center">
                                                            <td style="background-color: #cccccc" class="nopadding">來源
                                                            </td>
                                                            <td style="width: 80px" class="nopadding">&nbsp;
                                                            </td>
                                                            <td style="background-color: #cccccc" class="nopadding">所屬對象
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nopadding">
                                                                <asp:ListBox ID="fUsrAll" runat="server" SelectionMode="Multiple" DataSourceID="odsUsrAll" ClientIDMode="Static"
                                                                    DataTextField="Name" DataValueField="serno" Width="160px" Height="160px"></asp:ListBox>
                                                            </td>
                                                            <td style="text-align: center">
                                                                <input type="button" name="btnAllSelect" value=" ＞＞ " id="ubtnAllSelect" class="btn btn-default form-control btn-xs">
                                                                <br>
                                                                <input type="button" name="btnSelect" value=" ＞ " id="ubtnSelect" class="btn btn-default form-control btn-xs">
                                                                <br>
                                                                <input type="button" name="btnRomove" value=" ＜ " id="ubtnRomove" class="btn btn-default form-control btn-xs">
                                                                <br>
                                                                <input type="button" name="btnAllRomove" value=" ＜＜ " id="ubtnAllRomove" class="btn btn-default form-control btn-xs">
                                                                <br>
                                                            </td>
                                                            <td class="nopadding">
                                                                <asp:ListBox ID="usrlist" runat="server" SelectionMode="Multiple" DataSourceID="odsUsrList" ClientIDMode="Static"
                                                                    DataTextField="Name" DataValueField="serno" Width="160px" Height="160px"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditItemTemplate>
                                            </asp:TemplateField>                                     
                                            <asp:TemplateField>
                                                <InsertItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnInsert_foot" runat="server" Text="確定新增" CommandName="Insert" CssClass="btn btn-primary btn-xs" />
                                                        <asp:Button ID="btnBackTo_foot" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                    </div>
                                                </InsertItemTemplate>
                                                <EditItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnUpdate_foot" runat="server" Text="確定修改" CommandName="Update" CssClass="btn btn-primary btn-xs" />
                                                        <asp:Button ID="btnBackTo_foot" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                    </div>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:HiddenField runat="server" ID="hidUsr" ClientIDMode="Static" Value="" />
                                    <asp:HiddenField runat="server" ID="hidFunc" ClientIDMode="Static" Value="" />
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel-default -->

                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBOperatorGroup" SelectMethod="GetListBySerno" InsertMethod="Insert" UpdateMethod="Update">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="serno" QueryStringField="serno" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="odsFunAll" runat="server" SelectMethod="get_unselect_funlist" TypeName="BackEnd_DBFunctionTable">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="oid" QueryStringField="serno" Type="String" DefaultValue="99999" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="odsFunList" runat="server" SelectMethod="get_selected_funlist" TypeName="BackEnd_DBFunctionTable"
                                OnSelected="odsFunList_Selected"
                                OnSelecting="odsFunList_Selecting">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="oid" QueryStringField="serno" Type="String" DefaultValue="99999" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="odsUsrAll" runat="server" SelectMethod="get_unselect_usrlist" TypeName="BackEnd_DBFunctionTable">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="oid" QueryStringField="serno" Type="String" DefaultValue="99999" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="odsUsrList" runat="server" SelectMethod="get_selected_usrlist" TypeName="BackEnd_DBFunctionTable"
                                OnSelected="odsUsrList_Selected"
                                OnSelecting="odsUsrList_Selecting">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="oid" QueryStringField="serno" Type="String" DefaultValue="99999" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </form>
                    </div>
                </div>
                <!-- row -->
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>

    <uc1:uc_foot runat="server" ID="uc_foot" />

</body>
</html>

