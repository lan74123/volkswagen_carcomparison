﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_uc_uc_leftmenu : System.Web.UI.UserControl
{
    public string html_menu { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (BackendBasePage.IsLogin())
        {
            CreateLeftMenu();
        }
    }

    protected void CreateLeftMenu()
    {
        DataTable dt_Menu = Backend_DBFunctionMenu.GetMain(Convert.ToInt32(BackendBasePage.GetUserSerno()));

        string tempMenuName = string.Empty;

        if (dt_Menu.Rows.Count > 0)
        {
            var group = (from r in dt_Menu.AsEnumerable()
                         orderby r.Field<int>("FunMenuCorder") ascending
                         select r.Field<string>("MenuName")).Distinct();

            foreach (var g in group)
            {
                var rowdata = (from r in dt_Menu.AsEnumerable()
                               where r.Field<string>("MenuName") == g
                               select r);

                int itemCount = rowdata.Count();

                //if (itemCount == 1)
                //{
                //    DataRow tmp = rowdata.First();
                //    html_menu += CreateSingular(tmp["FunSerno"].ToString(), Config.BackendPath + tmp["FunLink"].ToString(), tmp["FunName"].ToString(), "", false, false);
                //}
                //else if (itemCount > 1)
                //{
                //    string parentName = rowdata.First()["MenuName"].ToString();
                //    string parentIconClass = rowdata.First()["ClassIcon"].ToString();
                //    List<CNode> nodes = new List<CNode>();
                //    foreach (DataRow tmp in rowdata)
                //    {
                //        nodes.Add(new CNode(tmp["FunSerno"].ToString(), Config.BackendPath + tmp["FunLink"].ToString(), tmp["FunName"].ToString(), "fa-caret-right", false, true));
                //    }
                //    html_menu += CreateTwoTierMenu(parentIconClass, parentName, nodes);
                //}
                string parentName = rowdata.First()["MenuName"].ToString();
                string parentIconClass = rowdata.First()["ClassIcon"].ToString();
                List<CNode> nodes = new List<CNode>();
                foreach (DataRow tmp in rowdata)
                {
                    nodes.Add(new CNode(tmp["FunSerno"].ToString(), Config.BackendPath + tmp["FunLink"].ToString(), tmp["FunName"].ToString(), "fa-caret-right", false, true));
                }
                html_menu += CreateTwoTierMenu(parentIconClass, parentName, nodes);
            }
        }

        dt_Menu.Dispose();
    }

    /// <summary>
    /// 建立單層選單
    /// </summary>
    /// <param name="_url"></param>
    /// <param name="_name"></param>
    /// <param name="_active"></param>
    /// <param name="_icon"></param>
    /// <returns></returns>
    private string CreateSingular(string _id, string _url, string _name, string _icon, bool _active, bool _hasParent)
    {
        return HttpUtility.HtmlEncode(new CNode(_id, _url, _name, _icon, _active, _hasParent).getHTML());
    }

    /// <summary>
    /// 建立雙層選單
    /// </summary>
    /// <param name="_parentIcon"></param>
    /// <param name="_parentName"></param>
    /// <param name="_nodes"></param>
    /// <returns></returns>
    private string CreateTwoTierMenu(string _parentIcon, string _parentName, List<CNode> _nodes)
    {
        string returnValue = string.Empty;

        returnValue = string.Format("<li class='nav-parent'><a href=''><i class='fa {0}'></i><span>{1}</span></a>", _parentIcon, _parentName);
        returnValue += "<ul class='children'>";

        foreach (CNode node in _nodes)
        {
            returnValue += node.getHTML();
        }

        returnValue += "</ul>";
        returnValue += "</li>";

        return HttpUtility.HtmlEncode(returnValue);
    }

    /// <summary>
    /// 單一節點的結構
    /// </summary>
    private class CNode
    {
        public CNode(string _id, string _url, string _name, string _icon, bool _active, bool _hasParent)
        {
            this.id = _id;
            this.url = _url;
            this.name = _name;
            this.icon = _icon;
            this.active = _active;
            this.hasParent = _hasParent;
        }

        private string id { get; set; }
        private string url { get; set; }
        private string name { get; set; }
        private string icon { get; set; }
        private bool active { get; set; }
        private bool hasParent { get; set; }

        public string getHTML()
        {
            if (active)
            {
                if (hasParent)
                {
                    return String.Format("<li id='node{4}' data-level='{3}' class='active'><a href='{0}'><i class='fa {1}'></i><span>{2}</span></a></li>", this.url, this.icon, this.name, "two", id);
                }
                else
                {
                    return String.Format("<li id='node{4}' data-level='{3}' class='active'><a href='{0}'><i class='fa {1}'></i><span>{2}</span></a></li>", this.url, this.icon, this.name, "one", id);
                }
            }
            else
            {
                if (hasParent)
                {
                    return String.Format("<li id='node{4}' data-level='{3}'><a href='{0}'><i class='fa {1}'></i><span>{2}</span></a></li>", this.url, this.icon, this.name, "two", id);
                }
                else
                {
                    return String.Format("<li id='node{4}' data-level='{3}'><a href='{0}'><i class='fa {1}'></i><span>{2}</span></a></li>", this.url, this.icon, this.name, "one", id);
                }
            }
        }
    }
}