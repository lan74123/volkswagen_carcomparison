﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_foot.ascx.cs" Inherits="backend_uc_uc_foot" %>



<script src='<%= ResolveUrl("~/backend/asset/js/bootstrap.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/modernizr.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/jquery.sparkline.min.js") %>'></script>


<script src='<%= ResolveUrl("~/backend/asset/js/toggles.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/retina.min.js") %>'></script>


<script src='<%= ResolveUrl("~/backend/asset/js/flot/jquery.flot.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/flot/jquery.flot.resize.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/flot/jquery.flot.spline.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/morris.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/raphael-2.1.0.min.js") %>'></script>

<script src='<%= ResolveUrl("~/backend/asset/js/custom.js") %>'></script>
<%--<script src="<%= ResolveUrl("/backend/js/dashboard.js") %>"></script>--%>