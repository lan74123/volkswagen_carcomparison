﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class backend_Car_editequip : BackendBasePage
{

    public string html_panelTitle { get; set; }

    public List<EquipTemp> EquipTempList { get; set; }
    Dictionary<string, EquipTemp> EquipDict { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Request.Params["CarId"]))
            {

                JSOutPutAndRedirect("查無此車", "list.aspx");

            }
            else
            {
                //---------------------------------------------------------編輯
                html_panelTitle = "編輯";
                qSRNValid.Text = null;
                qClassValid.Text = null;
                // GridViewEquip.ChangeMode(DetailsViewMode.Edit);

                string CarID = Request.Params["CarId"].ToString();

                DataTable dt = Backend_DBEquip.GetEquipTempListByCarId(CarID);

                EquipTempList = (List<EquipTemp>)DataTableExtensions.ToList<EquipTemp>(dt);

                GridViewEquip.DataSource = getEquipTemp(EquipTempList);
                GridViewEquip.DataBind();

            }
        }
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("list.aspx");
    }


    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        RadioButtonList EquipType = ((RadioButtonList)GridViewEquip.FindControl("qEquipType"));
        Label EquipTypeLabel = ((Label)GridViewEquip.FindControl("EquipType"));
        EquipType.SelectedValue = EquipTypeLabel.Text;
    }

    protected void areaNameAddSpace(object sender, EventArgs e)
    {
    }

    protected void GridViewImg_DataBound(object sender, EventArgs e)
    {


    }

    protected void TempSelect_Click(object sender, EventArgs e)
    {
        string carID = "";
        foreach (GridViewRow row in GridViewEquip.Rows)
        {
            Label Serno = (Label)row.FindControl("Serno");
            Label CarID = (Label)row.FindControl("CarID");
            Label equipId = (Label)row.FindControl("EquipId");

            Label Code = (Label)row.FindControl("Code");
            DropDownList EquiplistNameDropDownList = (DropDownList)row.FindControl("qEquiplistName");
            RadioButtonList nEquipType = (RadioButtonList)row.FindControl("qEquipType");
            TextBox equipExtraDetailBox = (TextBox)row.FindControl("EquipExtraDetail");

            Label EquipName = (Label)row.FindControl("EquipName");
            RadioButtonList Error = (RadioButtonList)row.FindControl("qError");

            carID = CarID.Text;
            string serno = Serno.Text;
            string EquipType = nEquipType.SelectedValue;
            string equipID = EquiplistNameDropDownList.SelectedValue;
            string equipExtraDetail = equipExtraDetailBox.Text;

            string error = "N";
            if (Error != null)
            {
                error = Error.SelectedValue;
            }


            Backend_DBEquip.UpdateTempSelect(serno, equipID, EquipType, error, equipExtraDetail);
        }

        JSOutPut("暫存成功");

        DataTable dt = Backend_DBEquip.GetEquipTempListByCarId(carID);

        EquipTempList = (List<EquipTemp>)DataTableExtensions.ToList<EquipTemp>(dt);

        GridViewEquip.DataSource = getEquipTemp(EquipTempList);
        GridViewEquip.DataBind();
    }

    protected void btnEquip_Click(object sender, EventArgs e)
    {
        string msg = "";
        //寄信列表
        List<sendMailEquip> mailEquipList = new List<sendMailEquip>();
        //要更新的配備
        List<carEquip> carEquipList = new List<carEquip>();

        foreach (GridViewRow row in GridViewEquip.Rows)
        {
            Label CarID = (Label)row.FindControl("CarID");
            Label equipId = (Label)row.FindControl("EquipId");

            DropDownList EquiplistNameDropDownList = (DropDownList)row.FindControl("qEquiplistName");
            RadioButtonList nEquipType = (RadioButtonList)row.FindControl("qEquipType");
            TextBox equipExtraDetailBox = (TextBox)row.FindControl("EquipExtraDetail");

            Label EquipName = (Label)row.FindControl("EquipName");
            RadioButtonList Error = (RadioButtonList)row.FindControl("qError");

            string carID = CarID.Text;
            string equipID = equipId.Text;

            string newEquipType = nEquipType.SelectedValue.ToString();
            string equipExtraDetail = equipExtraDetailBox.Text;

            string equipName = EquipName.Text;
            string nEquiplistNameDropDownList = EquiplistNameDropDownList.SelectedValue.ToString();
            string error = Error.SelectedValue.ToString();
            //error==IN 代表匯入 才判斷
            if (error == "I")
            {
                if (nEquiplistNameDropDownList == "0")
                {

                    msg += "請選擇  " + equipName + "  類別 \\n";

                }
            }

            if (error == "Y")
            {
                sendMailEquip obj = new sendMailEquip();
                obj.EquipID = equipID;
                obj.EquipName = equipName;
                mailEquipList.Add(obj);
            }



            //equipID==0 代表沒對應到
            if (equipID != "0")
            {
                //error==IN 代表匯入
                if (error == "I")
                {
                    carEquip ce = new carEquip();
                    ce.CarID = carID;
                    ce.EquipID = equipID;
                    ce.EquipType = newEquipType;
                    ce.EquipExtraDetail = equipExtraDetail;
                    carEquipList.Add(ce);
                }

            }

        }

        if (!string.IsNullOrEmpty(msg))
        {
            JSOutPut(msg);
        }
        else
        {
            string carID = "";
            //取得CARID 刪除
            for (int i = 0; i < carEquipList.Count; i++)
            {
                if (string.IsNullOrEmpty(carID))
                {
                    carID = carEquipList[i].CarID;
                    break;
                   
                }
            }
            Backend_DBEquip.DeleteFromBatch(carID);

            foreach (carEquip ce in carEquipList)
            {
                carID = ce.CarID;
                if (ce.EquipID != "0")
                {
                    if (ce.EquipType.Equals(""))
                    {
                    
                    }
                    else
                    {//insert or update 
                        Backend_DBEquip.InsertOrUpdate(ce.CarID, ce.EquipID, ce.EquipType, ce.EquipExtraDetail);
                    }
                }
            }

            //寄信給窗口     
            //if (mailEquipList.Count > 0)
            //{
            //    string username = BackendBasePage.GetUserName();
            //    string equips = "";
            //    foreach (sendMailEquip data in mailEquipList)
            //    {
            //        equips += data.EquipName + " " + data.code + "<br/> ";
            //    }

            //    Backend_DBOperator op = new Backend_DBOperator();
            //    string emails = op.GetEmailBySerno(BackendBasePage.GetUserSerno().ToString());


            //    ContactEmail.SendEquipImportIssue(username, equips, emails);
            //}

            if (IsPostBack)
            {
                //匯入成功珊除temp資料
                Backend_DBEquip.DeleteEquipTempByCarID(carID);
                Backend_DBCar.UpdateStatus(carID, "T", "Status");
                JSOutPutAndRedirect("匯入成功。", "edit.aspx?CarId=" + carID);
            }
        }

    }
    protected void GridViewEquip_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label EquiplistName = (Label)e.Row.FindControl("EquiplistName");
            Label EquipName = (Label)e.Row.FindControl("EquipName");
            Label EquipTempSelect = (Label)e.Row.FindControl("EquipTempSelect");
            Label Serno = (Label)e.Row.FindControl("Serno");
            DropDownList EquiplistNameDropDownList = (DropDownList)e.Row.FindControl("qEquiplistName");
            RadioButtonList Error = (RadioButtonList)e.Row.FindControl("qError");

            //  Error.Visible = false;

            EquiplistNameDropDownList.Items.Clear();

            foreach (string key in EquipDict.Keys)
            {
                if (key.Equals(EquipName.Text + "_" + Serno.Text))
                {

                    EquipTemp data = (EquipTemp)EquipDict[key];
                    if (EquipDict[key].EquiplistNameList.Count == 1)
                    {
                        EquiplistNameDropDownList.Items.Clear();
                        if (string.IsNullOrEmpty(data.EquiplistName))
                        {
                            EquiplistNameDropDownList.Visible = false;
                            if (!string.IsNullOrEmpty(data.EquipTempImport))
                                Error.SelectedValue = data.EquipTempImport;
                            //Error.Visible = true;
                        }
                        else
                        {
                            EquiplistNameDropDownList.Items.Add(new ListItem(data.EquiplistName, data.EquipID.ToString()));
                            Error.Items.Insert(0, new ListItem("匯入", "I"));

                            if (data.EquipNameFormPM.Equals(EquipName.Text))
                            {
                                Error.SelectedValue = "I";
                            }
                            else
                            {//配備類別與配備名稱不相符
                                EquiplistNameDropDownList.Style.Add("color", "blue");
                                Error.SelectedValue = "I";
                            }

                            //更新過 處理狀態撈後來的資料
                            if (data.IsUpdate == 1)
                                Error.SelectedValue = data.EquipTempImport;

                        }
                    }
                    else
                    {
                        EquiplistNameDropDownList.Items.Clear();
                        EquiplistNameDropDownList.Items.Add(new ListItem("請選擇", "0"));
                        EquiplistNameDropDownList.Style.Add("color", "red");
                        foreach (EquiplistObj dataObj in data.EquiplistNameList)
                        {
                            EquiplistNameDropDownList.Items.Add(new ListItem(dataObj.EquiplistName, dataObj.EquipID.ToString()));
                        }

                        if (!string.IsNullOrEmpty(EquipTempSelect.Text))
                        {
                            EquiplistNameDropDownList.SelectedValue = EquipTempSelect.Text;
                        }
                        //配備五碼對應到多筆配備，預設不匯入
                        Error.Items.Insert(0, new ListItem("匯入", "I"));
                        Error.SelectedValue = "N";

                        //更新過 處理狀態撈後來的資料
                        if (data.IsUpdate == 1)
                            Error.SelectedValue = data.EquipTempImport;
                    }
                }

            }

        }
    }


    protected List<EquipTemp> getEquipTemp(List<EquipTemp> datalist)
    {
        EquipDict = new Dictionary<string, EquipTemp>();

        foreach (EquipTemp element in EquipTempList)
        {
            string key = element.EquipName + "_" + element.Serno;

            //判斷配備名稱是否重覆
            if (!EquipDict.ContainsKey(key))
            {
                EquiplistObj obj = new EquiplistObj();
                obj.EquipCategoryID = element.EquipCategoryID;

                obj.EquipID = element.EquipID;
                obj.EquipTempSelect = element.EquipTempSelect;
                obj.EquipTempImport = element.EquipTempImport;

                obj.EquiplistName = element.EquiplistName;
                obj.EquipNameFormPM = element.EquipNameFormPM;

                //判斷重覆CODE對應的選單是否有起始值
                if (element.EquiplistNameList == null)
                {
                    List<EquiplistObj> eObj = new List<EquiplistObj>();

                    eObj.Add(obj);
                    element.EquiplistNameList = eObj;
                }

                EquipDict.Add(key, element);
            }
            else
            {

                EquipTemp oldelement = EquipDict[key];

                EquiplistObj obj = new EquiplistObj();
                obj.EquipCategoryID = element.EquipCategoryID;
                obj.EquipID = element.EquipID;
                obj.EquipTempSelect = element.EquipTempSelect;
                obj.EquiplistName = element.EquiplistName;
                obj.EquipTempImport = element.EquipTempImport;



                //取得CODE對應的選單
                List<EquiplistObj> eObj = oldelement.EquiplistNameList;
                eObj.Add(obj);
                element.EquiplistNameList = eObj;
                EquipDict[key] = oldelement;
            }
        }

        //從DICT取得整理過的資料寫回LIST
        List<EquipTemp> newList = new List<EquipTemp>();
        foreach (string key in EquipDict.Keys)
        {
            newList.Add(EquipDict[key]);
        }
        return newList;
    }

    //配備準備
    public class EquipTemp
    {
        public int Serno { get; set; }
        public int CarID { get; set; }
        public int EquipCategoryID { get; set; }
        public int EquipID { get; set; }
        public string EquipNameFormPM { get; set; }
        public string EquiplistName { get; set; }
        public int EquipTempSelect { get; set; }
        public string EquipExtraDetail { get; set; }
        public int IsUpdate { get; set; }


        public List<EquiplistObj> EquiplistNameList { get; set; }
        public string EquipType { get; set; }
        public string EquipName { get; set; }
        public string EquipTempImport { get; set; }

        public EquipTemp() { }
    }

    public class EquiplistObj
    {
        public int Serno { get; set; }
        public int EquipCategoryID { get; set; }
        public int EquipID { get; set; }
        public string EquiplistName { get; set; }
        public string EquipNameFormPM { get; set; }
        public int EquipTempSelect { get; set; }
        public string EquipTempImport { get; set; }
        public string EquipExtraDetail { get; set; }
        public int IsUpdate { get; set; }

        public EquiplistObj() { }
    }

    public class sendMailEquip
    {
        public string EquipID { get; set; }
        public string code { get; set; }
        public string EquipName { get; set; }

        public sendMailEquip() { }
    }

    public class carEquip
    {
        public string CarID { get; set; }
        public string EquipID { get; set; }
        public string EquipType { get; set; }
        public string EquipExtraDetail { get; set; }

        public carEquip() { }
    }



}