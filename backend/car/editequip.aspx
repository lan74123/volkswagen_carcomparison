﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="editequip.aspx.cs" Inherits="backend_Car_editequip" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>






<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <script type="text/javascript">
        $(document).ready(function () {
            //solve img chache
            var myimg = document.getElementsByTagName('img');
            for (var i = 0; i < myimg.length; i++) {
                if (myimg[i].src != "") {
                    myimg[i].src = myimg[i].src + "?_" + Date.now();
                }
            }
        });
        //只能輸入數字
        function ValidateNumber(e, pnumber) {
            if (!/^[0-9.]+$/.test(pnumber)) {
                e.value = /^[0-9.]+$/.exec(e.value);
            }
            return false;
        }
    </script>
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">
                    <div class="col-md-12">
                        <form id="EditForm" runat="server" class="form-horizontal">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><%= html_panelTitle %></h4>
                                    <asp:Label ID="qSRNValid" runat="server" Visible="false" Text="1"></asp:Label>
                                    <asp:Label ID="qClassValid" runat="server" Visible="false" Text="1"></asp:Label>
                                </div>
                                <ul class="nav nav-tabs" runat="server">
                                    <li class="active" runat="server" id="carEqPage"><a data-toggle="tab" href="#menuCarEquip">車輛配備</a></li>
                                </ul>
                                <div class="tab-content">

                                    <div>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                配備類別顏色說明:<br/> 
                                                黑色(正常)<br/> 
                                                <label style="color:red">紅色(配備名稱對應到多筆配備，預設不匯入，請選擇處理方式)</label><br/>
                                                <label style="color:blue">藍色(配備類別與配備名稱不相符，預設不匯入，請選擇處理方式)</label>
                                            </div>
                                            <div class="col-md-6 " style="text-align: right">
                                                <asp:Button runat="server" Text="暫時儲存" OnClick="TempSelect_Click" CssClass="btn btn-warning btn-xs" />
                                                <asp:Button runat="server" Text="確認匯入" OnClick="btnEquip_Click" CssClass="btn btn-primary btn-xs" />
                                                <asp:Button runat="server" Text="回列表" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                            </div>
                                        </div>
                                        <asp:GridView ID="GridViewEquip" AllowPaging="false" OnRowDataBound="GridViewEquip_RowDataBound" runat="server" AutoGenerateColumns="false" GridLines="None" DefaultMode="Edit" CssClass="table">
                                            <Columns>
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <div class="col-md-1 nopadding">
                                                            <asp:Label ID="Serno" runat="server" Text='<%# Bind("Serno") %>' ReadOnly="true"></asp:Label>
                                                            <asp:Label ID="EquipCategoryID" runat="server" Text='<%# Bind("EquipCategoryID") %>' ReadOnly="true"></asp:Label>
                                                            <asp:Label ID="EquipId" runat="server" Text='<%# Bind("EquipId") %>' ReadOnly="true"></asp:Label>
                                                            <asp:Label ID="EquipTempSelect" runat="server" Text='<%# Bind("EquipTempSelect") %>' ReadOnly="true"></asp:Label>
                                                            <asp:Label ID="CarID" runat="server" Text='<%# Bind("CarID") %>' ReadOnly="true"></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="配備類別" HeaderStyle-CssClass="col-md-4 ">
                                                    <ItemTemplate>
                                                        <div class="col-md-11 nopadding">
                                                            <asp:Label ID="EquiplistName" runat="server" Text='<%# Bind("EquiplistName") %>' Visible="false"></asp:Label>
                                                            <asp:DropDownList ID="qEquiplistName" runat="server" CssClass="form-control col-md-11">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                             

                                                <asp:TemplateField HeaderText="標選配" HeaderStyle-CssClass="col-md-2 ">
                                                    <ItemTemplate>
                                                        <asp:Label ID="EquipType" runat="server" Text='<%# Eval("EquipType").ToString().Trim() %>' Visible="false"></asp:Label>
                                                        <asp:RadioButtonList ID="qEquipType" runat="server" SelectedValue='<%# Eval("EquipType").ToString().Trim() %>' RepeatColumns="3">
                                                            <asp:ListItem Value="S" Text="標準配備"></asp:ListItem>
                                                            <asp:ListItem Value="E" Text="額外配備"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                         <label>配備說明文字</label>
                                                          <asp:TextBox ID="EquipExtraDetail" runat="server" Text='<%# Bind("EquipExtraDetail") %>' TextMode="MultiLine" Width="100%" Height="60px" MaxLength="100"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="配備名稱" HeaderStyle-CssClass="col-md-2">
                                                    <ItemTemplate>
                                                        <div class="col-md-8" style="top: 15px; display: inline-block;">
                                                            <asp:Label ID="EquipName" runat="server" Text='<%# Bind("EquipName") %>' ReadOnly="true"></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="處理" HeaderStyle-CssClass="col-md-3">
                                                    <ItemTemplate>
                                                        <asp:Label ID="EquipTempImport" runat="server" Text='<%# Eval("EquipTempImport").ToString().Trim() %>' Visible="false"></asp:Label>
                                                        <asp:RadioButtonList ID="qError" runat="server" RepeatColumns="3">
                                                            <asp:ListItem Value="N" Text="不匯入"></asp:ListItem>
                                                           <%-- <asp:ListItem Value="Y" Text="不匯入，通知MB配備遺漏"></asp:ListItem>--%>
                                                        </asp:RadioButtonList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <!-- panel-body -->
                        </form>
                    </div>
                </div>
                <!-- panel-default -->
            </div>
        </div>
        <!-- row -->
        </div>
        <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>

    <uc1:uc_foot runat="server" ID="uc_foot" />
</body>
</html>


