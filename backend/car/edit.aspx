﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="backend_Car_edit" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>






<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <script type="text/javascript">
        $(document).ready(function () {
            //solve img chache
            var myimg = document.getElementsByTagName('img');
            for (var i = 0; i < myimg.length; i++) {
                if (myimg[i].src != "") {
                    myimg[i].src = myimg[i].src + "?_" + Date.now();
                }
            }
        });
        //只能輸入數字
        function ValidateNumber(e, pnumber) {
            if (!/^[0-9.]+$/.test(pnumber)) {
                e.value = /^[0-9.]+$/.exec(e.value);
            }
            return false;
        }
    </script>
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">
                    <div class="col-md-12">
                        <form id="EditForm" runat="server" class="form-horizontal">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><%= html_panelTitle %></h4>
                                    <asp:Label ID="qSRNValid" runat="server" Visible="false" Text="1"></asp:Label>
                                </div>
                                <ul class="nav nav-tabs" runat="server">
                                    <li class="active" runat="server" id="carDataPage"><a data-toggle="tab" href="#home">基本資料</a></li>
                                    <li runat="server" id="carEqPage"><a data-toggle="tab" href="#menuCarEquip">車輛配備</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="panel-body tab-pane fade in active" id="home" runat="server" name="home">
                                        <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="CarId" CssClass="table"
                                            AutoGenerateRows="false" DefaultMode="Edit" DataSourceID="ObjectDataSource1"
                                            OnItemUpdating="DetailsView1_ItemUpdating" OnItemUpdated="DetailsView1_ItemUpdated"
                                            OnItemInserted="DetailsView1_ItemInserted" OnItemInserting="DetailsView1_ItemInserting"
                                            OnDataBound="DetailsView1_DataBound" GridLines="None">
                                            <Fields>
                                                <asp:TemplateField>
                                                    <InsertItemTemplate>
                                                        <div style="text-align: right">
                                                            <asp:Button ID="btnInsert" runat="server" Text="確定新增" CommandName="Insert" CssClass="btn btn-primary btn-xs" />
                                                            <asp:Button ID="btnBackTo" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                        </div>
                                                    </InsertItemTemplate>
                                                    <EditItemTemplate>
                                                        <div style="text-align: right">
                                                            <asp:Button ID="btnOnline" runat="server" Text="上傳正式區"  OnClientClick='return confirm("您確定要將修改儲存後的資料上傳正式區嗎?")'  OnClick="submitToOnline"  CssClass="btn btn-success btn-xs"/>
                                                            <asp:Button ID="btnDelete" runat="server" Text="刪除" OnClientClick='return confirm("您確定要刪除此筆資料嗎?")' CssClass="btn btn-danger btn-xs" OnClick="btnDelete_Click" />
                                                            <div runat="server" class="btn btn-primary btn-xs"><a style="color: white; text-decoration: none;" href='<%# Config.FrontPath + "/spec.html?CarID="+ Eval("CarId")+"&preview=preview"%>' target="_blank">預覽</a></div>
                                                            <asp:Button ID="btnUpdate" runat="server" Text="確定修改" CommandName="Update" CssClass="btn btn-primary btn-xs" />
                                                            <asp:Button ID="btnBackTo" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-CssClass="col-md-2" Visible="false" InsertVisible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:Label ID="CarId" runat="server" Text='<%# Bind("CarId") %>' ReadOnly="true" CssClass="col-md-3 nopadding"></asp:Label>
                                                            <asp:Label ID="Status" runat="server" Text='<%# Bind("Status") %>' ReadOnly="true" CssClass="col-md-3 nopadding"></asp:Label>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="車種" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:Label ID="CarTypeNameLabel" runat="server" Text='<%# Eval("CarTypeID").ToString().Trim() %>' Visible="false"></asp:Label>
                                                            <asp:DropDownList ID="qCarTypeID" runat="server"
                                                                DataTextField="CarTypeName" DataValueField="CarTypeID" OnDataBound="CarTypeID_DataBound" DataSourceID="ObjectDataSource2" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:DropDownList ID="qCarTypeID" runat="server"
                                                                DataTextField="CarTypeName" DataValueField="CarTypeID" DataSourceID="ObjectDataSource2" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="顯示在首頁" >
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="ShowHomePage" runat="server" Checked='<%# Eval("ShowHomePage").ToString()=="1" ? true :false %>' />
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <asp:CheckBox ID="ShowHomePage" runat="server" Checked='false' />
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<span style='color:red;'>*</span>車名" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="CarName" runat="server" Text='<%# Bind("CarName") %>' MaxLength="80" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="CarName" runat="server" Text='<%# Bind("CarName") %>' MaxLength="80" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<span style='color:red;'>*</span>排序" HeaderStyle-CssClass="col-md-2">
                                                <EditItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="corder" runat="server" Text='<%# Bind("COrder") %>' MaxLength="20" CssClass="form-control" type="Number"></asp:TextBox>
                                                    </div>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="corder" runat="server" Text='<%# Bind("COrder") %>' MaxLength="20" CssClass="form-control" type="Number"></asp:TextBox>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="車圖" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="CarImg" runat="server" Text='<%# Bind("CarImg") %>' hidden="true" type="Number"></asp:TextBox>
                                                        <img src='<%#  ResolveClientUrl ("~")+Eval("CarImg") %>' runat='server' width="530" visible='<%# ((Eval("CarImg").ToString().Length) >1) %>' />
                                                        <asp:FileUpload ID="FileUploadCarImg" runat="server" /><span>建議尺寸:1055*500 \ 檔案格式:.jpg,.jpeg,.png \ 大小不得超過5MB</span>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <asp:FileUpload ID="FileUploadCarImg" runat="server" /><span>建議尺寸:1055*500 \ 檔案格式:.jpg,.jpeg,.png \ 大小不得超過5MB</span>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="上下架狀態:" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:DropDownList ID="qStockStatus" runat="server" SelectedValue='<%# Bind("StockStatus") %>' CssClass="form-control input-sm">
                                                                <asp:ListItem Value="P">草稿</asp:ListItem>
                                                                <asp:ListItem Value="S">上架</asp:ListItem>
                                                                <asp:ListItem Value="D">下架</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <span style="color: #428bca;">(新車輛預設為草稿，資料確認後可切換為上架，不顯示前台切換成下架)</span><br />                                                           
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:DropDownList ID="qStockStatus" runat="server" CssClass="form-control input-sm">
                                                                <asp:ListItem Value="P" Selected="True">草稿</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <span style="color: #d9534f;">(新車輛預設為草稿，資料確認後可切換為上架，不顯示前台切換成下架)</span>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="長/ 寬/ 高(mm)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="CarSize" runat="server" Text='<%# Bind("CarSize") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="CarSize" runat="server" Text='<%# Bind("CarSize") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="軸距(mm)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Wheelbase" runat="server" Text='<%# Bind("Wheelbase") %>' MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Wheelbase" runat="server" Text='<%# Bind("Wheelbase") %>' MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="輪距前/ 後(mm)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Track" runat="server" Text='<%# Bind("Track") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Track" runat="server" Text='<%# Bind("Track") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="行李廂空間(L)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Luggage" runat="server" Text='<%# Bind("Luggage") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="150"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Luggage" runat="server" Text='<%# Bind("Luggage") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="150"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="重量(kg)<1" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Weight" runat="server" Text='<%# Bind("Weight") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Weight" runat="server" Text='<%# Bind("Weight") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="型式" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Type" runat="server" Text='<%# Bind("Type") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Type" runat="server" Text='<%# Bind("Type") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="排氣量 (c.c.)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="CC" runat="server" Text='<%# Bind("CC") %>' MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="CC" runat="server" Text='<%# Bind("CC") %>' MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="最大馬力 (hp/rpm)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Hp" runat="server" Text='<%# Bind("Hp") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Hp" runat="server" Text='<%# Bind("Hp") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="最大扭力(kgm/rpm)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Kgm" runat="server" Text='<%# Bind("Kgm") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Kgm" runat="server" Text='<%# Bind("Kgm") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="壓縮比" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Compression" runat="server" Text='<%# Bind("Compression") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Compression" runat="server" Text='<%# Bind("Compression") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="缸徑/ 衝程 (mm/ mm)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Stroke" runat="server" Text='<%# Bind("Stroke") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Stroke" runat="server" Text='<%# Bind("Stroke") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="0-100 km/h 加速(sec)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Acceleration" runat="server" Text='<%# Bind("Acceleration") %>' MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Acceleration" runat="server" Text='<%# Bind("Acceleration") %>' MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="極速(km/h)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="ExtremeSpeed" runat="server" Text='<%# Bind("ExtremeSpeed") %>' MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="ExtremeSpeed" runat="server" Text='<%# Bind("ExtremeSpeed") %>' MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="使用油料" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Oil" runat="server" Text='<%# Bind("Oil") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Oil" runat="server" Text='<%# Bind("Oil") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="油箱容量( 公升)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="FuelTank" runat="server" Text='<%# Bind("FuelTank") %>' MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="FuelTank" runat="server" Text='<%# Bind("FuelTank") %>' MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="傳動方式" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Transfer" runat="server" Text='<%# Bind("Transfer") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="100"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Transfer" runat="server" Text='<%# Bind("Transfer") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="100"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="變速箱" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Transmission" runat="server" Text='<%# Bind("Transmission") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Transmission" runat="server" Text='<%# Bind("Transmission") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="轉向系統" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Steering" runat="server" Text='<%# Bind("Steering") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Steering" runat="server" Text='<%# Bind("Steering") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="煞車系統" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Brake" runat="server" Text='<%# Bind("Brake") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Brake" runat="server" Text='<%# Bind("Brake") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="懸吊系統" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Suspension" runat="server" Text='<%# Bind("Suspension") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="Suspension" runat="server" Text='<%# Bind("Suspension") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="輪胎尺寸" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                         <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Tire" runat="server" Text='<%# Bind("Tire") %>'  MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                         <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Tire" runat="server" Text='<%# Bind("Tire") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="最小迴轉半徑(M)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="MinSlewing" runat="server" Text='<%# Bind("MinSlewing") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="MinSlewing" runat="server" Text='<%# Bind("MinSlewing") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="年耗油量( 公升)<2" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="AnnualFuel" runat="server" Text='<%# Bind("AnnualFuel") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="AnnualFuel" runat="server" Text='<%# Bind("AnnualFuel") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="非市區耗油量( 公里/ 公升)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="NonUrbanFuel" runat="server" Text='<%# Bind("NonUrbanFuel") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="NonUrbanFuel" runat="server" Text='<%# Bind("NonUrbanFuel") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="市區耗油量( 公里/ 公升)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="UrbanFuel" runat="server" Text='<%# Bind("UrbanFuel") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="UrbanFuel" runat="server" Text='<%# Bind("UrbanFuel") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="測試值( 公里/ 公升)<3" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="TestValue" runat="server" Text='<%# Bind("TestValue") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="TestValue" runat="server" Text='<%# Bind("TestValue") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="能源效率等級" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="EfficiencyRating" runat="server" Text='<%# Bind("EfficiencyRating") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="EfficiencyRating" runat="server" Text='<%# Bind("EfficiencyRating") %>' MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="車輛保護文字" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="CarProtectText" runat="server" Text='<%# Bind("CarProtectText") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="CarProtectText" runat="server" Text='<%# Bind("CarProtectText") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="最後修改人員" HeaderStyle-CssClass="col-md-2" InsertVisible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-6 nopadding">
                                                            <asp:Label ID="Username" runat="server" Text='<%# Eval("Username") %>' ReadOnly="true" CssClass="col-md-6 nopadding"></asp:Label>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="最後修改日期" HeaderStyle-CssClass="col-md-2" InsertVisible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-6 nopadding">
                                                            <asp:Label ID="MDate" runat="server" Text='<%# Eval("MDate") %>' ReadOnly="true" CssClass="col-md-6 nopadding"></asp:Label>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="建立日期" HeaderStyle-CssClass="col-md-2" Visible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-6 nopadding">
                                                            <asp:Label ID="CDate" runat="server" Text='<%# Eval("CDate") %>' ReadOnly="true" CssClass="col-md-6 nopadding"></asp:Label>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                            </Fields>
                                        </asp:DetailsView>
                                    </div>
                                    <div class="panel-body tab-pane fade" id="menuCarEquip" runat="server" name="menuCarEquip">
                                        <div>
                                            <div class="row">
                                                <div style="text-align: right">
                                                    <asp:Button runat="server" Text="確定修改" ID="newEquipUpdate" OnClick="btnEquip_Click" CssClass="btn btn-primary btn-xs" />
                                                    <asp:Button runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <asp:DropDownList ID="qEquipType" runat="server"
                                                            DataTextField="EquipCategoryName" DataValueField="EquipCategoryID" DataSourceID="ObjectDataSource3" OnSelectedIndexChanged="EquipType_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" Width="350px">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-6" runat="server" id="newEquipImport">
                                                        <div class="col-md-4">
                                                            <span class="control-fileupload">
                                                                <asp:FileUpload ID="ImportEquipExl" runat="server" />
                                                                <label>批次匯入檔案 檔案格式:.xls .xlsx</label>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:Button runat="server" Text="匯入檔案" OnClick="Import_Click" CausesValidation="false" CssClass="btn btn-primary btn-xs" />
                                                        </div>
                                                        <div class="col-md-1">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:Button ID="TempEquipBtn" runat="server" Text="修改已上傳配備" OnClick="GoToTemp_Click" CausesValidation="false" CssClass="btn btn-primary btn-xs" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:GridView ID="GridViewEquip" DataSourceID="ObjectDataSourceEquip" AllowPaging="false" OnRowDataBound="GridViewEquip_RowDataBound" runat="server" AutoGenerateColumns="false" GridLines="None" DefaultMode="Edit" CssClass="table">
                                            <Columns>
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <div class="col-md-2 nopadding">
                                                            <asp:Label ID="EquipId" runat="server" Text='<%# Bind("EquipId") %>' ReadOnly="true"></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:Label ID="EquipCategoryName" runat="server" Text='<%# Bind("EquipCategoryName") %>' ReadOnly="true"></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-CssClass="col-md-6 ">
                                                    <ItemTemplate>
                                                        <asp:Label ID="EquipType" runat="server" Text='<%# Eval("EquipType").ToString().Trim() %>' Visible="false"></asp:Label>
                                                        <asp:RadioButtonList ID="rEquipType" runat="server" SelectedValue='<%# Eval("EquipType").ToString().Trim() %>' RepeatColumns="3">
                                                            <asp:ListItem Value="" Text="無"></asp:ListItem>
                                                            <asp:ListItem Value="S" Text="標準配備"></asp:ListItem>
                                                            <asp:ListItem Value="E" Text="額外配備"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        <label>配備說明文字</label>
                                                        <asp:TextBox ID="EquipExtraDetail" runat="server" Text='<%# Bind("EquipExtraDetail") %>' TextMode="MultiLine" Width="100%" Height="60px" MaxLength="100"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-CssClass="col-md-6">
                                                    <ItemTemplate>
                                                        <div class="col-md-8" style="top: 15px; display: inline-block;">
                                                            <asp:Label ID="EquipName" runat="server" Text='<%# Bind("EquipName") %>' ReadOnly="true"></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <!-- panel-body -->
                        </form>
                    </div>
                    <!-- panel-default -->

                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBCar" SelectMethod="GetListBySerno" InsertMethod="Insert" UpdateMethod="Update" OnInserted="ObjectDataSource1_Inserted">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="CarId" QueryStringField="CarId" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="Backend_DBCar"
                        SelectMethod="GetCarTypeList">
                        <SelectParameters>
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" TypeName="Backend_DBEquip"
                        SelectMethod="GetEquipTypeList">
                        <SelectParameters>
                        </SelectParameters>
                    </asp:ObjectDataSource>

                    <asp:ObjectDataSource ID="ObjectDataSourceEquip" runat="server" TypeName="Backend_DBEquip" SelectMethod="GetListBySerno" UpdateMethod="Update">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="CarId" QueryStringField="CarId" Type="String" />
                            <asp:ControlParameter ControlID="qEquipType" Name="EquipCategorId" PropertyName="SelectedValue" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                </div>
            </div>
            <!-- row -->
        </div>
        <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>

    <uc1:uc_foot runat="server" ID="uc_foot" />
</body>
</html>


