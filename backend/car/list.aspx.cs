﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_Car_list : BackendBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {


    }

    protected void btnInsert_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("edit.aspx");
    }


    protected void drpShowCount_SelectedIndexChanged(object sender, EventArgs e)
    {
        //在GridView.PageIndex 無法正確被計算出的情況，切換顯示數目會有問題，所以切換至第一頁就沒問題
        DropDownList ddlpagesize = (DropDownList)sender;
        GridView1.PageSize = Convert.ToInt16(ddlpagesize.SelectedValue);
        GridView1.PageIndex = 0;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            // 取得控制項
            GridView gv = sender as GridView;
            PlaceHolder placeholder = e.Row.FindControl("placeholderPager") as PlaceHolder;

            //一次要顯示的頁數
            int showRange = 5;
            //分頁的數量
            int pageCount = gv.PageCount;
            //目前頁碼 (起始值為0)
            int pageIndex = gv.PageIndex;
            //顯示的第一個頁碼
            int startIndex = (pageIndex + 1 < showRange) ?
                0 : (pageIndex + 1 + showRange / 2 >= pageCount) ? pageCount - showRange : pageIndex - showRange / 2;
            //顯示的最後一個頁碼
            int endIndex = (startIndex >= pageCount - showRange) ? pageCount : startIndex + showRange;

            LinkButton lbtnfirst = new LinkButton();
            LinkButton lbtnlast = new LinkButton();

            LinkButton lbtnprev = new LinkButton();
            LinkButton lbtnnext = new LinkButton();

            LinkButton lbtnpage;
            TableRow tr;
            TableCell td;

            Table table = new Table();

            //新增第一頁 Start
            lbtnfirst.CommandName = "Page";
            lbtnfirst.CommandArgument = "1";
            lbtnfirst.Text = "&lt;&lt;";
            //新增第一頁 End

            //新增上一頁 Start
            lbtnprev.Text = "&lt;";
            //新增上一頁 End

            #region 往前處理 (可否點選以及加入至Table)
            if (gv.PageIndex > 0)
            {
                lbtnprev.Click += (obj, args) =>
                {
                    gv.PageIndex = gv.PageIndex - 1;
                };
            }
            else
            {
                lbtnprev.Enabled = false;
                lbtnprev.CssClass = "disabled";
                lbtnfirst.Enabled = false;
                lbtnfirst.CssClass = "disabled";
            }

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnfirst);
            tr.Cells.Add(td);
            table.Rows.Add(tr);

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnprev);
            tr.Cells.Add(td);
            table.Rows.Add(tr);
            #endregion

            #region 新增頁碼數字
            for (int i = startIndex; i < endIndex; i++)
            {
                lbtnpage = new LinkButton();
                lbtnpage.Text = (i + 1).ToString();
                lbtnpage.CommandName = "Page";
                lbtnpage.CommandArgument = (i + 1).ToString();
                lbtnpage.Font.Overline = false;
                if (i == pageIndex)
                {
                    lbtnpage.Font.Bold = true;
                    lbtnpage.Enabled = false;
                }
                else
                {
                    lbtnpage.Font.Bold = false;
                }

                tr = new TableRow();
                td = new TableCell();

                td.Controls.Add(lbtnpage);
                tr.Cells.Add(td);
                table.Rows.Add(tr);
            }
            #endregion

            //新增下一頁 Start            
            lbtnnext.Text = "&gt;";
            //新增下一頁 End

            //新增末頁 Start
            lbtnlast.CommandName = "Page";
            lbtnlast.CommandArgument = GridView1.PageCount.ToString();
            lbtnlast.Text = "&gt;&gt;";
            //新增末頁 End

            #region 往後處理 (可否點選以及加入至Table)
            if (gv.PageIndex < gv.PageCount)
            {
                lbtnnext.Click += (obj, args) =>
                {
                    gv.PageIndex = gv.PageIndex + 1;
                };
            }

            if (gv.PageIndex + 1 == gv.PageCount)
            {
                lbtnnext.Enabled = false;
                lbtnnext.CssClass = "disabled";
                lbtnlast.Enabled = false;
                lbtnlast.CssClass = "disabled";
            }

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnnext);
            tr.Cells.Add(td);
            table.Rows.Add(tr);

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnlast);
            tr.Cells.Add(td);
            table.Rows.Add(tr);
            #endregion

            // 動態加入控制項
            //phdPageNumber.Controls.Add(
            //    new LiteralControl(string.Format("總頁數  {0} / {1}", pageIndex + 1, pageCount)));
            placeholder.Controls.Add(table);
        }
    }

    protected void ObjectDataSource1_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        //依序進入的事件
        //SelectMethod
        //SelectCountMethod
        //利用型別判斷將第一次非數字的回傳值繞過
        if (e.ReturnValue.GetType() != typeof(DataTable))
        {
            string intCount = e.ReturnValue.ToString();
            lblTotalCount.Text = "共" + intCount + "筆";
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
    }
    protected void GridView1_PreRender(object sender, EventArgs e)
    {
        //為了產生 Table 的 thead
        GridView1.UseAccessibleHeader = true;
        if (GridView1.HeaderRow != null)
        {
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    protected void CarType_DataBound(object sender, EventArgs e)
    {
        var ctrl = (DropDownList)sender;
        ctrl.Items.Insert(0, new ListItem(" ", ""));
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        // 僅能針對銷售中的車輛才能成為精選
    }



    //列表頁修改上下架狀態
    protected void StockStatusBtn_Click(object sender, EventArgs e)
    {
        GridViewRow dr = ((Button)sender).Parent.Parent as GridViewRow;
        string CarId = (dr.FindControl("CarId") as Label).Text;

        string OldStockStatus = (dr.FindControl("StockStatus") as Label).Text;


        if (string.IsNullOrEmpty(CarId) || string.IsNullOrEmpty(OldStockStatus))
        {
            JSOutPut("資料錯誤，請進入內頁編輯。");
            return;
        }

        //提交
        if (!string.IsNullOrEmpty(sys_msg))
        {
            JSOutPut(sys_msg);
            GridView1.DataBind();
        }
        else
        {
            if (OldStockStatus.Equals("草稿"))
            {
                Backend_DBCar.UpdateStatusBtn(CarId, "S", "StockStatus");
            }
            else if (OldStockStatus.Equals("上架"))
            {
                Backend_DBCar.UpdateStatusBtn(CarId, "D", "StockStatus");
            }
            else if (OldStockStatus.Equals("下架"))
            {
                Backend_DBCar.UpdateStatusBtn(CarId, "S", "StockStatus");
            }
            JSOutPut("修改成功");
            GridView1.DataBind();
        }


    }
}
