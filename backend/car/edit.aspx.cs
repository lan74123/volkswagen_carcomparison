﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.OpenXml4Net.OPC;
using NPOI.SS.UserModel;
using System.Collections;
using System.ComponentModel;

public partial class backend_Car_edit : BackendBasePage
{
    [Serializable()]
    protected class CarEquipImport
    {
        public string CarID { get; set; }
        public string EquipType { get; set; }
        public string EquipName { get; set; }
        public string EquipTempSelect { get; set; }
        public string EquipTempImport { get; set; }
        public string EquipExtraDetail { get; set; }
        public int IsUpdate { get; set; }

    }

    public string html_panelTitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Request.Params["CarId"]))
            {
                //---------------------------------------------------------新增
                html_panelTitle = "新增";
                qSRNValid.Text = null;
                DetailsView1.ChangeMode(DetailsViewMode.Insert);

                //判斷前端頁面顯示
                carEqPage.Attributes["class"] = "";
                carEqPage.Style.Add("visibility", "hidden");

                carDataPage.Attributes["class"] = "active";
                home.Attributes["class"] = "panel-body tab-pane fade in active";

            }
            else
            {
                //---------------------------------------------------------編輯
                html_panelTitle = "編輯";
                qSRNValid.Text = null;
                DetailsView1.ChangeMode(DetailsViewMode.Edit);

                int haveTemp = Backend_DBEquip.CountEquipTempByCarID(Request.Params["CarId"]);
                if (haveTemp > 0)
                {
                    TempEquipBtn.Visible = true;
                }
                else
                {
                    TempEquipBtn.Visible = false;
                }

            }
        }
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("list.aspx");
    }

    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;

            //控制項
            Backend_DBOperator DBOperator = new Backend_DBOperator();
            DropDownList StockStatus = (DropDownList)DetailsView1.FindControl("qStockStatus");
            DropDownList CarTypeID = (DropDownList)DetailsView1.FindControl("qCarTypeID");
            Label CarId = (Label)DetailsView1.FindControl("CarId");
            CheckBox ShowHomePage = (CheckBox)DetailsView1.FindControl("ShowHomePage");
            FileUpload FileUploadCarImg = (FileUpload)DetailsView1.FindControl("FileUploadCarImg");

            //變數

            string carId = CarId.Text;
            string CarName = ValidateHelper.FilteXSSValue(e.NewValues["CarName"] as string);
            string corder = ValidateHelper.FilteXSSValue(e.NewValues["COrder"] as string);

            //檢查      

            string stock = StockStatus.SelectedValue;


            //必填欄位檢查            

            if (string.IsNullOrEmpty(CarName))
            {
                sys_msg += "請輸入車名\\n";
            }
            else
            {

            }
            if (ShowHomePage.Checked)
            {
                e.NewValues["ShowHomePage"] = "1";
            }
            else
            {
                e.NewValues["ShowHomePage"] = "0";
            }

            if (string.IsNullOrEmpty(corder))
            {
                if (ValidateHelper.IsNumber(corder))
                {

                }
                else {
                    sys_msg += "請輸入「排序」數字!\\r";
                }

                sys_msg += "請輸入「排序」!\\r";
            }

            //草稿&下架不檢查

            //提交
            if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {

                e.NewValues["StockStatus"] = StockStatus.SelectedValue;
                e.NewValues["CarTypeID"] = CarTypeID.SelectedValue;

                string folderPath = Server.MapPath("~/fileupload/car/");
                string fileName = string.Empty;
                string format_img = ".jpg,.jpeg,.png";
                #region 檔案處理
                //上傳檔案

                e.NewValues["CarImg"] = e.OldValues["CarImg"];
                if (FileUploadCarImg.HasFile && string.IsNullOrEmpty(sys_msg))
                {
                    string ext = System.IO.Path.GetExtension(FileUploadCarImg.FileName);

                    string imgfileName = carId + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "" + ext;
                    sys_msg = FileHelper.UploadFile(FileUploadCarImg, folderPath, format_img, imgfileName);
                   // sys_msg = FileHelper.UploadFileResize(FileUploadCarImg, folderPath, format_img, imgfileName, 1055, 0, (int)redrawImgPlace.TopLeft);
                    string ImgUrl = "/fileupload/car/" + imgfileName;
                    e.NewValues["CarImg"] = ImgUrl;

                    if (!string.IsNullOrEmpty(sys_msg))
                    {
                        e.NewValues["CarImg"] = e.OldValues["CarImg"];
                        JSOutPut(sys_msg);
                    }
                    else
                    {
                        e.NewValues["CarImg"] = ImgUrl;
                    }

                }

                #endregion


            }

        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInEditMode = true;
            JSOutPut("修改成功!");

            //判斷前端頁面顯示
            carEqPage.Attributes["class"] = "";
            menuCarEquip.Attributes["class"] = "panel-body tab-pane fade";

            carDataPage.Attributes["class"] = "active";
            home.Attributes["class"] = "panel-body tab-pane fade in active";
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;
            Backend_DBOperator DBOperator = new Backend_DBOperator();

            //控制項 
            DropDownList StockStatus = (DropDownList)DetailsView1.FindControl("qStockStatus");
            DropDownList CarTypeID = (DropDownList)DetailsView1.FindControl("qCarTypeID");
            Label CarId = (Label)DetailsView1.FindControl("CarId");
            CheckBox ShowHomePage = (CheckBox)DetailsView1.FindControl("ShowHomePage");
            FileUpload FileUploadCarImg = (FileUpload)DetailsView1.FindControl("FileUploadCarImg");

            //變數

            string carId = CarId.Text;
            string CarName = ValidateHelper.FilteXSSValue(e.Values["CarName"] as string);
            string corder = ValidateHelper.FilteXSSValue(e.Values["COrder"] as string);

            //檢查      

            string stock = StockStatus.SelectedValue;


            //必填欄位檢查            
            if (ShowHomePage.Checked)
            {
                e.Values["ShowHomePage"] = "1";
            }
            else
            {
                e.Values["ShowHomePage"] = "0";
            }
            if (string.IsNullOrEmpty(CarName))
            {
                sys_msg += "請輸入車名\\n";
            }
            else
            {

            }
            if (string.IsNullOrEmpty(corder))
            {
                if (ValidateHelper.IsNumber(corder))
                {

                }
                else
                {
                    sys_msg += "請輸入「排序」數字!\\r";
                }

                sys_msg += "請輸入「排序」!\\r";
            }

            //提交
            if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
                e.Values["CarTypeID"] = CarTypeID.SelectedValue;
                e.Values["StockStatus"] = StockStatus.SelectedValue;
                e.Values["CarImg"] = "";

            }
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInInsertMode = true;
            JSOutPutAndRedirect("新增成功!", "list.aspx");
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        //因為textmode為password時，資料沒辦法bind，所以靠一個隱藏的塞值  
        string mode = DetailsView1.CurrentMode.ToString();
        if (mode != "Insert")
        {
            var dt1 = (DataView)ObjectDataSource1.Select();
            if (dt1 != null)
            {
                if (dt1.Count < 1)
                {
                    JSOutPutAndRedirect("查無資料!", "list.aspx");
                }
            }
        }
        else
        {
            if (!IsRefresh)
            {  //新增模式新進車售價預設0

            }
        }

        if (mode != "Insert")
        {
            //順序不可逆 草稿 -> 上架 -> 下架
            DropDownList StockStatus = ((DropDownList)DetailsView1.FindControl("qStockStatus"));
            string StockStatusText = StockStatus.SelectedValue.ToString();
            StockStatus.Items.Clear();
            if (StockStatusText == "P")
            {
                StockStatus.Items.Add(new ListItem("草稿", "P"));
                StockStatus.Items.Add(new ListItem("上架", "S"));
            }
            else if (StockStatusText == "S")
            {
                StockStatus.Items.Add(new ListItem("上架", "S"));
                StockStatus.Items.Add(new ListItem("下架", "D"));
            }
            else
            {
                StockStatus.Items.Add(new ListItem("上架", "S"));
                StockStatus.Items.Add(new ListItem("下架", "D"));
            }

            Label Status = ((Label)DetailsView1.FindControl("Status"));
            Button btnOnline = ((Button)DetailsView1.FindControl("btnOnline"));
            if (Status != null)
            {
                if (Status.Text.Equals("O"))
                {
                    btnOnline.Visible = false;
                }
            }
        }

    }

    protected void areaNameAddSpace(object sender, EventArgs e)
    {
    }

    protected void ObjectDataSource1_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        //int retvalue = Convert.ToInt32(e.ReturnValue);
        string carId = Convert.ToString(e.ReturnValue);
        if (string.IsNullOrEmpty(carId))
        {
            JSOutPutAndRedirect("檔案上傳失敗，請重新上傳。", "list.aspx");
            return;
        }

        //控制項
        FileUpload FileUploadCarImg = (FileUpload)DetailsView1.FindControl("FileUploadCarImg");

        #region 檔案處理

        string folderPath = Server.MapPath("~/fileupload/car/");
        string fileName = string.Empty;
        string format_img = ".jpg,.jpeg,.png";

        //上傳檔案
        if (FileUploadCarImg.HasFile && string.IsNullOrEmpty(sys_msg))
        {
            string ext = System.IO.Path.GetExtension(FileUploadCarImg.FileName);

            string imgfileName = carId + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "" + ext;
            sys_msg = FileHelper.UploadFile(FileUploadCarImg, folderPath, format_img, imgfileName);
            //sys_msg = FileHelper.UploadFileResize(FileUploadCarImg, folderPath, format_img, imgfileName, 1055, 0, (int)redrawImgPlace.TopLeft);
            string ImgUrl = "/fileupload/car/" + imgfileName;
            if (!string.IsNullOrEmpty(sys_msg))
            {
                JSOutPut(sys_msg);
            }
            else
            {
                Backend_DBCar.InsertUploadImg(carId, "CarImg", ImgUrl);
            }
        }

        JSOutPutAndRedirect("新增成功!", "edit.aspx?CarId=" + carId);

        #endregion

    }


    protected void GridViewImg_DataBound(object sender, EventArgs e)
    {
        if (IsPostBack)
        {

            JSOutPut("修改成功!");

        }

    }

    protected void btnEquip_Click(object sender, EventArgs e)
    {

        foreach (GridViewRow row in GridViewEquip.Rows)
        {
            Label oEquipType = (Label)row.FindControl("EquipType");
            RadioButtonList nEquipType = (RadioButtonList)row.FindControl("rEquipType");
            Label equipId = (Label)row.FindControl("EquipId");
            Label carId = (Label)DetailsView1.FindControl("CarId");
            TextBox equipExtraDetailBox = (TextBox)row.FindControl("EquipExtraDetail");

            string oldEquipType = oEquipType.Text;
            string newEquipType = nEquipType.SelectedValue.ToString();
            string EquipId = equipId.Text;
            string CarId = carId.Text;
            string equipExtraDetail = equipExtraDetailBox.Text;

            if (newEquipType.Equals(""))
            {//delete
                Backend_DBEquip.Delete(EquipId, CarId);
                Backend_DBCar.UpdateStatus(CarId, "T", "Status");
            }
            else
            {//insert or update 
                if (equipExtraDetail.Length > 0)
                {
                    string aa = equipExtraDetail;
                }
                Backend_DBEquip.InsertOrUpdate(CarId, EquipId, newEquipType, equipExtraDetail);
                Backend_DBCar.UpdateStatus(CarId, "T", "Status");
            }

        }

        if (IsPostBack)
        {
            Label Status = ((Label)DetailsView1.FindControl("Status"));
            Button btnOnline = ((Button)DetailsView1.FindControl("btnOnline"));
            if (Status != null)
            {
                if (Status.Text.Equals("T"))
                {
                    btnOnline.Visible = true;
                }
            }
            JSOutPut("修改成功!");
        }

    }
    protected void GridViewEquip_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //新配備匯入只給指定日期之後新增的車用
        Label CDate = (Label)DetailsView1.FindControl("CDate");

    }
    protected void EquipType_SelectedIndexChanged(object sender, EventArgs e)
    {

        foreach (GridViewRow row in GridViewEquip.Rows)
        {
            Dictionary<string, object> datas = new Dictionary<string, object>();
            Label oEquipType = (Label)row.FindControl("EquipType");
            RadioButtonList nEquipType = (RadioButtonList)row.FindControl("rEquipType");
            Label equipId = (Label)row.FindControl("EquipId");
            Label carId = (Label)DetailsView1.FindControl("CarId");
            TextBox equipExtraDetailBox = (TextBox)row.FindControl("EquipExtraDetail");

            string oldEquipType = oEquipType.Text;
            string newEquipType = nEquipType.SelectedValue.ToString();
            string EquipId = equipId.Text;
            string CarId = carId.Text;
            string equipExtraDetail = equipExtraDetailBox.Text;

            datas.Add(EquipId, oEquipType + "," + nEquipType);

            if (!oldEquipType.Equals(newEquipType))
            {

                if (newEquipType.Equals(""))
                {//delete
                    Backend_DBEquip.Delete(EquipId, CarId);
                    Backend_DBCar.UpdateStatus(CarId, "T", "Status");
                }
                else
                {//insert or update 
                    Backend_DBEquip.InsertOrUpdate(CarId, EquipId, newEquipType, equipExtraDetail);
                    Backend_DBCar.UpdateStatus(CarId, "T", "Status");
                }
            }
        }

        GridViewEquip.DataBind();

        //判斷前端頁面顯示
        carEqPage.Attributes["class"] = "active";
        menuCarEquip.Attributes["class"] = "panel-body tab-pane fade in active";

        carDataPage.Attributes["class"] = "";
        home.Attributes["class"] = "panel-body tab-pane fade";


    }
    protected void CarTypeID_DataBound(object sender, EventArgs e)
    {
        Label CarTypeID = (Label)DetailsView1.FindControl("CarTypeNameLabel");
        DropDownList ctrl = (DropDownList)sender;

        if (CarTypeID != null)
        {
            foreach (ListItem l in ctrl.Items)
            {
                if (l.Value.Equals(CarTypeID.Text))
                {
                    ctrl.SelectedValue = CarTypeID.Text;
                }
            }
        }

    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Label carId = (Label)DetailsView1.FindControl("CarId");
        Backend_DBCar.UpdateValid(Convert.ToInt32(carId.Text));
        JSOutPutAndRedirect("刪除成功!", "list.aspx");
    }

    protected void GoToTemp_Click(object sender, EventArgs e)
    {
        Label CarId = (Label)DetailsView1.FindControl("CarId");

        JSRedirect("editequip.aspx?CarId=" + CarId.Text);
    }

    protected void Import_Click(object sender, EventArgs e)
    {
        if (ImportEquipExl.HasFile)
        {
            string fileFormat = Path.GetExtension(ImportEquipExl.FileName).ToLower().TrimEnd('.');
            bool formatFlag = ValidateHelper.IsValidateFormat(fileFormat, ".xls,.xlsx");
            if (!formatFlag)
            {
                JSOutPut("（支持格式：.xls,.xlsx) \\r");
            }
            else
            {

                Label CarId = (Label)DetailsView1.FindControl("CarId");

                try
                {
                    if (!string.IsNullOrEmpty(CarId.Text))
                    {
                        List<CarEquipImport> lstCarEquip = updalodExcel(ImportEquipExl, CarId.Text);
                        DataTable data = ConvertToDataTable(lstCarEquip);

                        Backend_DBEquip.DeleteEquipTempByCarID(CarId.Text);
                        string errMsg = Backend_DBEquip.ImportEquipTemp(data);
                        if (string.IsNullOrEmpty(errMsg))
                        {
                            JSOutPutAndRedirect("上傳成功，請進行編輯。", "editequip.aspx?CarId=" + CarId.Text);
                        }
                        else
                        {
                            JSOutPut(errMsg);
                        }
                    }

                }
                catch (Exception ex)
                {

                    JSOutPut(ex.Message);
                }

            }

        }
        else
        {
            JSOutPut("未上傳檔案");
        }
    }

    protected List<CarEquipImport> updalodExcel(FileUpload ImportEquipExl, string CarId)
    {
        string ext = System.IO.Path.GetExtension(ImportEquipExl.FileName);

        IWorkbook hssfworkbook = null;

        // 2007版本
        if (ext.Equals(".xlsx"))
        {
            hssfworkbook = new XSSFWorkbook(ImportEquipExl.PostedFile.InputStream);
        }
        // 2003版本
        else if (ext.Equals(".xls"))
        {
            hssfworkbook = new HSSFWorkbook(ImportEquipExl.PostedFile.InputStream);
        }

        List<CarEquipImport> lstCarEquip = new List<CarEquipImport>();

        DataTable dt = new DataTable();
        ISheet sheet = hssfworkbook.GetSheetAt(0);
        IRow headerRow = sheet.GetRow(0);
        IEnumerator rows = sheet.GetRowEnumerator();

        int colCount = headerRow.LastCellNum;
        int rowCount = sheet.LastRowNum;

        CarEquipImport c = new CarEquipImport() { };
        c.CarID = CarId;
        c.EquipType = (ToObjString(headerRow.GetCell(0)).Trim() == "+") ? "E" : "S";
        c.EquipExtraDetail = ToObjString(headerRow.GetCell(1)).Trim();
        c.EquipName = ToObjString(headerRow.GetCell(2)).Trim();
        c.EquipTempSelect = "0";
        c.EquipTempImport = "N";
        c.IsUpdate = 0;
        lstCarEquip.Add(c);

        bool skipReadingHeaderRow = rows.MoveNext();
        while (rows.MoveNext())
        {
            IRow row = (IRow)rows.Current;
            DataRow dr = dt.NewRow();
            c = new CarEquipImport() { };
            c.CarID = CarId;
            c.EquipType = (ToObjString(row.GetCell(0)).Trim() == "+") ? "E" : "S";
            c.EquipExtraDetail = ToObjString(row.GetCell(1)).Trim();
            c.EquipName = ToObjString(row.GetCell(2)).Trim();
            c.EquipTempSelect = "0";
            c.EquipTempImport = "N";
            c.IsUpdate = 0;
            lstCarEquip.Add(c);
        }

        hssfworkbook = null;
        sheet = null;

        return lstCarEquip;
    }

    /// <summary>
    /// 物件轉字串
    /// </summary>
    protected string ToObjString(object o)
    {
        if (o == null || o.ToString() == "undefined") return "";
        return o.ToString().Trim();
    }
    /// <summary>
    /// List物件轉DATATABLE
    /// </summary>
    public DataTable ConvertToDataTable<T>(IList<T> data)
    {
        PropertyDescriptorCollection properties =
           TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        foreach (PropertyDescriptor prop in properties)
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        foreach (T item in data)
        {
            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            table.Rows.Add(row);
        }
        return table;

    }






    protected void submitToOnline(object sender, EventArgs e)
    {
        Label carId = (Label)DetailsView1.FindControl("CarId");
        string carID = carId.Text;

        bool result = Backend_DBCar.uploadToOnline(carID);
        if (result)
        {
            Button btnOnline = ((Button)DetailsView1.FindControl("btnOnline"));
            btnOnline.Visible = false;
            JSOutPut("上傳成功");
        }
        else
        {
            JSOutPut("上傳失敗");
        }
    }
}