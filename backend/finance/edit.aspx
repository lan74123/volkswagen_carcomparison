﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="backend_Finance_edit" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>






<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <script type="text/javascript">
        $(document).ready(function () {
            //solve img chache
            var myimg = document.getElementsByTagName('img');
            for (var i = 0; i < myimg.length; i++) {
                if (myimg[i].src != "") {
                    myimg[i].src = myimg[i].src + "?_" + Date.now();
                }
            }
        });
        //只能輸入數字
        function ValidateNumber(e, pnumber) {
            if (!/^[0-9.]+$/.test(pnumber)) {
                e.value = /^[0-9.]+$/.exec(e.value);
            }
            return false;
        }
        $(function () {
            $("[id$=PromoteStart]").datepicker({ dateFormat: 'yy/mm/dd' });
        });
        $(function () {
            $("[id$=PromoteEnd]").datepicker({ dateFormat: 'yy/mm/dd' });
        });
    </script>
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">
                    <div class="col-md-12">
                        <form id="EditForm" runat="server" class="form-horizontal">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><%= html_panelTitle %></h4>
                                    <asp:Label ID="qSRNValid" runat="server" Visible="false" Text="1"></asp:Label>
                                </div>
                                <ul class="nav nav-tabs" runat="server">
                                    <li class="active" runat="server" id="carDataPage"><a data-toggle="tab" href="#home">分期試算</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="panel-body tab-pane fade in active" id="home" runat="server" name="home">
                                        <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="CarId" CssClass="table"
                                            AutoGenerateRows="false" DefaultMode="Edit" DataSourceID="ObjectDataSource1"
                                            OnItemUpdating="DetailsView1_ItemUpdating" OnItemUpdated="DetailsView1_ItemUpdated"
                                            OnItemInserted="DetailsView1_ItemInserted" OnItemInserting="DetailsView1_ItemInserting"
                                            OnDataBound="DetailsView1_DataBound" GridLines="None">
                                            <Fields>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <div style="text-align: right">
                                                            <asp:Button ID="btnUpdate" runat="server" Text="確定修改" CommandName="Update" CssClass="btn btn-primary btn-xs" />
                                                            <asp:Button ID="btnBackTo" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-CssClass="col-md-2" Visible="false" InsertVisible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:Label ID="CarId" runat="server" Text='<%# Eval("CarId") %>' ReadOnly="true" CssClass="col-md-3 nopadding"></asp:Label>
<%--                                                            <asp:Label ID="Status" runat="server" Text='<%# Bind("Status") %>' ReadOnly="true" CssClass="col-md-3 nopadding"></asp:Label>--%>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="上下架狀態:" HeaderStyle-CssClass="col-md-2" Visible="False">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:DropDownList ID="FinanceStatus" runat="server" SelectedValue='<%# Bind("FinanceStatus") %>' CssClass="form-control input-sm">
                                                                <asp:ListItem Value="1">上架</asp:ListItem>
                                                                <asp:ListItem Value="0">下架</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="車種" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:Label ID="CarTypeNameLabel" runat="server" Text='<%# Eval("CarTypeID").ToString().Trim() %>' Visible="false"></asp:Label>
                                                            <asp:DropDownList ID="qCarTypeID" Enabled="False" runat="server"
                                                                DataTextField="CarTypeName" DataValueField="CarTypeID" OnDataBound="CarTypeID_DataBound" DataSourceID="ObjectDataSource2" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<span style='color:red;'>*</span>車名" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="CarName" ReadOnly="true" runat="server" Text='<%# Eval("CarName") %>' MaxLength="80" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="車圖" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="CarImg" runat="server" Text='<%# Eval("CarImg") %>' hidden="true" type="Number"></asp:TextBox>
                                                        <img src='<%#  ResolveClientUrl ("~")+Eval("CarImg") %>' runat='server' width="530" visible='<%# ((Eval("CarImg").ToString().Length) >1) %>' />
                                                    </EditItemTemplate>
                                                    
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="建議售價" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="MSRP" runat="server" Text='<%# Bind("MSRP") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="最低頭期款" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="Downpayment" runat="server" Text='<%# Bind("Downpayment") %>' MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="利率(ex:2.8%請輸入2.8)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="InterestRate" runat="server" Text='<%# Bind("InterestRate") %>' MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="優惠內容" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="PromoteText" runat="server" Text='<%# Eval("PromoteText").ToString().Replace("<br>", "\r\n") %>' TextMode="MultiLine" Width="100%" Height="150px" CssClass="form-control"></asp:TextBox>
                                                            <%--<asp:TextBox ID="PromoteText" runat="server" Text='<%# Bind("PromoteText") %>' TextMode="MultiLine" Width="100%" Height="150px" CssClass="form-control"></asp:TextBox>--%>

                                                        </div>
                                                    </EditItemTemplate>
                                                    
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="優惠期間" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="PromoteStart" runat="server"  Text='<%# Bind("PromoteStart", "{0:yyyy/MM/dd}") %>' CssClass="form-control input-sm" MaxLength="10" placeholder="優惠開始日期" autocomplete="off"></asp:TextBox>
                                                            <asp:Label ID="Label1" runat="server" Text="至"></asp:Label>
                                                            <asp:TextBox ID="PromoteEnd" runat="server" Text='<%# Bind("PromoteEnd", "{0:yyyy/MM/dd}") %>' CssClass="form-control input-sm" MaxLength="10" placeholder="優惠結束日期" autocomplete="off"></asp:TextBox>          
                                                        </div>
                                                    </EditItemTemplate>
                                                    
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="優惠期間利率(ex:2.8%請輸入2.8)" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="PromoteInterestRate" runat="server" Text='<%# Bind("PromoteInterestRate") %>' MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    
                                                </asp:TemplateField>
                                               

                                                <asp:TemplateField HeaderText="最後修改人員" HeaderStyle-CssClass="col-md-2" InsertVisible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-6 nopadding">
                                                            <asp:Label ID="Username" runat="server" Text='<%# Eval("Username") %>' ReadOnly="true" CssClass="col-md-6 nopadding"></asp:Label>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="最後修改日期" HeaderStyle-CssClass="col-md-2" InsertVisible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-6 nopadding">
                                                            <asp:Label ID="MDate" runat="server" Text='<%# Eval("MDate") %>' ReadOnly="true" CssClass="col-md-6 nopadding"></asp:Label>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="建立日期" HeaderStyle-CssClass="col-md-2" Visible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-6 nopadding">
                                                            <asp:Label ID="CDate" runat="server" Text='<%# Eval("CDate") %>' ReadOnly="true" CssClass="col-md-6 nopadding"></asp:Label>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                            </Fields>
                                        </asp:DetailsView>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- panel-body -->
                        </form>
                    </div>
                    <!-- panel-default -->

                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBFinance" SelectMethod="GetListBySerno" InsertMethod="Insert" UpdateMethod="Update" OnInserted="ObjectDataSource1_Inserted">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="CarId" QueryStringField="CarId" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="Backend_DBCar"
                        SelectMethod="GetCarTypeList">
                        <SelectParameters>
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" TypeName="Backend_DBCar" SelectMethod="GetListBySerno" InsertMethod="Insert" UpdateMethod="Update" OnInserted="ObjectDataSource1_Inserted">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="CarId" QueryStringField="CarId" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>


                </div>
            </div>
            <!-- row -->
        </div>
        <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>

    <uc1:uc_foot runat="server" ID="uc_foot" />
</body>
</html>


