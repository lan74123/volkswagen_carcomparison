﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="backend_Car_list" %>


<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <style>
        /*在bootstrap.min.css 的table margin-bottom設為20px*/
        .table {
            margin-bottom: 0px;
        }

        div.circle_in_Green {
            width: 20px;
            height: 20px;
            border-radius: 15em;
            background-color: #17a08c;
            border-color: #119380;
        }

        div.circle_in_Yellow {
            width: 20px;
            height: 20px;
            border-radius: 15em;
            background-color: #f0ad4e;
            border-color: #eea236;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            (function () {
                //複製Gridview_TopInfo Start
                var $infoBar = $("#gridview_topInfo");
                if (!!$infoBar.length) {
                    $('#gridview_bottomInfo').append($infoBar.clone(true).html());
                }
                //複製Gridview_TopInfo End
            })();
        });

        //複製的情況會造成…Form Data會傳送同name但不同value，會以第一個為主，所以要透過onChange事件修正第一個value
        function showcountChange(obj) {
            $("select[ShowRoomName='drpShowCount']").each(function (i) {
                $(this).val($(obj).val());
            });
        }

        //只能輸入數字
        function ValidateNumber(e, pnumber) {
            if (!/^[0-9.]+$/.test(pnumber)) {
                e.value = /^[0-9.]+$/.exec(e.value);
            }
            return false;
        }

    </script>
    <script language="javascript">
        //參數 a,b ,IsSentToServer:是否傳到後端 
        function ChangeStockStatus(e, IsSentToServer) {


            var msg = "是否要把此筆資料改成" + e.value;


            //是否執行後端執令 
            if (!IsSentToServer) {
                if (confirm(msg)) {
                    event.returnValue = true;
                } else {
                    //下面這行把 Client 事件擋下來， form send 不出去，所以也就不會到後端了           
                    event.returnValue = false;
                }

            }

        }
    </script>
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <form runat="server" id="listForm" class="form-inline">
                    <div class="well well-sm search-panel">
                        <i class="glyphicon glyphicon-search"></i>搜尋<br />
                        <div class="form-group">
                            <label class="">車種:</label>
                            <asp:DropDownList ID="qCarType" runat="server"
                                DataTextField="CarTypeName" DataValueField="CarTypeID" OnDataBound="CarType_DataBound" DataSourceID="ObjectDataSource2" CssClass="form-control input-sm">
                            </asp:DropDownList>
                        </div>
                        <%--隱藏以下篩選--%>
                        <div class="form-group"  style="display:none">
                            <label class="">上下架狀態:</label>
                            <asp:DropDownList ID="qStockStatus" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="">不拘</asp:ListItem>
                                <asp:ListItem Value="P">草稿</asp:ListItem>
                                <asp:ListItem Value="S">上架</asp:ListItem>
                                <asp:ListItem Value="D">下架</asp:ListItem>
                            </asp:DropDownList>
                        </div>
<%--                        <div class="form-group"  style="display:none">
                            <label class="">上傳正式區狀態:</label>
                            <asp:DropDownList ID="qStatus" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="">不拘</asp:ListItem>
                                <asp:ListItem Value="O">已上傳</asp:ListItem>
                                <asp:ListItem Value="T">異動-未上傳</asp:ListItem>
                            </asp:DropDownList>
                        </div>--%>
                        <div class="form-group"  style="display:none">
                            <label class="">預設首頁車款狀態:</label>
                            <asp:DropDownList ID="qShowHomePage" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="">不拘</asp:ListItem>
                                <asp:ListItem Value="1">是</asp:ListItem>
                                <asp:ListItem Value="0">否</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <asp:Button ID="btnQuery" runat="server" CssClass="btn btn-primary btn-sm" Text="查詢" />
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div id="gridview_topInfo">
                                每頁顯示筆數
                                <asp:DropDownList runat="server" ID="drpShowCount" AutoPostBack="True" OnSelectedIndexChanged="drpShowCount_SelectedIndexChanged" onChange="showcountChange(this);">
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                </asp:DropDownList>
                                搜尋資料總筆數
                                <asp:Label runat="server" ID="lblTotalCount"></asp:Label>
                            </div>
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-primary" DataKeyNames="CarId" GridLines="None"
                                    AllowPaging="true" PageSize="10" AllowSorting="true" OnRowCreated="GridView1_RowCreated" OnPageIndexChanging="GridView1_PageIndexChanging" OnPreRender="GridView1_PreRender"
                                    AutoGenerateColumns="false" DataSourceID="ObjectDataSource1" OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound" PagerStyle-CssClass="bs-pagination text-right">
                                    <EmptyDataTemplate>查無資料</EmptyDataTemplate>
                                    <PagerTemplate>
                                        <asp:PlaceHolder ID="placeholderPager" runat="server"></asp:PlaceHolder>
                                    </PagerTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="編號" ControlStyle-CssClass="col-md-2">
                                            <ItemTemplate>
                                                <asp:Label ID="CarId" Text='<%# Eval("CarId").ToString().Trim() %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
<%--                                        <asp:TemplateField HeaderText="上傳正式區狀態" HeaderStyle-CssClass="col-md-1 ">
                                            <ItemTemplate>

                                                <asp:Label ID="Status" runat="server" Text='<%# Eval("Status").ToString().Trim() == "O" ? "<div class=\"circle_in_Green\"></div>": Eval("Status").ToString().Trim() == "T"?"<div class=\"circle_in_Yellow\"></div>":"沒資料" %>'>'</asp:Label><br />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <%--<asp:TemplateField HeaderText="上下架狀態" HeaderStyle-CssClass="col-md-1" >
                                            <ItemTemplate>
                                                <asp:Label ID="FinanceStatus" runat="server" Text='<%# Eval("FinanceStatus").ToString().Trim() == "1" ? "上架": "下架" %>'>'</asp:Label><br />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:BoundField DataField="CarTypeName" HeaderText="車種資料" ControlStyle-CssClass="col-md-1">
                                            <ControlStyle></ControlStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CarName" HeaderText="車名" ControlStyle-CssClass="col-md-1">
                                            <ControlStyle></ControlStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="建議售價" ControlStyle-CssClass="col-md-1">
                                            <ItemTemplate>
                                                <asp:Label ID="MSRP" Text='<%# Eval("MSRP").ToString() == "" ? "":Eval("MSRP").ToString().Trim() %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="最低頭期款" ControlStyle-CssClass="col-md-1">
                                            <ItemTemplate>
                                                <asp:Label ID="Downpayment" Text='<%# Eval("Downpayment").ToString() == "" ? "":Eval("Downpayment").ToString().Trim() %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="利率" ControlStyle-CssClass="col-md-1">
                                            <ItemTemplate>
                                                <asp:Label ID="InterestRate" Text='<%# Eval("InterestRate").ToString() == "" ? "":Eval("InterestRate").ToString().Trim() %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="優惠期間" ControlStyle-CssClass="col-md-2">
                                            <ItemTemplate>
                                                <asp:Label ID="PromotionDate" Text='<%# Eval("PromoteStart", "{0:yyyy/MM/dd}").ToString().Trim() +(Eval("PromoteStart").ToString() == "" ?"":"<br>-<br>")+Eval("PromoteEnd", "{0:yyyy/MM/dd}").ToString().Trim() %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="優惠利率" ControlStyle-CssClass="col-md-1">
                                            <ItemTemplate>
                                                <asp:Label ID="PromoteInterestRate" Text='<%# Eval("PromoteInterestRate").ToString() == "" ? "":Eval("PromoteInterestRate").ToString().Trim() %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        
                                        <asp:TemplateField HeaderText="功能" ShowHeader="false" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="col-md-2">
                                            <ItemTemplate>
                                                <a class="btn btn-warning btn-xs" href='edit.aspx?CarId=<%# Eval("CarId") %>' target="_self">編輯</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="gridview_bottomInfo">
                                <%--用來放尾巴資訊--%>
                            </div>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBCar" EnablePaging="true"
                                MaximumRowsParameterName="maxrows"
                                StartRowIndexParameterName="startrows"
                                SelectMethod="GetFinanceList"
                                SelectCountMethod="GetFinanceCounts"
                                OnSelected="ObjectDataSource1_Selected"
                                UpdateMethod="UpdateValid">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="qCarType" Name="CarTypeId" PropertyName="SelectedValue" Type="String" />
                                    <asp:ControlParameter ControlID="qStockStatus" Name="StockStatus" PropertyName="SelectedValue" Type="String" />
<%--                                    <asp:ControlParameter ControlID="qStatus" Name="Status" PropertyName="SelectedValue" Type="String" />--%>
                                    <asp:ControlParameter ControlID="qShowHomePage" Name="ShowHomePage" PropertyName="SelectedValue" Type="String" />                                    
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="Backend_DBCar"
                                SelectMethod="GetCarTypeList">
                                <SelectParameters>
                                  
                                </SelectParameters>
                            </asp:ObjectDataSource>

                        </div>
                    </div>
                </form>
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>
    <uc1:uc_foot runat="server" ID="uc_foot" />
    <script type="text/javascript" src='<%= ResolveUrl("~/backend/asset/js/bs.pagination.js") %>'></script>
</body>
</html>
