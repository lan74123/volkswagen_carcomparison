﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class backend_login : BackendBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
	protected void btnLogin_Click(object sender, EventArgs e)
	{
		//if (!IsRefresh)
		//{
		COperator user = new COperator();

		string in_user = ValidateHelper.FilteXSSValue(txtAccount.Text.Trim());
		string in_pwd = ValidateHelper.FilteXSSValue(txtPwd.Text.Trim());
		string sys_msg = "";


		if (Backend_DBOperator.VerifyUser(in_user, in_pwd, out sys_msg, out user))
		{
			FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, user.Name, DateTime.Now, DateTime.Now.AddHours(8), true, user.Serno.ToString());
			string encryptedTicket = FormsAuthentication.Encrypt(ticket);
			HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
			authCookie.Expires = ticket.Expiration;
			Response.Cookies.Add(authCookie);
		

			if (Request["ReturnUrl"] == null)
			{
				Response.Redirect("index.aspx");
			}
			else
			{
				Response.Redirect(Request["ReturnUrl"]);
			}
		}
		else
		{
			JSOutPut("帳號或密碼錯誤，請重新輸入");
        }
		//}
	}
}