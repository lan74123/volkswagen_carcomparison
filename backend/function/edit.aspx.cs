﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_function_edit : BackendBasePage
{
    public string html_panelTitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
		int count = Convert.ToInt32(history.Text);
		if (IsPostBack)
		{
			count++;
			history.Text = count.ToString();
		}
		if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Request.Params["serno"]))
            {
                //---------------------------------------------------------新增
                html_panelTitle = "新增";
                DetailsView1.ChangeMode(DetailsViewMode.Insert);
            }
            else
            {
                //---------------------------------------------------------編輯
                html_panelTitle = "編輯";
                DetailsView1.ChangeMode(DetailsViewMode.Edit);
            }
        }
    }

	protected string historyGo()
	{
		return history.Text;
	}

	protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("list.aspx");
    }

    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;

			//控制項
			Backend_DBFunction DBFunction = new Backend_DBFunction();
			TextBox txtclassicon = (TextBox)DetailsView1.FindControl("classicon");
			TextBox txtmenuname = (TextBox)DetailsView1.FindControl("menuname");
			TextBox txtcorder = (TextBox)DetailsView1.FindControl("corder");
            CheckBox cbvalid = (CheckBox)DetailsView1.FindControl("valid");

			//變數
			string in_classicon = ValidateHelper.FilteXSSValue(txtclassicon.Text);
			string in_menuname = ValidateHelper.FilteXSSValue(txtmenuname.Text);
			string in_corder = ValidateHelper.FilteXSSValue(txtcorder.Text);
            string in_valid = cbvalid.Checked ? "1" : "0";

            //檢查
            //必填欄位檢查
            if (string.IsNullOrEmpty(in_classicon))
            {
                sys_msg += "請輸入「圖示名稱」!\\r";
            }
            if (string.IsNullOrEmpty(in_menuname))
            {
                sys_msg += "請輸入「名稱」!\\r";
            }
			if (string.IsNullOrEmpty(in_corder))
			{
				sys_msg += "請輸入「排序」!\\r";
			}
			else if (!ValidateHelper.IsNumber(in_corder))
			{
				sys_msg += "「排序」必須是數字!\\r";
			}

			//提交
			if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
                e.NewValues["classicon"] = in_classicon;
				e.NewValues["menuname"] = in_menuname;
				e.NewValues["corder"] = in_corder;
                e.NewValues["valid"] = in_valid;
            }
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInEditMode = true;
            JSOutPut("修改成功!");
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;
			Backend_DBFunction DBFunction = new Backend_DBFunction();

			//控制項
			TextBox txtclassicon = (TextBox)DetailsView1.FindControl("classicon");
			TextBox txtmenuname = (TextBox)DetailsView1.FindControl("menuname");
			TextBox txtcorder = (TextBox)DetailsView1.FindControl("corder");
			CheckBox cbvalid = (CheckBox)DetailsView1.FindControl("valid");

			//變數
			string in_classicon = ValidateHelper.FilteXSSValue(txtclassicon.Text);
			string in_menuname = ValidateHelper.FilteXSSValue(txtmenuname.Text);
			string in_corder = ValidateHelper.FilteXSSValue(txtcorder.Text);
			string in_valid = cbvalid.Checked ? "1" : "0";

			//檢查
			//必填欄位檢查
			if (string.IsNullOrEmpty(in_classicon))

			{
				sys_msg += "請輸入「圖示名稱」!\\r";
			}
			if (string.IsNullOrEmpty(in_menuname))
			{
				sys_msg += "請輸入「名稱」!\\r";
			}
			if (string.IsNullOrEmpty(in_corder))
			{
				sys_msg += "請輸入「排序」!\\r";
			}
			else if (!ValidateHelper.IsNumber(in_corder))
			{
				sys_msg += "「排序」必須是數字!\\r";
			}

			//提交
			if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
				e.Values["classicon"] = in_classicon;
				e.Values["menuname"] = in_menuname;
				e.Values["corder"] = in_corder;
				e.Values["valid"] = in_valid;
			}
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInInsertMode = true;
            JSOutPutAndRedirect("新增成功!", "list.aspx");
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {

    }
    
}