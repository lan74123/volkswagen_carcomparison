﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="backend_function_list" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <style>
        /*在bootstrap.min.css 的table margin-bottom設為20px*/
        .table {
            margin-bottom: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            (function () {
                //複製Gridview_TopInfo Start
                var $infoBar = $("#gridview_topInfo");
                if (!!$infoBar.length) {
                    $('#gridview_bottomInfo').append($infoBar.clone(true).html());
                }
                //複製Gridview_TopInfo End
            })();
        });

        //複製的情況會造成…Form Data會傳送同name但不同value，會以第一個為主，所以要透過onChange事件修正第一個value
        function showcountChange(obj) {
            $("select[name='drpShowCount']").each(function (i) {
                $(this).val($(obj).val());
            });
        }
    </script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">
                <form runat="server" id="listForm" class="form-inline">
                    <div class="well well-sm search-panel">
                        <i class="glyphicon glyphicon-search"></i>搜尋<br />
                        <div class="form-group">
                            <label class="">有效否:</label>
                            <asp:DropDownList ID="qValid" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="1">是</asp:ListItem>
                                <asp:ListItem Value="0">否</asp:ListItem>
                                <asp:ListItem Value="">不拘</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:Button ID="btnQuery" runat="server" CssClass="btn btn-primary btn-sm" Text="查詢" />
                        <asp:Button ID="btnInsert" runat="server" CssClass="btn btn-success btn-sm pull-right" Text="建立新資料" OnClick="btnInsert_Click" />
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div id="gridview_topInfo">
                                每頁顯示筆數
                                <asp:DropDownList runat="server" ID="drpShowCount" AutoPostBack="True" OnSelectedIndexChanged="drpShowCount_SelectedIndexChanged" onChange="showcountChange(this);">
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                </asp:DropDownList>
                                搜尋資料總筆數
                                <asp:Label runat="server" ID="lblTotalCount"></asp:Label>
                            </div>
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-primary" DataKeyNames="Serno" GridLines="None"
                                    AllowPaging="true" PageSize="10" AllowSorting="true" OnRowCreated="GridView1_RowCreated" OnPageIndexChanging="GridView1_PageIndexChanging" OnPreRender="GridView1_PreRender"
                                    AutoGenerateColumns="false" DataSourceID="ObjectDataSource1" OnRowUpdating="GridView1_RowUpdating" PagerStyle-CssClass="bs-pagination text-right">
                                    <EmptyDataTemplate>查無資料</EmptyDataTemplate>
                                    <PagerTemplate>
                                        <asp:PlaceHolder ID="placeholderPager" runat="server"></asp:PlaceHolder>
                                    </PagerTemplate>
                                    <Columns>
                                        <asp:BoundField DataField="serno" HeaderText="#"></asp:BoundField>
                                        <asp:TemplateField HeaderText="圖示">
                                            <ItemTemplate>
                                                <i class='fa <%# Eval("ClassIcon") %>'></i>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="MenuName" HeaderText="名稱"></asp:BoundField>
                                        <asp:TemplateField HeaderText="有效否">
                                            <ItemTemplate>
                                                <div class="ckbox ckbox-primary">
                                                    <asp:CheckBox ID="fValid" runat="server" Checked='<%# DBHelper.GetTrueOrFalse(Convert.ToString(Eval("Valid"))) %>'></asp:CheckBox>
                                                    <label class="ckbox-label" for=""></label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="cdate" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HeaderText="建立日期"></asp:BoundField>
                                        <asp:BoundField DataField="mdate" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HeaderText="最後修改日期"></asp:BoundField>
                                        <asp:TemplateField HeaderText="操作" ShowHeader="false" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <a class="btn btn-warning btn-xs" href='edit.aspx?serno=<%# Eval("Serno") %>' target="_self">編輯</a>
                                                <a class="btn btn-primary btn-xs" href='children-list.aspx?serno=<%# Eval("Serno") %>' target="_self">編輯子選單</a>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-danger btn-xs" Text="無效" Enabled='<%# DBHelper.GetTrueOrFalse(Eval("Valid").ToString()) %>' OnClientClick='return confirm("您確定要將此筆記錄變成無效嗎?")' CommandName="Update" CausesValidation="true"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="gridview_bottomInfo">
                                <%--用來放尾巴資訊--%>
                            </div>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBFunction" EnablePaging="true"
                                MaximumRowsParameterName="maxrows"
                                StartRowIndexParameterName="startrows"
                                SelectMethod="GetList"
                                SelectCountMethod="GetCounts"
                                OnSelected="ObjectDataSource1_Selected"
                                UpdateMethod="UpdateValid">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="qValid" Name="valid" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </div>
                </form>
                <!-- row -->
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>
    <uc1:uc_foot runat="server" ID="uc_foot" />
    <script type="text/javascript" src='<%= ResolveUrl("~/backend/asset/js/bs.pagination.js") %>'></script>
    <script>
        function pageLoad(sender, args) {
            var cbgroup = $("input[type=checkbox]");
            var lbgroup = $(".ckbox-label");
            var cbcnt = cbgroup.length;
            var lbcnt = lbgroup.length;
            cbgroup.prop("disabled", true);
            if (cbcnt == lbcnt) {
                for (var i = 0; i < cbcnt; i++) {
                    var cbid = cbgroup.eq(i).attr("id");
                    lbgroup.eq(i).attr("for", cbid);
                }
            }
        }
    </script>
</body>
</html>

