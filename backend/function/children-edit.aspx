﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="children-edit.aspx.cs" Inherits="backend_function_children_edit" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>






<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">
                    <div class="col-md-12">
                        <form id="EditForm" runat="server" class="form-horizontal">
                            <asp:Label ID="history" runat="server" Visible="false" Text="1"></asp:Label>
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-10 col-xs-10"><h4 class="panel-title"><%= html_panelTitle %></h4></div>
                                        <div class="col-md-2 col-xs-2" style="text-align: right;"><button class="btn btn-default btn-xs" onclick="history.go((-<%=historyGo() %>))">回查詢</button></div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="serno" CssClass="table"
                                        AutoGenerateRows="false" DefaultMode="Edit" DataSourceID="ObjectDataSource1"
                                        OnItemUpdating="DetailsView1_ItemUpdating" OnItemUpdated="DetailsView1_ItemUpdated"
                                        OnItemInserted="DetailsView1_ItemInserted" OnItemInserting="DetailsView1_ItemInserting"
                                        OnDataBound="DetailsView1_DataBound" GridLines="None">
                                        <Fields>
                                            <asp:BoundField DataField="serno" HeaderText="功能代碼" ReadOnly="True" InsertVisible="False" HeaderStyle-CssClass="col-md-2" />

                                            
                                            <asp:TemplateField HeaderText="名稱" HeaderStyle-CssClass="col-md-2">
                                                <EditItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="funname" runat="server" Text='<%# Bind("FunName") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="funname" runat="server" Text='<%# Bind("FunName") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="連結" HeaderStyle-CssClass="col-md-2">
                                                <EditItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="funlink" runat="server" Text='<%# Bind("FunLink") %>' CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="funlink" runat="server" Text='<%# Bind("FunLink") %>' CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="描述" HeaderStyle-CssClass="col-md-2">
                                                <EditItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="fundesc" runat="server" Text='<%# Bind("FunDesc") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="fundesc" runat="server" Text='<%# Bind("FunDesc") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="排序" HeaderStyle-CssClass="col-md-2">
                                                <EditItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="corder" runat="server" Text='<%# Bind("COrder") %>' MaxLength="20" CssClass="form-control" type="Number"></asp:TextBox>
                                                    </div>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="corder" runat="server" Text='<%# Bind("COrder") %>' MaxLength="20" CssClass="form-control" type="Number"></asp:TextBox>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="有效否">
                                                <EditItemTemplate>
                                                    <div class="onoffswitch">
                                                        <asp:CheckBox ID="valid" runat="server" Checked='<%# Eval("Valid").ToString()=="0" ? false :true %>' />
                                                        <label class="onoffswitch-label" for=""></label>
                                                    </div>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="onoffswitch">
                                                        <asp:CheckBox ID="valid" runat="server" Checked='true' />
                                                        <label class="onoffswitch-label" for=""></label>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CDate" HeaderText="建立日期" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                                ReadOnly="True" InsertVisible="False"></asp:BoundField>
                                            <asp:BoundField DataField="MDate" HeaderText="最後修改日期" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                                ReadOnly="True" InsertVisible="False"></asp:BoundField>
                                            <asp:TemplateField HeaderText="最後修改人員" InsertVisible="false">
                                                <EditItemTemplate>
                                                    <asp:Label ID="fOid" runat="server" Text='<%# Backend_DBOperator.GetNameBySerno(Eval("oid").ToString()) %>' />
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <InsertItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnInsert_foot" runat="server" Text="確定新增" CommandName="Insert" CssClass="btn btn-primary btn-xs" />
                                                        <button class="btn btn-default btn-xs" onclick="history.go((-<%=historyGo() %>))">回查詢</button>
                                                    </div>
                                                </InsertItemTemplate>
                                                <EditItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnUpdate_foot" runat="server" Text="確定修改" CommandName="Update" CssClass="btn btn-primary btn-xs" />
                                                        <button class="btn btn-default btn-xs" onclick="history.go((-<%=historyGo() %>))">回查詢</button>
                                                    </div>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel-default -->

                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBFunction" SelectMethod="GetChildrenListBySerno" InsertMethod="InsertChildren" UpdateMethod="UpdateChildren">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="serno" QueryStringField="serno" Type="String" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:QueryStringParameter Name="serno" QueryStringField="serno" Type="String" />
                                </UpdateParameters>
                                <InsertParameters>
                                    <asp:QueryStringParameter Name="parent" QueryStringField="parent" Type="String" />
                                </InsertParameters>
                            </asp:ObjectDataSource>
                        </form>
                    </div>
                </div>
                <!-- row -->
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>

    <uc1:uc_foot runat="server" ID="uc_foot" />
    <script>
        $(document).ready(function () {
            var cbgroup = $("input[type=checkbox]");
            var lbgroup = $(".onoffswitch-label");
            var cbcnt = cbgroup.length;
            var lbcnt = lbgroup.length;
            cbgroup.addClass("onoffswitch-checkbox");
            if (cbcnt == lbcnt) {
                for (var i = 0; i < cbcnt; i++) {
                    var cbid = cbgroup.eq(i).attr("id");
                    lbgroup.eq(i).attr("for", cbid);
                }
            }
        })
    </script>
</body>
</html>

