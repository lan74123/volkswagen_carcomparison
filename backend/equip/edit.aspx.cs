﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.ComponentModel;

public partial class backend_Equip_edit : BackendBasePage
{
    [Serializable()]
    protected class CarEquipImport
    {
        public string CarID { get; set; }
        public string EquipType { get; set; }
        public string EquipName { get; set; }
        public string EquipTempSelect { get; set; }
        public string EquipTempImport { get; set; }
        public string EquipExtraDetail { get; set; }
        public int IsUpdate { get; set; }

    }

    public string html_panelTitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Request.Params["EquipID"]))
            {
                //---------------------------------------------------------新增
                html_panelTitle = "新增";

                DetailsView1.ChangeMode(DetailsViewMode.Insert);

                //判斷前端頁面顯示
                carDataPage.Attributes["class"] = "active";
                home.Attributes["class"] = "panel-body tab-pane fade in active";

            }
            else
            {
                //---------------------------------------------------------編輯
                html_panelTitle = "編輯";
                DetailsView1.ChangeMode(DetailsViewMode.Edit);

            }
        }
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("list.aspx");
    }

    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;

            //控制項
            Backend_DBOperator DBOperator = new Backend_DBOperator();
            DropDownList EquipCategory = (DropDownList)DetailsView1.FindControl("qEquipCategory");
            Label EquipID = (Label)DetailsView1.FindControl("EquipID");

            //變數
            string equipID = EquipID.Text;         

            //檢查      
            string equipCategory = EquipCategory.SelectedValue;


            //必填欄位檢查          

         
          

            //草稿&下架不檢查

            //提交
            if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
                e.NewValues["EquipCategoryID"] = EquipCategory.SelectedValue;
            }

        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInEditMode = true;
            JSOutPut("修改成功!");

            //判斷前端頁面顯示
            carDataPage.Attributes["class"] = "active";
            home.Attributes["class"] = "panel-body tab-pane fade in active";
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;
            Backend_DBOperator DBOperator = new Backend_DBOperator();

            //控制項 
            DropDownList EquipCategory = (DropDownList)DetailsView1.FindControl("qEquipCategory");
            Label EquipID = (Label)DetailsView1.FindControl("EquipID");

            //變數

            //檢查  

            //必填欄位檢查   
                  
            //提交
            if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
                e.Values["EquipCategoryID"] = EquipCategory.SelectedValue;              

            }
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInInsertMode = true;
            JSOutPutAndRedirect("新增成功!", "list.aspx");
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        //因為textmode為password時，資料沒辦法bind，所以靠一個隱藏的塞值  
        string mode = DetailsView1.CurrentMode.ToString();
        if (mode != "Insert")
        {
            var dt1 = (DataView)ObjectDataSource1.Select();
            if (dt1 != null)
            {
                if (dt1.Count < 1)
                {
                    JSOutPutAndRedirect("查無資料!", "list.aspx");
                }
            }
        }
        else
        {
            if (!IsRefresh)
            {  //新增模式新進車售價預設0

            }
        }

        if (mode != "Insert")
        {
                        
        }

    }

    protected void areaNameAddSpace(object sender, EventArgs e)
    {
    }

    protected void ObjectDataSource1_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        //int retvalue = Convert.ToInt32(e.ReturnValue);
        string carId = Convert.ToString(e.ReturnValue);
        if (string.IsNullOrEmpty(carId))
        {
            JSOutPutAndRedirect("檔案上傳失敗，請重新上傳。", "list.aspx");
            return;
        }         

    }


    protected void GridViewImg_DataBound(object sender, EventArgs e)
    {
        if (IsPostBack)
        {

            JSOutPut("修改成功!");

        }

    }


    protected void EquipCategory_DataBound(object sender, EventArgs e)
    {
        Label EquipCategoryID = (Label)DetailsView1.FindControl("EquipCategoryID");
        DropDownList ctrl = (DropDownList)sender;

        if (EquipCategoryID != null)
        {
            foreach (ListItem l in ctrl.Items)
            {
                if (l.Value.Equals(EquipCategoryID.Text))
                {
                    ctrl.SelectedValue = EquipCategoryID.Text;
                }
            }
        }

    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Label carId = (Label)DetailsView1.FindControl("CarId");
        Backend_DBCar.UpdateValid(Convert.ToInt32(carId.Text));
        JSOutPutAndRedirect("刪除成功!", "list.aspx");
    }

    /// <summary>
    /// 物件轉字串
    /// </summary>
    protected string ToObjString(object o)
    {
        if (o == null || o.ToString() == "undefined") return "";
        return o.ToString().Trim();
    }
    /// <summary>
    /// List物件轉DATATABLE
    /// </summary>
    public DataTable ConvertToDataTable<T>(IList<T> data)
    {
        PropertyDescriptorCollection properties =
           TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        foreach (PropertyDescriptor prop in properties)
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        foreach (T item in data)
        {
            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            table.Rows.Add(row);
        }
        return table;

    }


}