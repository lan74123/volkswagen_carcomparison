﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="backend_Equip_edit" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>






<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <script type="text/javascript">
        $(document).ready(function () {
            //solve img chache
            var myimg = document.getElementsByTagName('img');
            for (var i = 0; i < myimg.length; i++) {
                if (myimg[i].src != "") {
                    myimg[i].src = myimg[i].src + "?_" + Date.now();
                }
            }
        });
        //只能輸入數字
        function ValidateNumber(e, pnumber) {
            if (!/^[0-9.]+$/.test(pnumber)) {
                e.value = /^[0-9.]+$/.exec(e.value);
            }
            return false;
        }
    </script>
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">
                    <div class="col-md-12">
                        <form id="EditForm" runat="server" class="form-horizontal">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><%= html_panelTitle %></h4>                                   
                                </div>
                                <ul class="nav nav-tabs" runat="server">
                                    <li class="active" runat="server" id="carDataPage"><a data-toggle="tab" href="#home">基本資料</a></li>                                  
                                </ul>
                                <div class="tab-content">
                                    <div class="panel-body tab-pane fade in active" id="home" runat="server" name="home">
                                        <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="EquipID" CssClass="table"
                                            AutoGenerateRows="false" DefaultMode="Edit" DataSourceID="ObjectDataSource1"
                                            OnItemUpdating="DetailsView1_ItemUpdating" OnItemUpdated="DetailsView1_ItemUpdated"
                                            OnItemInserted="DetailsView1_ItemInserted" OnItemInserting="DetailsView1_ItemInserting"
                                            OnDataBound="DetailsView1_DataBound" GridLines="None">
                                            <Fields>
                                                <asp:TemplateField>
                                                    <InsertItemTemplate>
                                                        <div style="text-align: right">
                                                            <asp:Button ID="btnInsert" runat="server" Text="確定新增" CommandName="Insert" CssClass="btn btn-primary btn-xs" />
                                                            <asp:Button ID="btnBackTo" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                        </div>
                                                    </InsertItemTemplate>
                                                    <EditItemTemplate>
                                                        <div style="text-align: right">                                                         
                                                            <asp:Button ID="btnUpdate" runat="server" Text="確定修改" CommandName="Update" CssClass="btn btn-primary btn-xs" />
                                                            <asp:Button ID="btnBackTo" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-CssClass="col-md-2" Visible="false" InsertVisible="false">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:Label ID="EquipID" runat="server" Text='<%# Bind("EquipID") %>' ReadOnly="true" CssClass="col-md-3 nopadding"></asp:Label>
                                                        </div>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="配備分類" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:Label ID="EquipCategoryID" runat="server" Text='<%# Eval("EquipCategoryID").ToString().Trim() %>' Visible="false"></asp:Label>
                                                            <asp:DropDownList ID="qEquipCategory" runat="server" 
                                                                DataTextField="EquipCategoryName" DataValueField="EquipCategoryID" OnDataBound="EquipCategory_DataBound" DataSourceID="ObjectDataSource2" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:DropDownList ID="qEquipCategory" runat="server"
                                                                DataTextField="EquipCategoryName" DataValueField="EquipCategoryID" DataSourceID="ObjectDataSource2" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>
                                               
                                                <asp:TemplateField HeaderText="配備名稱" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="EquipName" runat="server" Text='<%# Bind("EquipName") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-4 nopadding">
                                                            <asp:TextBox ID="EquipName" runat="server" Text='<%# Bind("EquipName") %>' MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="配備額外說明" HeaderStyle-CssClass="col-md-2">
                                                    <EditItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="EquipProtectText" runat="server" Text='<%# Bind("EquipProtectText") %>' TextMode="MultiLine" Width="100%" Height="150px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </EditItemTemplate>
                                                    <InsertItemTemplate>
                                                        <div class="col-md-10 nopadding">
                                                            <asp:TextBox ID="EquipProtectText" runat="server" Text='<%# Bind("EquipProtectText") %>' TextMode="MultiLine" Width="100%" Height="80px" MaxLength="200"></asp:TextBox>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:TemplateField>
                                            </Fields>
                                        </asp:DetailsView>
                                    </div>                                  
                                </div>
                            </div>
                            <!-- panel-body -->
                        </form>
                    </div>
                    <!-- panel-default -->

                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBEquip" SelectMethod="GetListBySerno" InsertMethod="Insert" UpdateMethod="Update" OnInserted="ObjectDataSource1_Inserted">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="EquipID" QueryStringField="EquipID" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="Backend_DBEquip"
                        SelectMethod="GetEquipTypeList">
                        <SelectParameters>
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </div>
            <!-- row -->
        </div>
        <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>

    <uc1:uc_foot runat="server" ID="uc_foot" />
</body>
</html>


