﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="backend_Equip_list" %>


<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <style>
        /*在bootstrap.min.css 的table margin-bottom設為20px*/
        .table {
            margin-bottom: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            (function () {
                //複製Gridview_TopInfo Start
                var $infoBar = $("#gridview_topInfo");
                if (!!$infoBar.length) {
                    $('#gridview_bottomInfo').append($infoBar.clone(true).html());
                }
                //複製Gridview_TopInfo End
            })();
        });

        //複製的情況會造成…Form Data會傳送同name但不同value，會以第一個為主，所以要透過onChange事件修正第一個value
        function showcountChange(obj) {
            $("select[ShowRoomName='drpShowCount']").each(function (i) {
                $(this).val($(obj).val());
            });
        }

        //只能輸入數字
        function ValidateNumber(e, pnumber) {
            if (!/^[0-9.]+$/.test(pnumber)) {
                e.value = /^[0-9.]+$/.exec(e.value);
            }
            return false;
        }

    </script>
    <script language="javascript">
        //參數 a,b ,IsSentToServer:是否傳到後端 
        function ChangeStockStatus(e, IsSentToServer) {


            var msg = "是否要把此筆資料改成" + e.value;


            //是否執行後端執令 
            if (!IsSentToServer) {
                if (confirm(msg)) {
                    event.returnValue = true;
                } else {
                    //下面這行把 Client 事件擋下來， form send 不出去，所以也就不會到後端了           
                    event.returnValue = false;
                }

            }

        }
    </script>
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <form runat="server" id="listForm" class="form-inline">
                    <div class="well well-sm search-panel">
                        <i class="glyphicon glyphicon-search"></i>搜尋<br /> 
                        <div class="form-group">
                            <label class="">配備種類:</label>
                            <asp:DropDownList ID="qEquipCategory" runat="server"
                                DataTextField="EquipCategoryName" DataValueField="EquipCategoryID" OnDataBound="EquipCategory_DataBound" DataSourceID="ObjectDataSource2" CssClass="form-control input-sm">
                            </asp:DropDownList>
                        </div>               
                       
                        <asp:Button ID="btnQuery" runat="server" CssClass="btn btn-primary btn-sm" Text="查詢" />
                       <asp:Button ID="btnOnline" runat="server" Text="上傳正式區"  OnClientClick='return confirm("您確定要將修改儲存後的資料上傳正式區嗎?")'   OnClick="submitToOnline" CssClass="btn btn-success btn-xs"/>
                        <asp:Button ID="btnInsert" runat="server" CssClass="btn btn-success btn-sm pull-right" Text="建立新資料" OnClick="btnInsert_Click" />
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div id="gridview_topInfo">
                                每頁顯示筆數
                                <asp:DropDownList runat="server" ID="drpShowCount" AutoPostBack="True" OnSelectedIndexChanged="drpShowCount_SelectedIndexChanged" onChange="showcountChange(this);">
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                </asp:DropDownList>
                                搜尋資料總筆數
                                <asp:Label runat="server" ID="lblTotalCount"></asp:Label>
                            </div>
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-primary" DataKeyNames="EquipID" GridLines="None"
                                    AllowPaging="true" PageSize="10" AllowSorting="true" OnRowCreated="GridView1_RowCreated" OnPageIndexChanging="GridView1_PageIndexChanging" OnPreRender="GridView1_PreRender"
                                    AutoGenerateColumns="false" DataSourceID="ObjectDataSource1" OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound" PagerStyle-CssClass="bs-pagination text-right">
                                    <EmptyDataTemplate>查無資料</EmptyDataTemplate>
                                    <PagerTemplate>
                                        <asp:PlaceHolder ID="placeholderPager" runat="server"></asp:PlaceHolder>
                                    </PagerTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="編號" ControlStyle-CssClass="col-md-1">
                                            <ItemTemplate>
                                                <asp:Label ID="EquipID" Text='<%# Eval("EquipID").ToString().Trim() %>' runat="server"></asp:Label>                                               
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    
                                        <asp:BoundField DataField="EquipCategoryName" HeaderText="配備類別" ControlStyle-CssClass="col-md-3">
                                            <ControlStyle></ControlStyle>
                                        </asp:BoundField>
                                         <asp:BoundField DataField="EquipName" HeaderText="配備名稱" ControlStyle-CssClass="col-md-3">
                                            <ControlStyle></ControlStyle>
                                        </asp:BoundField>  
                                         <asp:BoundField DataField="EquipProtectText" HeaderText="配備額外說明" ControlStyle-CssClass="col-md-3">
                                            <ControlStyle></ControlStyle>
                                        </asp:BoundField>                                           
                                                                     
                                        <asp:TemplateField HeaderText="功能" ShowHeader="false" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="col-md-2">
                                            <ItemTemplate>                                                
                                                <a class="btn btn-warning btn-xs" href='edit.aspx?EquipID=<%# Eval("EquipID") %>' target="_self">編輯</a>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-danger btn-xs" Text="刪除" OnClientClick='return confirm("您確定要刪除此筆資料嗎?")' CommandName="Update" CausesValidation="true"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="gridview_bottomInfo">
                                <%--用來放尾巴資訊--%>
                            </div>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBEquip" EnablePaging="true"
                                MaximumRowsParameterName="maxrows"
                                StartRowIndexParameterName="startrows"
                                SelectMethod="GetList"
                                SelectCountMethod="GetCounts"
                                OnSelected="ObjectDataSource1_Selected"
                                UpdateMethod="UpdateValid">
                                <SelectParameters> 
                                    <asp:ControlParameter ControlID="qEquipCategory" Name="EquipCategoryID" PropertyName="SelectedValue" Type="String" />                               
                                                   
                                </SelectParameters>
                            </asp:ObjectDataSource>                           
                            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="Backend_DBEquip"
                                SelectMethod="GetEquipTypeList">
                                <SelectParameters>                                   
                                </SelectParameters>
                            </asp:ObjectDataSource>

                        </div>
                    </div>
                </form>
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>
    <uc1:uc_foot runat="server" ID="uc_foot" />
    <script type="text/javascript" src='<%= ResolveUrl("~/backend/asset/js/bs.pagination.js") %>'></script>
</body>
</html>
