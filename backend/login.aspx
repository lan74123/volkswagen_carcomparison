﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="backend_login" %>

<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
</head>

<body class="signin">
    <section>
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-1"></div>
            <div class="col-md-6 col-sm-6 col-xs-10">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-1"></div>
                    <div class="col-md-6 col-sm-6 col-sm-10 signinpanel">
                        <form runat="server" id="loginForm">
                            <h4 class="nomargin"><%= Config.Site %> 後端管理系統</h4>
                            <asp:TextBox runat="server" ID="txtAccount" CssClass="form-control uname" placeholder="帳號"></asp:TextBox>
                            <asp:TextBox runat="server" ID="txtPwd" CssClass="form-control pword" placeholder="密碼" TextMode="Password"></asp:TextBox>
                            <asp:Button runat="server" ID="btnLogin" CssClass="btn btn-block btn-primary" Text="登入" OnClick="btnLogin_Click" />
                        </form>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-1"></div>
                    <!-- col-sm-5 -->
                </div>
                <!-- row -->

                <div class="signup-footer">
                    <div class="pull-left">
                        &copy; 2016. All Rights Reserved. Ogilvy One
                    </div>
                    <div class="pull-right">
                        Modified By: OgilvyOne
                    </div>
                </div>

            </div>
            <div class="col-md-3 col-sm-3 col-xs-1"></div>
        </div>
        <!-- signin -->

    </section>
    <uc1:uc_foot runat="server" ID="uc_foot" />
</body>
</html>
