﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        HttpContext.Current.Session.Abandon();

        // clear authentication cookie
        HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
        cookie1.Expires = DateTime.Now.AddYears(-1);
        HttpContext.Current.Response.Cookies.Add(cookie1);

        // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
        HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
        cookie2.Expires = DateTime.Now.AddYears(-1);
        HttpContext.Current.Response.Cookies.Add(cookie2);

        //清除麵包屑的cookie
        HttpCookie cookie3 = new HttpCookie("websiteBread", "");
        cookie3.Expires = DateTime.Now.AddYears(-1);
        HttpContext.Current.Response.Cookies.Add(cookie3);

		HttpCookie cookie4 = new HttpCookie("manage_showrooms", "");
		cookie4.Expires = DateTime.Now.AddYears(-1);
		HttpContext.Current.Response.Cookies.Add(cookie4);



		Response.Redirect("login.aspx");
    }
}