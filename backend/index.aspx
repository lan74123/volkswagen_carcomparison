﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="backend_index" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
        
    </div>
    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->


        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />

            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <!-- 內容區塊-->
                    </div>
                </div>

                <!-- row -->
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>


    <uc1:uc_foot runat="server" ID="uc_foot" />
</body>
</html>

