﻿<%@ WebHandler Language="C#" Class="carFinance" %>

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Web.Script.Serialization;


public class carFinance : IHttpHandler {
    private string conn = Config.ConnString;
    private string last_modify = DateTime.Now.GetDateTimeFormats('r')[0].ToString();
    private string expires = DateTime.Now.AddHours(1).GetDateTimeFormats('r')[0].ToString();
    public DateTime today = DateTime.UtcNow.AddHours(8);

    protected class CarFinanceObj
    {
        public int CarID { get; set; }
        public int MSRP { get; set; }
        public int Downpayment{ get; set; }
        public decimal InterstRate { get; set; }
        public string PromoteText { get; set; }
        //public string PromoteStart { get; set; }
        //public string PromoteEnd { get; set; }
        //public decimal? PromoteInterstRate { get; set; }
        public List<int> Tenor { get; set; }
    }
    //input
    public string carID;
    //output
    public string carfinancejson;
    private string _result = "";
    private string _errmsg = "";


    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/json ";
        context.Response.CacheControl = "no-cache";
        context.Response.AddHeader("Pragma", "no-cache");
        context.Response.AddHeader("cache-control", "no-cache");
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetNoServerCaching();
        context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
        context.Response.AppendHeader("Expires", "0");
        context.Response.AppendHeader("Access-Control-Allow-Origin", " http://www.volkswagentaiwan.com.tw");
        // context.Response.AppendHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Authorization");
        _result = "N";
        _errmsg = "";

        string method = context.Request.HttpMethod;
        string urlCome = "";

        if (context.Request.Headers.Get("Referer") != null)
        {
            urlCome = context.Request.Headers.Get("Referer").ToString();
        }

        string[] allowUrlArray = { "www.volkswagentaiwan.com.tw" };

        //should set false on offical
        bool allowCheck = true;

        foreach (string allow in allowUrlArray)
        {
            if (urlCome.Contains(allow))
            {
                allowCheck = true;
            }
        }

        if (Config.IsDebug.ToString().Equals("Y"))
        {
            allowCheck = true;
        }

        if (!allowCheck)
        {
            _errmsg = "拒絕存取";
            _result = "N ";
        }
        else if (context.Request.RequestType == "POST")
        {
            carID = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["carID"]) ? "" : context.Request["carID"]);
            if (_errmsg == "")
            {
                if(carID == "")
                {
                    _errmsg += "請輸入車子ID\\n";
                }
                else
                {
                    if (!ValidateHelper.IsNumber(carID))
                    {
                        _errmsg += "carID輸入錯誤\\n";
                    }
                }
            }
            SqlConnection db = new SqlConnection(this.conn);
            SqlConnection db2 = new SqlConnection(this.conn);

            string commandText = @"  ";
            commandText += @"SELECT * FROM [CarFinance] LEFT JOIN [Car] on [Car].CarID=[CarFinance].CarID WHERE [CarFinance].carID=@carID AND FinanceStatus=1 AND StockStatus = 'S' ";
            SqlCommand cmd = new SqlCommand(commandText, db);
            cmd.Parameters.AddWithValue("@carID", carID);

            string TenorcommandText = @"  ";
            TenorcommandText += @"SELECT Tenor FROM [Tenor] ORDER BY Tenor ASC";
            SqlCommand Tennorcmd = new SqlCommand(TenorcommandText, db2);


            if (_errmsg == "")
            {
                try
                {
                    db.Open();
                    db2.Open();
                    CarFinanceObj carfinanceobj = new CarFinanceObj();

                    SqlDataReader sqlReader = cmd.ExecuteReader();
                    _result = "Y";

                    if (sqlReader.Read())
                    {
                        carfinanceobj.CarID = sqlReader.GetInt32(sqlReader.GetOrdinal("CarID"));
                        carfinanceobj.MSRP = sqlReader.GetInt32(sqlReader.GetOrdinal("MSRP"));
                        carfinanceobj.Downpayment = sqlReader.GetInt32(sqlReader.GetOrdinal("Downpayment"));
                        carfinanceobj.InterstRate = sqlReader.GetDecimal(sqlReader.GetOrdinal("InterestRate"));
                        carfinanceobj.PromoteText = sqlReader.GetString(sqlReader.GetOrdinal("PromoteText"));
                        DateTime? promotestart= (sqlReader.IsDBNull(sqlReader.GetOrdinal("PromoteStart"))) ? (DateTime?)null:(sqlReader.GetDateTime(sqlReader.GetOrdinal("PromoteStart")));
                        DateTime? promoteend= (sqlReader.IsDBNull(sqlReader.GetOrdinal("PromoteEnd"))) ? (DateTime?)null:(sqlReader.GetDateTime(sqlReader.GetOrdinal("PromoteEnd"))).AddDays(1);
                        if(today >=promotestart && today <= promoteend)
                        {
                            carfinanceobj.InterstRate = sqlReader.GetDecimal(sqlReader.GetOrdinal("PromoteInterestRate"));
                        }
                        //carfinanceobj.PromoteStart = (sqlReader.IsDBNull(sqlReader.GetOrdinal("PromoteStart"))) ? null:(sqlReader.GetDateTime(sqlReader.GetOrdinal("PromoteStart"))).ToString("yyyyMMdd") ;
                        //carfinanceobj.PromoteEnd = (sqlReader.IsDBNull(sqlReader.GetOrdinal("PromoteEnd"))) ? null:(sqlReader.GetDateTime(sqlReader.GetOrdinal("PromoteEnd"))).ToString("yyyyMMdd") ;

                        //carfinanceobj.PromoteInterstRate = sqlReader.GetDecimal(sqlReader.GetOrdinal("PromoteInterestRate"));
                        //if(sqlReader.GetDecimal(sqlReader.GetOrdinal("PromoteInterestRate")).Equals(Decimal.Zero))
                        //{
                        //    carfinanceobj.PromoteInterstRate = null;
                        //}

                        SqlDataReader TenorReader = Tennorcmd.ExecuteReader();
                        carfinanceobj.Tenor = new List<int>();
                        if (TenorReader.HasRows)
                        {
                            while (TenorReader.Read())
                            {
                                //Console.WriteLine(TenorReader.GetInt16(TenorReader.GetOrdinal("Tenor")));
                                carfinanceobj.Tenor.Add(TenorReader.GetInt16(TenorReader.GetOrdinal("Tenor")));
                            }
                        }
                        carfinancejson = new JavaScriptSerializer().Serialize(carfinanceobj);
                    }
                    else
                    {
                        _errmsg += "查無資料";
                    }
                }
                catch (Exception o)
                {
                    _result = "N";
                    _errmsg += "資料存取失敗， " + o.Message.ToString();
                }
                finally
                {
                    db.Close();
                    db2.Close();
                }
            }

            //回傳
            context.Response.AddHeader("Expires", expires);
            context.Response.AddHeader("Last-Modified", last_modify);
            context.Response.AddHeader("Cache-Control", "no-cache, must-revalidate");
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("x-frame-options", "DENY");

            if (string.IsNullOrEmpty(_errmsg))
            {
                context.Response.Write("{\"Result\": \"" + _result + "\", \"data\": " + carfinancejson + ", \"ErrorMsg\": \"\"}");
            }
            else
            {
                context.Response.Write("{\"Result\": \"" + _result + "\", \"ErrorMsg\": \"" + _errmsg + "\"}");
            }
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}