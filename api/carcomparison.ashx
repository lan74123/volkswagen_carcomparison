﻿<%@ WebHandler Language="C#" Class="carcomparison" %>

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

public class carcomparison : IHttpHandler
{
    [Serializable()]
    //car
    protected class CarObj
    {
        public string CarID { get; set; }
        public string CarTypeName { get; set; }
        public string CarName { get; set; }
        public string CarImg { get; set; }
        public string CarShowSize { get; set; }
        public string CarShowWheelbase { get; set; }
        public string CarShowTrack { get; set; }
        public string CarShowLuggage { get; set; }
        public string CarShowWeight { get; set; }
        public string CarShowType { get; set; }
        public string CarShowCC { get; set; }
        public string CarShowHp { get; set; }
        public string CarShowKgm { get; set; }
        public string CarShowCompression { get; set; }
        public string CarShowStroke { get; set; }
        public string CarShowAcceleration { get; set; }
        public string CarShowExtremeSpeed { get; set; }
        public string CarShowOil { get; set; }
        public string CarShowFuelTank { get; set; }
        public string CarShowTransfer { get; set; }
        public string CarShowTransmission { get; set; }
        public string CarShowSteering { get; set; }
        public string CarShowBrake { get; set; }
        public string CarShowSuspension { get; set; }
        public string CarShowTire { get; set; }
        public string CarShowMinSlewing { get; set; }
        public string CarShowAnnualFuel { get; set; }
        public string CarShowNonUrbanFuel { get; set; }
        public string CarShowUrbanFuel { get; set; }
        public string CarShowTestValue { get; set; }
        public string CarShowEfficiencyRating { get; set; }
    }

    protected class CarShowObj
    {
        public string CarShowSize { get; set; }
        public string CarShowWheelbase { get; set; }
        public string CarShowTrack { get; set; }
        public string CarShowLuggage { get; set; }
        public string CarShowWeight { get; set; }
        public string CarShowType { get; set; }
        public string CarShowCC { get; set; }
        public string CarShowHp { get; set; }
        public string CarShowKgm { get; set; }
        public string CarShowCompression { get; set; }
        public string CarShowStroke { get; set; }
        public string CarShowAcceleration { get; set; }
        public string CarShowExtremeSpeed { get; set; }
        public string CarShowOil { get; set; }
        public string CarShowFuelTank { get; set; }
        public string CarShowTransfer { get; set; }
        public string CarShowTransmission { get; set; }
        public string CarShowSteering { get; set; }
        public string CarShowBrake { get; set; }
        public string CarShowSuspension { get; set; }
        public string CarShowTire { get; set; }
        public string CarShowMinSlewing { get; set; }
        public string CarShowAnnualFuel { get; set; }
        public string CarShowNonUrbanFuel { get; set; }
        public string CarShowUrbanFuel { get; set; }
        public string CarShowTestValue { get; set; }
        public string CarShowEfficiencyRating { get; set; }
    }


    //compare
    protected class EquipCategoryNameObj
    {
        public string EquipCategoryName { get; set; }
        public List<EquipNameObj> EquipNameList { get; set; }
        public Dictionary<string, EquipNameObj> EquipNameDict { get; set; }
    }

    protected class EquipNameObj
    {
        public string EquipName { get; set; }
        public string EquipProtectText { get; set; }
        public List<CarEquipObj> CarEquipList { get; set; }
    }

    protected class CarProtectObj
    {
        public string CarName { get; set; }
        public string CarProtectText { get; set; }
    }

    protected class CarEquipObj
    {
        public string CarID { get; set; }
        public string EquipType { get; set; }
        public string EquipText { get; set; }

    }


    private string conn = Config.ConnString;
    private string last_modify = DateTime.Now.GetDateTimeFormats('r')[0].ToString();
    private string expires = DateTime.Now.AddHours(1).GetDateTimeFormats('r')[0].ToString();

    public string CarID, IP, UUID, Preview;
    public string Utm_Source, Utm_Medium;
    public string Succ, Msg, Car, Compare, EquipProtectText, CarProtectText;


    //Main
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/json ";
        context.Response.CacheControl = "no-cache";
        context.Response.AddHeader("Pragma", "no-cache");
        context.Response.AddHeader("cache-control", "no-cache");
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetNoServerCaching();
        context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
        context.Response.AppendHeader("Expires", "0");
        context.Response.AppendHeader("Access-Control-Allow-Origin", " http://www.volkswagentaiwan.com.tw");
        // context.Response.AppendHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Authorization");
        Succ = "N ";
        Msg = "";
        Car = "[]";
        Compare = "[]";
        EquipProtectText = "[]";
        CarProtectText = "[]";

        string method = context.Request.HttpMethod;
        string urlCome = "";
        if (context.Request.Headers.Get("Referer") != null)
        {
            urlCome = context.Request.Headers.Get("Referer").ToString();
        }
        string[] allowUrlArray = { "www.volkswagentaiwan.com.tw" };

        bool allowCheck = true;
        foreach (string allow in allowUrlArray)
        {
            if (urlCome.Contains(allow))
            {
                allowCheck = true;
            }
        }

        if (Config.IsDebug.ToString().Equals("Y"))
        {
            allowCheck = true;
        }

        if (!allowCheck)
        {
            Msg = "拒絕存取";
            Succ = "N ";
        }
        else if (context.Request.RequestType == "GET")
        {

            CarID = string.IsNullOrEmpty(context.Request["CarID"]) ? "" : context.Request["CarID"];

            Preview = string.IsNullOrEmpty(context.Request["Preview"]) ? "" : context.Request["Preview"];


            Utm_Source = "";
            if (context.Request.Cookies["UtmSource"] != null)
            {
                Utm_Source = context.Request.Cookies["UtmSource"].Value.ToString();
            }
            Utm_Medium = "";
            if (context.Request.Cookies["UtmMedium"] != null)
            {
                Utm_Medium = context.Request.Cookies["UtmMedium"].Value.ToString();
            }

            //parse value


            CarID = CarID.TrimEnd(',');
            string[] CarIDArray = CarID.Split(',');

            try
            {
                IP = IPNetworking.GetClientIP();
            }
            catch
            {
                IP = " ";
                // Msg += "錯誤不明 ";
            }

            try
            {
                UUID = string.IsNullOrEmpty(context.Request.Cookies["uuid"].Value) ? "" : context.Request.Cookies["uuid"].Value;
            }
            catch
            {
                UUID = " ";
                //Msg += "錯誤不明 ";
            }


            //check           

            if (CarIDArray.Length < 1 || CarIDArray.Length > 3) { Msg += "車輛比較數目錯誤 "; }
            for (int i = 0; i < CarIDArray.Length; i++)
            {
                if (!ValidateHelper.IsNumber(CarIDArray[i])) { Msg += "車輛錯誤 "; }
            }


            if (!CkeckCarIsExist(CarIDArray)) { Msg += "車輛錯誤 "; }


            if (string.IsNullOrEmpty(Msg))
            {
                if (Preview.ToLower().Equals("preview"))
                {
                    Msg = Preview;
                    getData(CarIDArray, "");
                }
                else
                {
                    Msg = Preview;
                    getData(CarIDArray, "_online");
                }
            }
            else
            {
                Succ = "N ";
            }
        }
        else
        {
            Succ = "N ";
        }


        if (Car.Equals("[]"))
        {
        }
        else
        {
            //  Car = Compare.Replace("CarSize", "長/ 寬/ 高(mm)").Replace("Wheelbase", "軸距(mm)").Replace("Track", "輪距前/ 後(mm)").Replace("Luggage", "行李廂空間(L)").Replace("Weight", "重量(kg)<1").Replace("CType", "型式").Replace("CC", "排氣量 (c.c.)").Replace("Hp", "最大馬力 (hp/rpm)").Replace("Kgm", "最大扭力(kgm/rpm)").Replace("Compression", "壓縮比").Replace("Stroke", "缸徑/ 衝程 (mm/ mm)").Replace("Acceleration", "0-100 km/h 加速(sec)").Replace("ExtremeSpeed", "極速(km/h)").Replace("Oil", "使用油料").Replace("FuelTank", "油箱容量( 公升)").Replace("Transfer", "傳動方式").Replace("Transmission", "變速箱").Replace("Steering", "轉向系統").Replace("Brake", "煞車系統").Replace("Suspension", "懸吊系統").Replace("Tire", "輪胎尺寸").Replace("MinSlewing", "最小迴轉半徑(M)").Replace("AnnualFuel", "年耗油量( 公升)<2").Replace("NonUrbanFuel", "非市區耗油量( 公里/ 公升)").Replace("UrbanFuel", "市區耗油量( 公里/ 公升)").Replace("TestValue", "測試值( 公里/ 公升)<3").Replace("EfficiencyRating", "能源效率等級");
        }

        if (Compare.Equals("[]"))
        {
        }
        else
        {
            Compare = Compare.Replace(",\"EquipNameDict\":null", "");
            Compare = Compare.Replace("CarShowSize", "長/ 寬/ 高(mm)").Replace("CarShowWheelbase", "軸距(mm)").Replace("CarShowTrack", "輪距前/ 後(mm)").Replace("CarShowLuggage", "行李廂空間(L)")
                    .Replace("CarShowWeight", "重量(kg)<1").Replace("CarShowType", "型式").Replace("CarShowCC", "排氣量 (c.c.)").Replace("CarShowHp", "最大馬力 (hp/rpm)")
                    .Replace("CarShowKgm", "最大扭力(kgm/rpm)").Replace("CarShowCompression", "壓縮比").Replace("CarShowStroke", "缸徑/ 衝程 (mm/ mm)")
                    .Replace("CarShowAcceleration", "0-100 km/h 加速(sec)").Replace("CarShowExtremeSpeed", "極速(km/h)").Replace("CarShowOil", "使用油料")
                    .Replace("CarShowFuelTank", "油箱容量( 公升)").Replace("CarShowTransfer", "傳動方式").Replace("CarShowTransmission", "變速箱")
                    .Replace("CarShowSteering", "轉向系統").Replace("CarShowBrake", "煞車系統").Replace("CarShowSuspension", "懸吊系統")
                    .Replace("CarShowTire", "輪胎尺寸").Replace("CarShowMinSlewing", "最小迴轉半徑(M)").Replace("CarShowAnnualFuel", "年耗油量( 公升)<2")
                    .Replace("CarShowNonUrbanFuel", "非市區耗油量( 公里/ 公升)").Replace("CarShowUrbanFuel", "市區耗油量( 公里/ 公升)")
                    .Replace("CarShowTestValue", "測試值( 公里/ 公升)<3").Replace("CarShowEfficiencyRating", "能源效率等級");
        }

        if (EquipProtectText.Equals("[]"))
        {
        }
        else
        {
            EquipProtectText = EquipProtectText.Replace(",\"CarEquipList\":null", "");
        }

        if (CarProtectText.Equals("[]"))
        {

        }
        else
        {
            //  CarProtectText = CarProtectText.Replace(",\"CarEquipList\":null", "");
        }



        //回傳
        context.Response.AddHeader("Expires", expires);
        context.Response.AddHeader("Last-Modified", last_modify);
        context.Response.AddHeader("Cache-Control", "no-cache, must-revalidate");
        context.Response.AddHeader("Pragma", "no-cache");
        context.Response.AddHeader("x-frame-options", "DENY");
        context.Response.Write("{\"succ\":\"" + Succ + "\", \"Msg\": \"" + Msg + "\", \"Car\": " + Car + ", \"Compare\": " + Compare + ",\"EquipProtextText\":" + EquipProtectText + ",\"CarProtectText\":" + CarProtectText + "}");
    }

    public void saveCompareLog(string carid1, string carid2, string carid3)
    {

        SqlConnection db = new SqlConnection(this.conn);
        string commandText = "INSERT INTO [Compare]([GUID],[UUID],[CarID1],[CarID2],[CarID3],[CDate],[MDate],[Utm_Source],[Utm_Medium])"
        + "VALUES ( @GUID,@UUID,@CarID1,@CarID2,@CarID3,@CDate,@MDate,@Utm_Source,@Utm_Medium) ";


        SqlCommand cmd = new SqlCommand(commandText, db);

        cmd.Parameters.AddWithValue("@GUID", Guid.NewGuid());
        cmd.Parameters.AddWithValue("@UUID", UUID);
        cmd.Parameters.AddWithValue("@CarID1", carid1);
        cmd.Parameters.AddWithValue("@CarID2", carid2);
        cmd.Parameters.AddWithValue("@CarID3", carid3);


        cmd.Parameters.AddWithValue("@CDate", DateTime.Now);
        cmd.Parameters.AddWithValue("@MDate", DateTime.Now);


        cmd.Parameters.AddWithValue("@Utm_Source", Utm_Source);
        cmd.Parameters.AddWithValue("@Utm_Medium", Utm_Medium);

        try
        {
            db.Open();
            cmd.ExecuteNonQuery();
            Succ = "Y ";


        }
        catch (Exception o)
        {
            Succ = "N ";
            Msg = "不好意思，資料記錄失敗，請稍後再試 ";
        }
        finally
        {
            db.Close();
        }
    }


    public void getData(string[] CarIDArray, string tbName)
    {
        //case ce.EquipType when 'E' then '標配' else '選配' END as EquipType
        SqlConnection db = new SqlConnection(this.conn);
        string commandText = @"  ";

        for (int i = 0; i < CarIDArray.Length; i++)
        {
            if (i == 0)
            {
                commandText += @" SELECT c.CarName,c.CarImg,c.CarSize,c.Wheelbase,c.Track,c.Luggage,c.Weight,c.Type,c.CC,c.Hp,c.Kgm,c.Compression,c.Stroke,c.Acceleration,c.ExtremeSpeed,c.Oil,c.FuelTank,c.Transfer,c.Transmission,c.Steering,c.Brake,c.Suspension,c.Tire,c.MinSlewing,c.AnnualFuel,c.NonUrbanFuel,c.UrbanFuel,c.TestValue,c.EfficiencyRating, c.CarProtectText,ce.Serno as 'ceSerno','" + i + @"' as 'compareorder',
                                        c.carid,ct.carTypeName, c.carname ,  ce.EquipType ,ec.EquipCategoryName, e.EquipCategoryID,    
                                        CASE WHEN e.EquipProtectText = '' THEN  e.EquipName  ELSE  e.EquipName+'*' END EquipName, e.EquipProtectText ,
                                        e.EquipProtectText ,e.corder, ce.EquipExtraDetail
                                        FROM [CarEquip" + tbName + @"]  ce
                                        left join [Car" + tbName + @"] c
                                        on c.carid=ce.carid
                                        left join [CarType] ct
                                        on c.CarTypeid=ct.CarTypeid
                                        left join [Equip" + tbName + @"] e
                                        on e.EquipID = ce.EquipID
                                        left join [EquipCategory] ec
                                        on e.EquipCategoryID = ec.EquipCategoryID
                                        where c.valid='1' and c.StockStatus='S' and ce.carid= '" + CarIDArray[i] + "' and e.valid='1'";
            }
            else
            {

                commandText += @"  UNION
                                         SELECT c.CarName,c.CarImg,c.CarSize,c.Wheelbase,c.Track,c.Luggage,c.Weight,c.Type,c.CC,c.Hp,c.Kgm,c.Compression,c.Stroke,c.Acceleration,c.ExtremeSpeed,c.Oil,c.FuelTank,c.Transfer,c.Transmission,c.Steering,c.Brake,c.Suspension,c.Tire,c.MinSlewing,c.AnnualFuel,c.NonUrbanFuel,c.UrbanFuel,c.TestValue,c.EfficiencyRating,c.CarProtectText, ce.Serno as 'ceSerno','" + i + @"' as 'compareorder',
                                        c.carid,ct.carTypeName, c.carname ,  ce.EquipType ,ec.EquipCategoryName, e.EquipCategoryID,    
                                        CASE WHEN e.EquipProtectText = '' THEN  e.EquipName  ELSE  e.EquipName+'*' END EquipName, e.EquipProtectText ,
                                        e.EquipProtectText ,e.corder, ce.EquipExtraDetail
                                        FROM [CarEquip" + tbName + @"]  ce
                                        left join [Car" + tbName + @"] c
                                        on c.carid=ce.carid
                                        left join [CarType] ct
                                        on c.CarTypeid=ct.CarTypeid
                                        left join [Equip" + tbName + @"] e
                                        on e.EquipID = ce.EquipID
                                        left join [EquipCategory] ec
                                        on e.EquipCategoryID = ec.EquipCategoryID
                                        where c.valid='1' and c.StockStatus='S' and ce.carid= '" + CarIDArray[i] + "' and e.valid='1'";

            }

        }

        commandText += " order by compareorder,e.EquipCategoryID,e.corder ";

        SqlCommand cmd = new SqlCommand(commandText, db);

        //cmd.Parameters.AddWithValue("@IP", IP);
        //cmd.Parameters.AddWithValue("@Valid", "1");
        //cmd.Parameters.AddWithValue("@UUID", UUID);

        //cmd.Parameters.AddWithValue("@Utm_Source", Utm_Source);
        //cmd.Parameters.AddWithValue("@Utm_Medium", Utm_Medium);             

        try
        {
            db.Open();
            SqlDataReader sqlReader = cmd.ExecuteReader();
            Succ = "Y ";

            List<CarObj> CarList = new List<CarObj>();

            // List<EquipNameObj> EquipNameList = new List<EquipNameObj>();
            //List<CarEquipObj> CarEquipList = new List<CarEquipObj>();

            Dictionary<string, string> CarDictTemp = new Dictionary<string, string>();

            Dictionary<string, EquipCategoryNameObj> EquipCategoryDictTemp = new Dictionary<string, EquipCategoryNameObj>();
            Dictionary<string, EquipNameObj> EquipDictTemp = new Dictionary<string, EquipNameObj>();
            List<EquipNameObj> EquipProtectTextList = new List<EquipNameObj>();
            List<CarProtectObj> CarProtectTextList = new List<CarProtectObj>();


            while (sqlReader.Read())
            {
                string key = sqlReader["carid"].ToString();

                string carID = sqlReader["carid"].ToString();
                string carTypeName = sqlReader["carTypeName"].ToString();
                string carName = sqlReader["carname"].ToString();
                string carimg = sqlReader["CarImg"].ToString();
                string carsize = sqlReader["CarSize"].ToString();
                string wheelbase = sqlReader["Wheelbase"].ToString();
                string track = sqlReader["Track"].ToString();
                string luggage = sqlReader["Luggage"].ToString();
                string weight = sqlReader["Weight"].ToString();
                string type = sqlReader["Type"].ToString();
                string cc = sqlReader["CC"].ToString();
                string hp = sqlReader["Hp"].ToString();
                string kgm = sqlReader["Kgm"].ToString();
                string compression = sqlReader["Compression"].ToString();
                string stroke = sqlReader["Stroke"].ToString();
                string acceleration = sqlReader["Acceleration"].ToString();
                string extremespeed = sqlReader["ExtremeSpeed"].ToString();
                string oil = sqlReader["Oil"].ToString();
                string fueltank = sqlReader["FuelTank"].ToString();
                string transfer = sqlReader["Transfer"].ToString();
                string transmission = sqlReader["Transmission"].ToString();
                string steering = sqlReader["Steering"].ToString();
                string brake = sqlReader["Brake"].ToString();
                string suspension = sqlReader["Suspension"].ToString();
                string tire = sqlReader["Tire"].ToString();
                string minslewing = sqlReader["MinSlewing"].ToString();
                string annualfuel = sqlReader["AnnualFuel"].ToString();
                string nonurbanfuel = sqlReader["NonUrbanFuel"].ToString();
                string urbanfuel = sqlReader["UrbanFuel"].ToString();
                string testvalue = sqlReader["TestValue"].ToString();
                string efficiencyrating = sqlReader["EfficiencyRating"].ToString();
                string carprotecttext = sqlReader["CarProtectText"].ToString();

                string equipType = sqlReader["EquipType"].ToString();
                string equipCategoryName = sqlReader["EquipCategoryName"].ToString();
                string equipName = sqlReader["EquipName"].ToString();
                string equipProtectText = sqlReader["equipProtectText"].ToString();
                string equipExtraDetail = sqlReader["EquipExtraDetail"].ToString();

                /*
                 *配備保護文字
                */
                if (!string.IsNullOrEmpty(equipProtectText))
                {
                    bool isNotExit = true;
                    foreach (EquipNameObj eno in EquipProtectTextList)
                    {
                        if (equipName.Equals(eno.EquipName))
                        {
                            isNotExit = false;
                        }
                    }

                    if (isNotExit)
                    {
                        EquipNameObj eno = new EquipNameObj()
                        {
                            EquipName = equipName,
                            EquipProtectText = equipProtectText.Replace("\r\n", "<br>")
                        };
                        EquipProtectTextList.Add(eno);
                    }

                }
                //去除重複的配備保護文字
                for (int i = 0; i < EquipProtectTextList.Count; i++)  //外循环是循环的次数
                {
                    for (int j = EquipProtectTextList.Count - 1; j > i; j--)  //内循环是 外循环一次比较的次数
                    {

                        if (EquipProtectTextList[i].EquipProtectText.Equals(EquipProtectTextList[j].EquipProtectText))
                        {
                            EquipProtectTextList.RemoveAt(j);
                        }
                    }
                }
                /*整理 
                 * 配備分類1 {
                             *  配備名稱1 { 
                             *          [車輛1,車輛2,車輛3]
                             *          },
                             *  配備名稱2 { 
                             *          [車輛1,車輛2,車輛3]
                             *          }
                 *          }*/
                //判斷車款名稱是否重覆
                if (!CarDictTemp.ContainsKey(key))
                {

                    CarObj carobj = new CarObj()
                    {
                        CarID = carID,
                        CarTypeName = carTypeName.Replace("\r\n", "<br>"),
                        CarName = carName,
                        CarImg = Config.FrontPath + "" + carimg.Replace("\r\n", "<br>"),
                        CarShowSize = carsize.Replace("\r\n", "<br>"),
                        CarShowWheelbase = wheelbase.Replace("\r\n", "<br>"),
                        CarShowTrack = track.Replace("\r\n", "<br>"),
                        CarShowLuggage = luggage.Replace("\r\n", "<br>"),
                        CarShowWeight = weight.Replace("\r\n", "<br>"),
                        CarShowType = type.Replace("\r\n", "<br>"),
                        CarShowCC = cc.Replace("\r\n", "<br>"),
                        CarShowHp = hp.Replace("\r\n", "<br>"),
                        CarShowKgm = kgm.Replace("\r\n", "<br>"),
                        CarShowCompression = compression.Replace("\r\n", "<br>"),
                        CarShowStroke = stroke.Replace("\r\n", "<br>"),
                        CarShowAcceleration = acceleration.Replace("\r\n", "<br>"),
                        CarShowExtremeSpeed = extremespeed.Replace("\r\n", "<br>"),
                        CarShowOil = oil.Replace("\r\n", "<br>"),
                        CarShowFuelTank = fueltank.Replace("\r\n", "<br>"),
                        CarShowTransfer = transfer.Replace("\r\n", "<br>"),
                        CarShowTransmission = transmission.Replace("\r\n", "<br>"),
                        CarShowSteering = steering.Replace("\r\n", "<br>"),
                        CarShowBrake = brake.Replace("\r\n", "<br>"),
                        CarShowSuspension = suspension.Replace("\r\n", "<br>"),
                        CarShowTire = tire.Replace("\r\n", "<br>"),
                        CarShowMinSlewing = minslewing.Replace("\r\n", "<br>"),
                        CarShowAnnualFuel = annualfuel.Replace("\r\n", "<br>"),
                        CarShowNonUrbanFuel = nonurbanfuel.Replace("\r\n", "<br>"),
                        CarShowUrbanFuel = urbanfuel.Replace("\r\n", "<br>"),
                        CarShowTestValue = testvalue.Replace("\r\n", "<br>"),
                        CarShowEfficiencyRating = efficiencyrating.Replace("\r\n", "<br>")


                    };
                    CarList.Add(carobj);
                    CarDictTemp.Add(key, key);


                    //新增車款的保護文字
                    CarProtectObj cpo = new CarProtectObj()
                    {
                        CarName = carobj.CarTypeName + " " + carobj.CarName,
                        CarProtectText = carprotecttext.Replace("\r\n", "<br>")
                    };
                    CarProtectTextList.Add(cpo);
                }

                //判斷車款名稱是否重覆
                if (!EquipCategoryDictTemp.ContainsKey(equipCategoryName))
                {
                    //配備類別
                    EquipCategoryNameObj ecobj = new EquipCategoryNameObj()
                    {
                        EquipCategoryName = equipCategoryName,
                    };

                    List<EquipNameObj> EquipNameList = new List<EquipNameObj>();
                    //配備名稱
                    EquipNameObj enobj = new EquipNameObj()
                    {
                        EquipName = equipName,
                        EquipProtectText = equipProtectText.Replace("\r\n", "<br>")
                    };

                    EquipNameList.Add(enobj);

                    Dictionary<string, EquipNameObj> EquipNameDict = new Dictionary<string, EquipNameObj>();

                    //車輛配備
                    List<CarEquipObj> CarEquipList = new List<CarEquipObj>();
                    CarEquipObj ceobj = new CarEquipObj()
                    {
                        CarID = carID,
                        EquipType = equipType,
                        EquipText = equipExtraDetail.Replace("\r\n", "<br>")
                    };
                    CarEquipList.Add(ceobj);
                    enobj.CarEquipList = CarEquipList;

                    EquipNameDict.Add(equipName, enobj);
                    // ecobj.EquipNameList = EquipNameList;
                    ecobj.EquipNameDict = EquipNameDict;

                    EquipCategoryDictTemp.Add(equipCategoryName, ecobj);
                }
                else
                {
                    //配備類別
                    EquipCategoryNameObj ecobj = EquipCategoryDictTemp[equipCategoryName];

                    Dictionary<string, EquipNameObj> EquipNameDict = ecobj.EquipNameDict;
                    //檢查配備有沒有被加入
                    if (!EquipNameDict.ContainsKey(equipName))
                    {
                        // EquipNameDict = new Dictionary<string, EquipNameObj>();
                        //配備名稱
                        EquipNameObj enobj = new EquipNameObj()
                        {
                            EquipName = equipName,
                            EquipProtectText = equipProtectText.Replace("\r\n", "<br>")
                        };
                        //車輛配備
                        List<CarEquipObj> CarEquipList = new List<CarEquipObj>();
                        CarEquipObj ceobj = new CarEquipObj()
                        {
                            CarID = carID,
                            EquipType = equipType,
                            EquipText = equipExtraDetail.Replace("\r\n", "<br>")
                        };
                        CarEquipList.Add(ceobj);
                        enobj.CarEquipList = CarEquipList;
                        EquipNameDict.Add(equipName, enobj);
                    }
                    else
                    {
                        //配備名稱
                        EquipNameObj enobj = EquipNameDict[equipName];
                        //車輛配備
                        List<CarEquipObj> CarEquipList = enobj.CarEquipList;
                        CarEquipObj ceobj = new CarEquipObj()
                        {
                            CarID = carID,
                            EquipType = equipType,
                            EquipText = equipExtraDetail.Replace("\r\n", "<br>")
                        };
                        CarEquipList.Add(ceobj);
                        enobj.CarEquipList = CarEquipList;

                        EquipNameDict[equipName] = enobj;
                    }

                    ecobj.EquipNameDict = EquipNameDict;

                    EquipCategoryDictTemp[equipCategoryName] = ecobj;
                }
            }

            #region 新增車資訊
            /*
            * 增加技術規格 CAR 內容
                */
            if (CarList.Count > 0)
            {
                //--取得CAR的欄位名稱
                CarObj c = new CarObj();
                CarShowObj ct = new CarShowObj();
                for (int i = 0; i < CarList.Count; i++)
                {
                    if (i == 0)
                    {
                        c = CarList[i];
                        ct.CarShowSize = c.CarShowSize;
                        ct.CarShowWheelbase = c.CarShowWheelbase;
                        ct.CarShowTrack = c.CarShowTrack;
                        ct.CarShowLuggage = c.CarShowLuggage;
                        ct.CarShowWeight = c.CarShowWeight;
                        ct.CarShowType = c.CarShowType;
                        ct.CarShowCC = c.CarShowCC;
                        ct.CarShowHp = c.CarShowHp;
                        ct.CarShowKgm = c.CarShowKgm;
                        ct.CarShowCompression = c.CarShowCompression;
                        ct.CarShowStroke = c.CarShowStroke;
                        ct.CarShowAcceleration = c.CarShowAcceleration;
                        ct.CarShowExtremeSpeed = c.CarShowExtremeSpeed;
                        ct.CarShowOil = c.CarShowOil;
                        ct.CarShowFuelTank = c.CarShowFuelTank;
                        ct.CarShowTransfer = c.CarShowTransfer;
                        ct.CarShowTransmission = c.CarShowTransmission;
                        ct.CarShowSteering = c.CarShowSteering;
                        ct.CarShowBrake = c.CarShowBrake;
                        ct.CarShowSuspension = c.CarShowSuspension;
                        ct.CarShowTire = c.CarShowTire;
                        ct.CarShowMinSlewing = c.CarShowMinSlewing;
                        ct.CarShowAnnualFuel = c.CarShowAnnualFuel;
                        ct.CarShowNonUrbanFuel = c.CarShowNonUrbanFuel;
                        ct.CarShowUrbanFuel = c.CarShowUrbanFuel;
                        ct.CarShowTestValue = c.CarShowTestValue;
                        ct.CarShowEfficiencyRating = c.CarShowEfficiencyRating;
                    }
                }
                //取得物件的KEY值
                string json = JsonConvert.SerializeObject(ct);
                Dictionary<string, string> CarInfoDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                //---
                //車輛資訊大類
                EquipCategoryNameObj ecobjCar = new EquipCategoryNameObj()
                {
                    EquipCategoryName = "技術規格",
                };

                //產生與配備格式相同的車輛資訊
                Dictionary<string, EquipNameObj> EquipNameDictCar = new Dictionary<string, EquipNameObj>();

                foreach (string carKey in CarInfoDict.Keys)
                {
                    foreach (CarObj car in CarList)
                    {
                        string key = carKey;
                        if (!EquipNameDictCar.ContainsKey(key))
                        {
                            EquipNameObj eno = new EquipNameObj();
                            eno.EquipName = key;
                            eno.EquipProtectText = "";
                            //把物件轉為和 CarInfoDict KEY相符 從 CarInfoDictTemp 的 KEY 動態抓值塞入
                            Dictionary<string, string> CarInfoDictTemp = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(car));
                            string CarE = (CarInfoDictTemp.ContainsKey(carKey)) ? CarInfoDictTemp[carKey] : "";
                            List<CarEquipObj> CarEquipList = new List<CarEquipObj>();
                            CarEquipObj ceo = new CarEquipObj()
                            {
                                CarID = car.CarID,
                                EquipType = "C",
                                EquipText = CarE.Replace("\r\n", "<br>")
                            };

                            CarEquipList.Add(ceo);
                            eno.CarEquipList = CarEquipList;
                            EquipNameDictCar.Add(key, eno);
                        }
                        else
                        {
                            EquipNameObj eno = EquipNameDictCar[key];

                            //把物件轉為和 CarInfoDict KEY相符 從 CarInfoDictTemp 的 KEY 動態抓值塞入
                            Dictionary<string, string> CarInfoDictTemp = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(car));
                            string CarE = (CarInfoDictTemp.ContainsKey(carKey)) ? CarInfoDictTemp[carKey] : "";

                            List<CarEquipObj> CarEquipList = eno.CarEquipList;
                            CarEquipObj ceo = new CarEquipObj()
                            {
                                CarID = car.CarID,
                                EquipType = "C",
                                EquipText = CarE.Replace("\r\n", "<br>")
                            };

                            CarEquipList.Add(ceo);
                            eno.CarEquipList = CarEquipList;
                            EquipNameDictCar[key] = eno;
                        }
                    }
                }

                Dictionary<string, EquipNameObj> EquipNameDictCarTemp = new Dictionary<string, EquipNameObj>();

                //check car both not have EquipName
                foreach (string key in EquipNameDictCar.Keys)
                {
                    EquipNameObj eno = EquipNameDictCar[key];
                    List<CarEquipObj> CarEquipList = eno.CarEquipList;
                    bool carHaveEquip = false;

                    foreach (CarEquipObj ce in CarEquipList)
                    {
                        if (!string.IsNullOrEmpty(ce.EquipText))
                        {
                            carHaveEquip = true;
                        }
                    }
                    //如果有車輛說明有值 就新增
                    if (carHaveEquip)
                    {
                        EquipNameDictCarTemp[key] = eno;
                    }

                }

                ecobjCar.EquipNameDict = EquipNameDictCarTemp;
                EquipCategoryDictTemp.Add("技術規格", ecobjCar);
            }


            #endregion

            /*整理 
                 * [EquipCategoryName : 配備分類1 ,
                 * EquipNameList :
                             *  EquipName : 配備名稱1 
                             *  CarEquipList : [車輛1,車輛2,車輛3]
                             *        
                             *  EquipName :配備名稱2 
                             *          CarEquipList : [車輛1,車輛2,車輛3]
                             *          
                 *          ]*/

            List<EquipCategoryNameObj> EquipCategoryNameList = new List<EquipCategoryNameObj>();

            foreach (string key in EquipCategoryDictTemp.Keys)
            {
                List<EquipNameObj> EquipNameList = new List<EquipNameObj>();
                EquipCategoryNameObj ecn = EquipCategoryDictTemp[key];
                foreach (string key2 in ecn.EquipNameDict.Keys)
                {
                    EquipNameObj en = ecn.EquipNameDict[key2];

                    List<CarEquipObj> CarEquipListMain = new List<CarEquipObj>();
                    for (int i = 0; i < CarIDArray.Length; i++)
                    {
                        CarEquipObj ceobj = new CarEquipObj()
                        {
                            CarID = CarIDArray[i],
                            EquipType = "",
                            EquipText = ""
                        };
                        CarEquipListMain.Add(ceobj);
                    }
                    //檢查CARID 整理沒有配備 新增空直 ["CarID":"37","EquipType":"","EquipText":""]
                    for (int i = 0; i < CarEquipListMain.Count; i++)
                    {
                        string carid = CarEquipListMain[i].CarID;
                        foreach (CarEquipObj ceobj in en.CarEquipList)
                        {
                            if (carid.Equals(ceobj.CarID))
                            {
                                CarEquipListMain[i] = ceobj;
                            }
                        }
                    }

                    en.CarEquipList = CarEquipListMain;
                    EquipNameList.Add(en);

                }
                ecn.EquipNameList = EquipNameList;
                ecn.EquipNameDict = null;
                EquipCategoryNameList.Add(ecn);
            }


            Car = JsonConvert.SerializeObject(CarList);
            Compare = JsonConvert.SerializeObject(EquipCategoryNameList);
            EquipProtectText = JsonConvert.SerializeObject(EquipProtectTextList);
            CarProtectText = JsonConvert.SerializeObject(CarProtectTextList);

            if (CarIDArray.Length > 1 && CarIDArray.Length < 4)
            {
                string carid1 = "";
                string carid2 = "";
                string carid3 = "";
                for (int i = 0; i < CarIDArray.Length; i++)
                {
                    if (i == 0) { carid1 = CarIDArray[i]; }
                    if (i == 1) { carid2 = CarIDArray[i]; }
                    if (i == 2) { carid3 = CarIDArray[i]; }
                }

                saveCompareLog(carid1, carid2, carid3);
            }

        }
        catch (Exception o)
        {
            Succ = "N ";
            Msg += "不好意思，資料記錄失敗，請稍後再試 " + o.Message.ToString();
        }
        finally
        {
            db.Close();
        }

    }

    public bool CkeckCarIsExist(string[] CarIDArray)
    {
        bool result = true;
        SqlConnection db = new SqlConnection(this.conn);
        string commandText = @"  ";
        for (int i = 0; i < CarIDArray.Length; i++)
        {
            if (i == 0)
            {
                commandText += @" select [CarId] from [car] where [CarId]='" + CarIDArray[i] + "' ";
            }
            else
            {

                commandText += @"  UNION select [CarId] from [car] where [CarId]='" + CarIDArray[i] + "' ";

            }
        }
        SqlCommand cmd = new SqlCommand(commandText, db);

        try
        {
            db.Open();
            SqlDataReader sqlReader = cmd.ExecuteReader();

            Dictionary<string, string> CarDictTempCheck = new Dictionary<string, string>();
            while (sqlReader.Read())
            {
                string carID = sqlReader["carid"].ToString();

                if (!CarDictTempCheck.ContainsKey(carID))
                {
                    CarDictTempCheck.Add(carID, carID);
                }
            }

            for (int i = 0; i < CarIDArray.Length; i++)
            {
                if (!CarDictTempCheck.ContainsKey(CarIDArray[i]))
                {
                    result = false;
                }
            }


            Succ = "Y ";


        }
        catch (Exception o)
        {
            Succ = "N ";

        }
        finally
        {
            db.Close();
        }

        return result;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}