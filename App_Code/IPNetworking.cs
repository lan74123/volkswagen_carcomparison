﻿using System;
using System.Net;
using System.Web;

public class IPNetworking
{
    /// <summary>
    /// 取得客戶端主機 IPv4 位址
    /// </summary>
    /// <returns></returns>
    public static string GetClientIPv4()
    {
        string ipv4 = String.Empty;

        foreach (IPAddress ip in Dns.GetHostAddresses(GetClientIP()))
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                ipv4 = ip.ToString();
                break;
            }
        }

        if (ipv4 != String.Empty)
        {
            return ipv4;
        }

        // 原作使用 Dns.GetHostName 方法取回的是 Server 端資訊，非 Client 端。
        // 改寫為利用 Dns.GetHostEntry 方法，由獲取的 IPv6 位址反查 DNS 紀錄，
        // 再逐一判斷何者屬 IPv4 協定，即可轉為 IPv4 位址。
        foreach (IPAddress ip in Dns.GetHostEntry(GetClientIP()).AddressList)
        //foreach (IPAddress ip in Dns.GetHostAddresses(Dns.GetHostName()))
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                ipv4 = ip.ToString();
                break;
            }
        }

        return ipv4;
    }

    /// <summary>
    /// 取得客戶端主機位址
    /// </summary>
    public static string GetClientIP()
    {
        if (null == HttpContext.Current.Request.ServerVariables["HTTP_VIA"])
        {
            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        else
        {
            return HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        }
    }
}