﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

/// <summary>
/// News 的摘要描述
/// </summary>
public partial class News : BasePage
{
	public News()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
	}

    protected DataTable GetNewsData(int type, int Lang)
    {
        string sSql = "SELECT Serno, Lang, PDate, Img, Title, ShortDesc, Content FROM data_news ";
        sSql += "WHERE Valid=1 AND Lang=@Lang And Type=@Type AND PDate <= getdate() ";
        sSql += "ORDER BY PDate DESC";
        Database db = DBHelper.GetDatabase();
        DbCommand dbComm = db.GetSqlStringCommand(sSql);
        db.AddInParameter(dbComm, "Lang", DbType.Int32, Lang);
        db.AddInParameter(dbComm, "Type", DbType.Int32, type);
        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        return dt;
    }

    protected DataTable GetTop2News(int type, int Lang)
    {
        string sSql = "SELECT top 2 Serno, Lang, PDate, Img, Title, ShortDesc, Content FROM data_news ";
        sSql += "WHERE Valid=1 AND Lang=@Lang And Type=@Type AND PDate <= getdate() ";
        sSql += "ORDER BY PDate DESC";
        Database db = DBHelper.GetDatabase();
        DbCommand dbComm = db.GetSqlStringCommand(sSql);
        db.AddInParameter(dbComm, "Lang", DbType.Int32, Lang);
        db.AddInParameter(dbComm, "Type", DbType.Int32, type);
        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        return dt;
    }

    protected DataTable GetNewsDetail(int serno, int preview, int Lang)
    {
        string sSql = "SELECT Serno, Lang, Type, Img, Title, ShortDesc, Content FROM data_news ";
        if (preview == 1)
            sSql += "WHERE serno=@serno ";
        else
            sSql += "WHERE Valid=1 AND PDate <= getdate() AND serno=@serno ";
        Database db = DBHelper.GetDatabase();
        DbCommand dbComm = db.GetSqlStringCommand(sSql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, serno);
        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        return dt;
    }

    public class NewsLayout
    {
        public int Serno { get; set; }
        public DateTime PDate { get; set; }
        public string Img { get; set; }
        public string Title { get; set; }
        public int Type { get; set; }
        public string ShortDesc { get; set; }
        public string Content { get; set; }
    }
}