﻿using System;
using System.Configuration;
using Microsoft.Security.Application;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Configuration;
using System.Web.Helpers;

/// <summary>
/// ValidateHelper 的摘要描述
/// </summary>
public static class ValidateHelper
{
    public static Encryption.Crypt Crypt = new Encryption.Crypt();

    /// <summary>
    /// 過濾XSS (含去前後空白)
    /// </summary>
    /// <param name="_value"></param>
    public static string FilteXSSValue(string _value)
    {
        if (string.IsNullOrEmpty(_value))
        {
            return @"";
        }
        string rtn = Sanitizer.GetSafeHtmlFragment(_value.Trim());
        rtn = hfjString_SanitizerCompatibleWithChineseCharacters(rtn);
        return rtn;
    }

    /// <summary>
    /// 檢查是否為數字型態
    /// </summary>
    /// <param name="_value">數字字串</param>
    /// <returns>Y/N</returns>
    public static bool IsNumber(string _value)
    {
        Regex objNotNumberPattern = new Regex("[^0-9.-]");
        Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
        Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
        string strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
        string strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
        Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");

        return !objNotNumberPattern.IsMatch(_value) & !objTwoDotPattern.IsMatch(_value) & !objTwoMinusPattern.IsMatch(_value) & objNumberPattern.IsMatch(_value);
    }

    /// <summary>
    /// 檢查是否為合格email 
    /// </summary>
    /// <param name="_value">Email</param>
    /// <returns>Y/N</returns>
    public static bool IsEmail(string _value)
    {
        if (!string.IsNullOrEmpty(_value))
        {
            Regex n = new Regex("[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\\.[a-zA-Z]{2,4}");
            Match v = n.Match(_value);

            if (!v.Success || _value.Length != v.Length)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }

    }


    public static bool IsValidateFormat(string fileFormat, string _arrayformate)
    {

        string[] allowedExtensions = _arrayformate.Split(',');

        Boolean fileOK = false;
        for (int i = 0; i < allowedExtensions.Length; i++)
        {
            if (!string.IsNullOrEmpty(fileFormat))
            {
                if (fileFormat.ToLower() == allowedExtensions[i])
                {
                    fileOK = true;
                }
            }
        }

        return fileOK;
    }



    /// <summary>
    /// 檢查是否為手機號碼:要純數字,且 10 碼, 且 09開頭
    /// </summary>
    /// <param name="_value">手機字串</param>
    /// <returns>Y/N</returns>
    public static bool IsMobile(string _value)
    {
        bool rtn = false;
        if (!string.IsNullOrEmpty(_value) && (IsNumber(_value)) && (_value.Length == 10) && (_value.Substring(0, 2) == "09")) rtn = true;
        return rtn;
    }

    /// <summary>
    /// 檢查是否為日期格式
    /// </summary>
    /// <param name="_value">日期字串</param>
    /// <returns>Y/N</returns>
    public static bool IsDateTime(string _value)
    {
        bool blnIsDate = false;
        try
        {
            DateTime myDateTime = DateTime.Parse(_value);
            blnIsDate = true;
        }
        catch { }
        return blnIsDate;
    }

    /// <summary>
    /// 檢查參數長度是否介於 M to N ，(都設0可以偵測是否為空字串)
    /// </summary>
    /// <param name="_value">字串</param>
    /// <param name="m">小於等於</param>
    /// <param name="n">大於等於</param>
    /// <returns>Y/N</returns>
    public static bool IsLenMToN(string _value, int m, int n)
    {
        if (_value.Length >= m && _value.Length <= n)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 檢查是否為合法的身份證字號
    /// </summary>
    /// <param name="_value">身份證字號</param>
    /// <returns>Y/N</returns>
    public static bool IsIlleagleID(string _value)
    {
        if (string.IsNullOrEmpty(_value))
            return false;   //沒有輸入，回傳 ID 錯誤
        _value = _value.ToUpper();
        Regex regex = new Regex("^[A-Z]{1}[0-9]{9}$");
        if (!regex.IsMatch(_value))
            return false;   //Regular Expression 驗證失敗，回傳 ID 錯誤

        int[] seed = new int[10];       //除了檢查碼外每個數字的存放空間
        string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };
        //A=10 B=11 C=12 D=13 E=14 F=15 G=16 H=17 J=18 K=19 L=20 M=21 N=22
        //P=23 Q=24 R=25 S=26 T=27 U=28 V=29 X=30 Y=31 W=32  Z=33 I=34 O=35
        string target = _value.Substring(0, 1);
        for (int index = 0; index < charMapping.Length; index++)
        {
            if (charMapping[index] == target)
            {
                index += 10;
                seed[0] = index / 10;       //10進制的高位元放入存放空間
                seed[1] = (index % 10) * 9; //10進制的低位元*9後放入存放空間
                break;
            }
        }
        for (int index = 2; index < 10; index++)
        {   //將剩餘數字乘上權數後放入存放空間
            seed[index] = Convert.ToInt32(_value.Substring(index - 1, 1)) * (10 - index);
        }
        //檢查是否符合檢查規則，10減存放空間所有數字和除以10的餘數的個位數字是否等於檢查碼
        //(10 - ((seed[0] + .... + seed[9]) % 10)) % 10 == 身分證字號的最後一碼
        int ss = 0;
        for (int i = 0; i <= 9; i++) ss += seed[i];
        return (10 - (ss % 10)) % 10 == Convert.ToInt32(_value.Substring(9, 1));
    }

    /// <summary>
    /// 檢查SQL Injection
    /// </summary>
    /// <param name="mystr">字串</param>
    /// <returns>True: 安全</returns>
    public static bool IsSQLInjection(string mystr)
    {
        bool rtn = false;
        if (FilteSqlInjection(mystr) == "")
        {
            rtn = true;  //安全的
        }
        else
        {
            rtn = false; //不安全的…含有injection的值
        }
        return rtn;
    }


    /// <summary>
    /// SQL Injection(如果參數存在不安全字符，則返回 不安全字符
    /// </summary>
    /// <param name="_value">檢驗字串</param>
    /// <returns>字串</returns>
    public static string FilteSqlInjection(string _value)
    {
        _value = _value + " ";
        _value = _value.Trim();

        string reStr = "";

        if (!string.IsNullOrEmpty(_value))
        {
            string SQL_injdata = "exec | insert | select | delete | update | count | chr | mid |master|truncate| char |declare|drop |creat | table| and |*|%|'|<|>";
            char[] tmp = { '|' };
            string[] sql_c = SQL_injdata.Split(tmp);
            //string sl = null;
            foreach (string sl in sql_c)
            {
                if (_value.ToLower().IndexOf(sl.TrimStart()) >= 0)
                {
                    reStr = sl;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
        }
        return reStr;
    }

    /// <summary>
    /// 字串加密
    /// </summary>
    /// <param name="_value"></param>
    /// <returns></returns>
    public static string EncryptString(string _value)
    {
        if (!string.IsNullOrEmpty(_value.Trim()))
        {
            return Crypt.EncryptStr(_value);
        }
        return "";
    }

    /// <summary>
    /// 字串解密
    /// </summary>
    /// <param name="_value"></param>
    /// <returns></returns>
    public static string DecryptString(string _value)
    {
        if (!string.IsNullOrEmpty(_value.Trim()))
        {
            return Crypt.Decrypt(_value);
        }
        return "";
    }

    /// <summary>
    /// Google reCaptcha驗證工具
    /// </summary>
    /// <param name="_response"></param>
    /// <returns></returns>
    public static bool VerifyCaptcha(string _response)
    {
        bool result = false;
        string secret = WebConfigurationManager.AppSettings["g-recaptcha-secret"].ToString();
        string param = "secret=" + secret + "&response=" + _response;
        byte[] bs = Encoding.ASCII.GetBytes(param);

        HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("https://www.google.com/recaptcha/api/siteverify");
        req.Method = "POST";
        req.ContentType = "application/x-www-form-urlencoded";
        req.ContentLength = bs.Length;

        using (Stream reqStream = req.GetRequestStream())
        {
            reqStream.Write(bs, 0, bs.Length);
        }
        using (WebResponse wr = req.GetResponse())
        {
            using (StreamReader myStreamReader = new StreamReader(wr.GetResponseStream(), Encoding.GetEncoding("UTF-8")))
            {
                string data = myStreamReader.ReadToEnd();
                try
                {
                    Json.Decode(data);
                }
                catch (Exception)
                {
                    return result;
                }
                var encode = Json.Decode(data);
                if (encode.success)
                {
                    result = true;
                }
            }
        }
        return result;
    }

    private static System.Collections.Generic.Dictionary<string, string> hbjDictionaryFX = new System.Collections.Generic.Dictionary<string, string>();
    /// <summary>
    /// 微軟的AntiXSS v4.0 讓部分漢字亂碼,這裏將亂碼部分漢字轉換回來
    /// </summary>
    /// <param name="hc輸入值"></param>
    /// <returns></returns>
    public static String hfjString_SanitizerCompatibleWithChineseCharacters(String hcString_Para)
    {
        string hbString_ReturnValue = hcString_Para;

        hbString_ReturnValue = hbString_ReturnValue.Replace("\r\n", "");//避免出現<br>等標簽後被認爲加上\r\n的換行符,這會出現在多行textbox控件中,不需要的人請注釋這一行代碼
        if (hbString_ReturnValue.Contains("&#"))
        {
            //Dictionary如果沒有內容就初始化內容
            if (hbjDictionaryFX.Keys.Count == 0)
            {
                lock (hbjDictionaryFX)
                {
                    if (hbjDictionaryFX.Keys.Count == 0)
                    {
                        hbjDictionaryFX.Clear();//防止多線程情況下的不安全情況,雙重檢查理論很完美,但是在多處理器,多線程下,會有平台漏洞,原因是亂序寫入這一cpu或系統功能的存在

                        hbjDictionaryFX.Add("&#20028;", "丼");
                        hbjDictionaryFX.Add("&#20284;", "似");
                        hbjDictionaryFX.Add("&#20540;", "值");
                        hbjDictionaryFX.Add("&#20796;", "儼");
                        hbjDictionaryFX.Add("&#21052;", "刼");
                        hbjDictionaryFX.Add("&#21308;", "匼");
                        hbjDictionaryFX.Add("&#21564;", "吼");
                        hbjDictionaryFX.Add("&#21820;", "唼");
                        hbjDictionaryFX.Add("&#22076;", "嘼");
                        hbjDictionaryFX.Add("&#22332;", "圼");
                        hbjDictionaryFX.Add("&#22588;", "堼");
                        hbjDictionaryFX.Add("&#23612;", "尼");
                        hbjDictionaryFX.Add("&#26684;", "格");
                        hbjDictionaryFX.Add("&#22844;", "夼");
                        hbjDictionaryFX.Add("&#23100;", "娼");
                        hbjDictionaryFX.Add("&#23356;", "嬼");
                        hbjDictionaryFX.Add("&#23868;", "崼");
                        hbjDictionaryFX.Add("&#24124;", "帼");
                        hbjDictionaryFX.Add("&#24380;", "弼");
                        hbjDictionaryFX.Add("&#24636;", "怼");
                        hbjDictionaryFX.Add("&#24892;", "愼");
                        hbjDictionaryFX.Add("&#25148;", "戼");
                        hbjDictionaryFX.Add("&#25404;", "挼");
                        hbjDictionaryFX.Add("&#25660;", "搼");
                        hbjDictionaryFX.Add("&#25916;", "攼");
                        hbjDictionaryFX.Add("&#26172;", "晝");
                        hbjDictionaryFX.Add("&#26428;", "朼");
                        hbjDictionaryFX.Add("&#26940;", "椼");
                        hbjDictionaryFX.Add("&#27196;", "樼");
                        hbjDictionaryFX.Add("&#27452;", "欼");
                        hbjDictionaryFX.Add("&#27708;", "氼");
                        hbjDictionaryFX.Add("&#27964;", "窪");
                        hbjDictionaryFX.Add("&#28220;", "渼");
                        hbjDictionaryFX.Add("&#28476;", "漼");
                        hbjDictionaryFX.Add("&#28732;", "瀼");
                        hbjDictionaryFX.Add("&#28988;", "焼");
                        hbjDictionaryFX.Add("&#29244;", "爼");
                        hbjDictionaryFX.Add("&#29500;", "猼");
                        hbjDictionaryFX.Add("&#29756;", "瓊");
                        hbjDictionaryFX.Add("&#30012;", "甼");
                        hbjDictionaryFX.Add("&#30268;", "瘼");
                        hbjDictionaryFX.Add("&#30524;", "眼");
                        hbjDictionaryFX.Add("&#30780;", "砼");
                        hbjDictionaryFX.Add("&#31036;", "禮");
                        hbjDictionaryFX.Add("&#31292;", "稼");
                        hbjDictionaryFX.Add("&#31548;", "籠");
                        hbjDictionaryFX.Add("&#31804;", "簼");
                        hbjDictionaryFX.Add("&#32060;", "紼");
                        hbjDictionaryFX.Add("&#32316;", "縼");
                        hbjDictionaryFX.Add("&#32572;", "缼");
                        hbjDictionaryFX.Add("&#32828;", "耼");
                        hbjDictionaryFX.Add("&#33084;", "脼");
                        hbjDictionaryFX.Add("&#40;", "舼");
                        hbjDictionaryFX.Add("&#33596;", "茼");
                        hbjDictionaryFX.Add("&#33852;", "萼");
                        hbjDictionaryFX.Add("&#34108;", "藹");
                        hbjDictionaryFX.Add("&#36156;", "賊");
                        hbjDictionaryFX.Add("&#39740;", "鬼");
                    }
                }

            }

            //開始替換的遍曆
            foreach (string key in hbjDictionaryFX.Keys)
            {
                if (hbString_ReturnValue.Contains(key))
                {
                    hbString_ReturnValue = hbString_ReturnValue.Replace(key, hbjDictionaryFX[key]);
                }
            }

        }

        return hbString_ReturnValue;
    }

    /// <summary>
    /// 字串加密(AES)
    /// </summary>
    /// <param name="_value"></param>
    /// <returns></returns>
    public static string EncryptStringByAES(string _value)
    {
        string key = "urNchI6SxtoHIkBeNmANws12j0iAyNae";
        string iv = "6cDONwiTCyAnzkrE";

        byte[] encryptBytes = Encoding.UTF8.GetBytes(_value);
        var aes = new RijndaelManaged();
        aes.Key = Encoding.UTF8.GetBytes(key);
        aes.IV = Encoding.UTF8.GetBytes(iv);
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;

        ICryptoTransform transform = aes.CreateEncryptor();

        return Convert.ToBase64String(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length));
    }

    /// <summary>
    /// 字串解密(AES)
    /// </summary>
    /// <param name="_value"></param>
    /// <returns></returns>
    public static string DecryptStringByAES(string _value)
    {
        string key = "urNchI6SxtoHIkBeNmANws12j0iAyNae";
        string iv = "6cDONwiTCyAnzkrE";

        byte[] decryptBytes = Convert.FromBase64String(_value);
        var aes = new RijndaelManaged();
        aes.Key = Encoding.UTF8.GetBytes(key);
        aes.IV = Encoding.UTF8.GetBytes(iv);
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;

        ICryptoTransform transform = aes.CreateDecryptor();

        return Encoding.UTF8.GetString(transform.TransformFinalBlock(decryptBytes, 0, decryptBytes.Length));
    }

}