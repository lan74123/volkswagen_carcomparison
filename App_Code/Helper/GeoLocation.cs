﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;


/// <summary>
/// GeoLocation 的摘要描述
/// </summary>
public static class GeoLocation
{
   

    /// <summary>
    /// 地址取得經緯度
    /// </summary>
    /// <param name="address">地址</param>
    public static Geometry getLatandLongByAddress(string address)
    {

        var url = String.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}", address);

        string result = String.Empty;
        Geometry geo = new Geometry();


        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
        using (var response = request.GetResponse())
        using (StreamReader sr = new StreamReader(response.GetResponseStream()))
        {
            //Json格式: 請參考HTTP://code.google.com/intl/zh-TW/apis/maps/documentation/geocoding/
            result = sr.ReadToEnd();


            GeoLocationObject googleData = JsonConvert.DeserializeObject<GeoLocationObject>(result);
            if (googleData.status.ToString() == "OK") {             
                foreach(Result res in googleData.results){
                    geo = new Geometry();
                    geo = res.geometry;
                }            
            }
        }


        return geo;
    }
   
}