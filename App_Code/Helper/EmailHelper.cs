﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Text;

/// <summary>
/// EmailHelper 的摘要描述
/// </summary>
public static class EmailHelper
{
	public static string SendEmail(string _from, string _fromname, string _to, string _toname, string _cc, string _subject, string _body)
	{
		string _result = "";
		try
		{			
			//設定Email
			MailMessage MyEmail = new MailMessage();
			//寄件者收件者
			MyEmail.From = new MailAddress(_from, _fromname, Encoding.UTF8); //信箱隨便打都可以
			if (!string.IsNullOrEmpty(_to)) MyEmail.To.Add(_to);
			if (!string.IsNullOrEmpty(_cc)) MyEmail.CC.Add(_cc);

			//標題
			MyEmail.SubjectEncoding = Encoding.UTF8;
			MyEmail.Subject = _subject;
			//內文
			MyEmail.BodyEncoding = System.Text.Encoding.GetEncoding("UTF-8");
			MyEmail.Body = _body;
			MyEmail.IsBodyHtml = true;
			MyEmail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
			//寄送設定
			System.Net.Mail.SmtpClient smtpClient = new SmtpClient("127.0.0.1", 25);

			smtpClient.Send(MyEmail);
			MyEmail.Dispose();
			//刪除附件
		}
		catch (Exception o)
		{
			_result = "error";
			throw (o);
		}
		finally
		{
			_result = "success";
		}
		return _result;
	}
}