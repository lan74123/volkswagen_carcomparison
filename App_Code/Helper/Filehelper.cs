﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Newtonsoft.Json;


/// <summary>
/// filehelper 的摘要描述
/// </summary>
public static class FileHelper
{
    /// <summary>
    /// 上傳檔案 (共用模組), 覆蓋掉舊檔
    /// </summary>
    /// <param name="_objuploadfile">FileUpload Object</param>
    /// <param name="_uploadfolder">要上傳到的Folder Ex:fileupload/fontstory_kv/</param>
    /// <param name="_arrayformate">可接受格式 (以','分隔)</param>
    /// <param name="_filename">檔案名稱(含副檔名)</param>
    /// <returns></returns>
    public static string UploadFile(FileUpload _objuploadfile, string _uploadfolder, string _arrayformate, string _filename)
    {
        string mErrMsg = string.Empty;
        string mUploadPath = string.Empty;
        string mUploadDir = string.Empty;
        HttpServerUtility serverutility = HttpContext.Current.Server;

        if (_objuploadfile == null || !_objuploadfile.HasFile)
        {
            return "該文件不存在";
        }

        bool formatFlag = false;  //預設是不合法的格式
        string fileFormat = Path.GetExtension(_objuploadfile.FileName).ToLower().TrimEnd('.');

        formatFlag = ValidateHelper.IsValidateFormat(fileFormat, _arrayformate);

        if (!formatFlag)
        {
            mErrMsg += "文件格式錯誤（支持格式：" + _arrayformate.ToString() + ") \\r";
        }

        if (string.IsNullOrEmpty(mErrMsg))
        {
            mUploadDir = Path.Combine(serverutility.MapPath("~/"), _uploadfolder) + "/";

            bool isExists = System.IO.Directory.Exists(mUploadDir);

            if (!isExists)
            {
                System.IO.Directory.CreateDirectory(mUploadDir);
            }

            mUploadPath = Path.Combine(mUploadDir, _filename);
            _objuploadfile.SaveAs(mUploadPath);
        }

        return mErrMsg;
    }

    public static string UploadFile(HttpPostedFile _objuploadfile, string _uploadfolder, string _arrayformate, string _filename)
    {
        string mErrMsg = string.Empty;
        string mUploadPath = string.Empty;
        string mUploadDir = string.Empty;
        HttpServerUtility serverutility = HttpContext.Current.Server;

        if (_objuploadfile == null || _objuploadfile.ContentLength <= 0)
        {
            return "該文件不存在";
        }

        bool formatFlag = false;  //預設是不合法的格式
        string fileFormat = Path.GetExtension(_objuploadfile.FileName).ToLower().TrimEnd('.');

        formatFlag = ValidateHelper.IsValidateFormat(fileFormat, _arrayformate);

        if (!formatFlag)
        {
            mErrMsg += "文件格式錯誤（支持格式：" + _arrayformate.ToString() + ") \\r";
        }

        if (string.IsNullOrEmpty(mErrMsg))
        {
            mUploadDir = Path.Combine(serverutility.MapPath("~/"), _uploadfolder + "/");

            bool isExists = System.IO.Directory.Exists(mUploadDir);

            if (!isExists)
            {
                System.IO.Directory.CreateDirectory(mUploadDir);
            }


            mUploadPath = Path.Combine(mUploadDir, _filename);
            _objuploadfile.SaveAs(mUploadPath);
        }

        return mErrMsg;
    }

    /// <summary>
    /// 刪除檔案
    /// </summary>
    /// <param name="_filename">檔案名稱(含副檔名)</param>
    /// <param name="_folder">要刪除資料的資料夾位置 Ex: fileupload/fontstory_kv/</param>
    public static void DeleteFile(string _filename, string _folder)
    {
        if (!string.IsNullOrEmpty(_filename))
        {
            HttpServerUtility serverutility = HttpContext.Current.Server;
            try
            {
                string deletefilepath = Path.Combine(serverutility.MapPath("~/"), _folder + _filename);
                System.IO.File.Delete(deletefilepath);
            }
            catch { }
        }
    }

    public static string ReadFileContent(string _filename, string _folder)
    {
        if (!string.IsNullOrEmpty(_filename) && !string.IsNullOrEmpty(_folder))
        {
            HttpServerUtility serverutility = HttpContext.Current.Server;
            try
            {
                string filepath = Path.Combine(serverutility.MapPath("~/"), _folder + _filename);
                return System.IO.File.ReadAllText(filepath);
            }
            catch (Exception)
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }

    public static bool IsExistedFile(string _filepath)
    {
        return File.Exists(_filepath);
    }

    public static string getUniqueFileName(string folderpath, string filename)
    {
        string rtn = "";
        string tmp_filename = filename;
        for (int i = 1; i <= 100; i++)
        {
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/") + folderpath + tmp_filename))
            {
                rtn = tmp_filename;
                break;
            }
            else
            {
                tmp_filename = Path.GetFileNameWithoutExtension(filename) + "_" + i + Path.GetExtension(filename);
                continue;

            }
        }
        return rtn;
    }

    public static string CheckExtension(HttpPostedFile _objuploadfile, string _arrayformate)
    {
        if (_objuploadfile == null || _objuploadfile.ContentLength <= 0)
        {
            return "該文件不存在";
        }

        string fileFormat = Path.GetExtension(_objuploadfile.FileName).ToLower().TrimEnd('.');

        if (!ValidateHelper.IsValidateFormat(fileFormat, _arrayformate))
        {
            return "文件格式錯誤（支持格式：" + _arrayformate.ToString() + ")";
        }

        return "";
    }
    public static void StringToFile(string _str, string filename = "")
    {
        if (string.IsNullOrEmpty(filename)) filename = "MailBody_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
        HttpContext context = HttpContext.Current;
        context.Response.Clear();
        // 指定編碼
        //context.Response.HeaderEncoding = System.Text.Encoding.Default;
        // 加入BOM
        //context.Response.BinaryWrite(new byte[] { 0xEF, 0xBB, 0xBF });
        context.Response.AddHeader("Pragma", "private");
        context.Response.AddHeader("Cache-Control", "private");

        context.Response.ContentEncoding = System.Text.Encoding.UTF8;  //2016/9/7 因為其他地方下載為csv 客戶希望可以直接用excel開，所以會設為big-5 , 但是這裡用big5 信內文會亂碼 所以直接指定 utf-8 //Config.TextEncoding; //台灣版
        //context.Response.ContentEncoding = System.Text.Encoding.GetEncoding(932); //"shift_jis"  <<< 日本版專用
        //context.Response.BinaryWrite(Encoding.UTF8.GetPreamble());
        context.Response.ContentType = "text/csv";
        context.Response.AddHeader("Content-type", "text/csv");
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        // context.Response.AddHeader("Pragma", "public");


        context.Response.Write(_str);

        context.Response.End();
    }


    /// <summary>
    /// 上傳檔案 (共用模組), 覆蓋掉舊檔 resize圖檔
    /// </summary>
    /// <param name="_objuploadfile">FileUpload Object</param>
    /// <param name="_uploadfolder">要上傳到的Folder Ex:fileupload/fontstory_kv/</param>
    /// <param name="_arrayformate">可接受格式 (以','分隔)</param>
    /// <param name="_filename">檔案名稱(含副檔名)</param>
    /// <param name="resizeWidth">resize寬</param>
    /// <param name="resizeHeight">resize高</param>
    /// <param name="drawPlace">0:左上 1:上下左右置中</param>
    /// <returns></returns>
    public static string UploadFileResize(FileUpload _objuploadfile, string _uploadfolder, string _arrayformate, string _filename, int resizeWidth, int resizeHeight, int drawPlace)
    {
        string mErrMsg = string.Empty;
        string mUploadPath = string.Empty;
        string mUploadDir = string.Empty;
        HttpServerUtility serverutility = HttpContext.Current.Server;

        if (_objuploadfile == null || !_objuploadfile.HasFile)
        {
            return "該文件不存在";
        }

        bool formatFlag = false;  //預設是不合法的格式
        string fileFormat = Path.GetExtension(_objuploadfile.FileName).ToLower().TrimEnd('.');

        formatFlag = ValidateHelper.IsValidateFormat(fileFormat, _arrayformate);

        if (!formatFlag)
        {
            mErrMsg += "文件格式錯誤（支持格式：" + _arrayformate.ToString() + ") \\r";
        }

        if (string.IsNullOrEmpty(mErrMsg))
        {
            mUploadDir = Path.Combine(serverutility.MapPath("~/"), _uploadfolder) + "/";

            bool isExists = System.IO.Directory.Exists(mUploadDir);

            if (!isExists)
            {
                System.IO.Directory.CreateDirectory(mUploadDir);
            }

            mUploadPath = Path.Combine(mUploadDir, _filename);
            //resize 圖檔
            HttpPostedFile pf = _objuploadfile.PostedFile;
            System.Drawing.Image bm = System.Drawing.Image.FromStream(pf.InputStream);
            bm = ScaleImage(bm, resizeWidth, resizeHeight, drawPlace);

            if (fileFormat.Equals(".png"))
            {
                bm.Save(mUploadPath, ImageFormat.Png);
            }
            else
            {
                bm.Save(mUploadPath, ImageFormat.Jpeg);
            }


        }

        return mErrMsg;
    }

    public static string UploadFileResize(HttpPostedFile _objuploadfile, string _uploadfolder, string _arrayformate, string _filename, int resizeWidth, int resizeHeight, int drawPlace)
    {
        string mErrMsg = string.Empty;
        string mUploadPath = string.Empty;
        string mUploadDir = string.Empty;
        HttpServerUtility serverutility = HttpContext.Current.Server;

        if (_objuploadfile == null || _objuploadfile.ContentLength <= 0)
        {
            return "該文件不存在";
        }

        bool formatFlag = false;  //預設是不合法的格式
        string fileFormat = Path.GetExtension(_objuploadfile.FileName).ToLower().TrimEnd('.');

        formatFlag = ValidateHelper.IsValidateFormat(fileFormat, _arrayformate);

        if (!formatFlag)
        {
            mErrMsg += "文件格式錯誤（支持格式：" + _arrayformate.ToString() + ") \\r";
        }

        if (string.IsNullOrEmpty(mErrMsg))
        {
            mUploadDir = Path.Combine(serverutility.MapPath("~/"), _uploadfolder + "/");

            bool isExists = System.IO.Directory.Exists(mUploadDir);

            if (!isExists)
            {
                System.IO.Directory.CreateDirectory(mUploadDir);
            }

            mUploadPath = Path.Combine(mUploadDir, _filename);
            //resize 圖檔
            HttpPostedFile pf = _objuploadfile;
            System.Drawing.Image bm = System.Drawing.Image.FromStream(pf.InputStream);
            bm = ScaleImage(bm, resizeWidth, resizeHeight, drawPlace);

            if (fileFormat.Equals(".png"))
            {
                bm.Save(mUploadPath, ImageFormat.Png);
            }
            else
            {
                bm.Save(mUploadPath, ImageFormat.Jpeg);
            }
        }

        return mErrMsg;
    }

    /// <summary>產生亂數字串</summary>
    /// <param name="Number">字元數</param>
    /// <returns></returns>
    public static string CreateRandomCode(int Number)
    {
        string allChar = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        string[] allCharArray = allChar.Split(',');
        string randomCode = "";
        int temp = -1;

        Random rand = new Random();
        for (int i = 0; i < Number; i++)
        {
            if (temp != -1)
            {
                rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
            }
            int t = rand.Next(allCharArray.Length);
            if (temp != -1 && temp == t)
            {
                return CreateRandomCode(Number);
            }
            temp = t;
            randomCode += allCharArray[t];
        }
        return randomCode;
    }

    /// <summary>resize 圖檔</summary>
    /// <param name="image">原圖</param>
    /// <param name="newWidth">寬 0:不限制寬</param>
    ///  <param name="newHeight">高 0:不限制高</param>
    ///  <param name="drawPlace">0:左上 1:上下左右置中</param>
    /// <returns></returns>
    public static System.Drawing.Image ScaleImage(System.Drawing.Image image, int newWidth, int newHeight, int drawPlace)
    {
        if (newHeight == 0)
        {
            newHeight = (int)(newWidth * image.Height / image.Width);
        }

        if (newWidth == 0)
        {
            newHeight = (int)(newHeight * image.Width / image.Height);
        }

        Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format32bppArgb);

        using (Graphics g = Graphics.FromImage(newImage))
        {
            g.Clear(Color.Transparent);
            //將各像成像品質設定為HighQuality
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //設定重畫縮圖範圍
            Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);

            if (drawPlace == 1)
            {
                //先從大圖縮小再裁切
                g.DrawImage(image, imageRectangle);
                newImage = cropImg(newImage, 550, 310);
            }
            else
            {
                g.DrawImage(image, imageRectangle);
            }
        }
        return newImage;
    }

    public static Bitmap cropImg(System.Drawing.Image image, int newWidth, int newHeight)
    {
        Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format32bppArgb);

        using (Graphics g = Graphics.FromImage(newImage))
        {
            g.Clear(Color.Transparent);
            //將各像成像品質設定為HighQuality
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //設定重畫縮圖範圍
            Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);

            Bitmap src = (Bitmap)image;
            //設定裁切範圍
            Rectangle cropRect = new Rectangle(image.Width / 2 - newWidth / 2, image.Height / 2 - newHeight / 2, newWidth, newHeight);
            //於座標(0,0)開始繪製裁切影像
            g.DrawImage(src, new Rectangle(0, 0, newWidth, newHeight), cropRect, GraphicsUnit.Pixel);
            g.DrawImage(image, imageRectangle);
        }

        return newImage;
    }


    public static ImageCodecInfo GetEncoderInfo(string mimeType)
    {
        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
        for (int i = 0; i < codecs.Length; i++)
            if (codecs[i].MimeType == mimeType)
                return codecs[i];
        return null;
    }


    /// <summary>產生json檔案</summary>
    /// <param name="datas">object</param>
    /// <returns></returns>
    public static void saveDataToJsonFile(object datas, string filename)
    {
        HttpServerUtility serverutility = HttpContext.Current.Server;

        string jsonString = JsonConvert.SerializeObject(datas);

        string mUploadDir = Path.Combine(serverutility.MapPath("~/"), "json") + "/" + filename;
        try
        {
            System.IO.File.WriteAllText(mUploadDir, jsonString);
        }
        catch (Exception ex)
        {
            throw (ex);

        }

    }

    public static void saveDataToJsonFileByJsonNet(object datas, string filename)
    {
        HttpServerUtility serverutility = HttpContext.Current.Server;

        string jsonString = JsonConvert.SerializeObject(datas);

        string mUploadDir = Path.Combine(serverutility.MapPath("~/"), "json") + "/" + filename;
        try
        {
            System.IO.File.WriteAllText(mUploadDir, jsonString);
        }
        catch (Exception ex)
        {
            throw (ex);

        }

    }


}