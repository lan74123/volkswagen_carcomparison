﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// WebBasePage 的摘要描述
/// </summary>
public class WebBasePage :BasePage
{
    /// <summary>
    /// 前端彈出視窗
    /// </summary>
    /// <param name="info">訊息</param>
    protected void JSOutPut(string info)
    {
        string strScripts;
        strScripts = "<script language=javascript>";
        strScripts += "window.alert('" + info + "');";
        strScripts += "</script>";
        ClientScript.RegisterStartupScript(this.GetType(), System.DateTime.Now.ToString(), strScripts);
    }

    /// <summary>
    /// 前端轉址
    /// </summary>
    /// <param name="url">網址</param>
    protected void JSRedirect(string url)
    {
        string strScripts;
        strScripts = "<script language=javascript>\n";
        strScripts += "location.href=\"" + url + "\"";
        strScripts += "</script>";

        ClientScript.RegisterStartupScript(this.GetType(), System.DateTime.Now.ToString(), strScripts);
    }

    /// <summary>
    /// 前端彈出視窗並且轉址
    /// </summary>
    /// <param name="info"></param>
    /// <param name="url"></param>
    protected void JSOutPutAndRedirect(string info, string url)
    {
        string strScripts;
        strScripts = "<script language=javascript>\n";
        strScripts += "window.alert('" + info + "');";
        strScripts += "location.href=\"" + url + "\"";
        strScripts += "</script>";

        ClientScript.RegisterStartupScript(this.GetType(), System.DateTime.Now.ToString(), strScripts);
    }

    /// <summary>
    /// 前端彈出視窗並且關閉網頁 (好像會有問題)
    /// </summary>
    /// <param name="info"></param>
    protected void JSOutPutAndClose(string info)
    {
        string strScripts;
        strScripts = "<script language=javascript>\n";
        strScripts += "window.alert('" + info + "');";
        strScripts += "window.opener=null; window.open('','_self');window.close();";
        strScripts += "</script>";

        ClientScript.RegisterStartupScript(this.GetType(), System.DateTime.Now.ToString(), strScripts);
    }
}