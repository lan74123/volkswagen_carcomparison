﻿using System;

//Parse成Json時，如果需要變成字串的話，在上方加上這個Contract[JsonConverter(typeof(StringEnumConverter))]

public enum Value_Y_N
{
    N=0,
    Y=1,
}

//圖片訂定起始位置

public enum redrawImgPlace : int
{
    TopLeft = 0,
    Center = 1
}