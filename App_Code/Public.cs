﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// Public 的摘要描述
/// </summary>
public class Public
{
    public Public()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }
    [Serializable()]
    //CarType
    public class CarTypeObj
    {
        public string CarTypeName { get; set; }
        public List<CarObj> CarList { get; set; }
    }
    //car
    public class CarObj
    {
        public string CarID { get; set; }
        public string CarTypeName { get; set; }
        public string CarName { get; set; }

    }
    //CarImg
    public class CarImgObj
    {
        public string CarID { get; set; }
        public string CarTypeName { get; set; }
        public string CarName { get; set; }
        public string CarImg { get; set; }
    }



    public static List<CarTypeObj> GetIndexCarType()
    {
        List<CarTypeObj> result = new List<CarTypeObj>();
        Dictionary<string, CarTypeObj> CarTypeNameDictTemp = new Dictionary<string, CarTypeObj>();

        Database db = DBHelper.GetDatabase();
        string sql = @"SELECT ct.CarTypeName ,c.carName,c.CarID
                      FROM [dbo].[Car_online] c
                      left join [CarType] ct
                      on c.CarTypeID=ct.CarTypeID where ct.Valid='1' and  c.StockStatus='S'
                      order by ct.Corder , c.Corder desc";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                string carID = dr["CarID"].ToString();
                string carTypeName = dr["CarTypeName"].ToString();
                string carName = dr["carName"].ToString();

                if (!CarTypeNameDictTemp.ContainsKey(carTypeName))
                {
                    List<CarObj> CarList = new List<CarObj>();
                    CarObj carobj = new CarObj()
                    {
                        CarID = carID,
                        CarTypeName = carTypeName,
                        CarName = carName
                    };
                    CarList.Add(carobj);

                    CarTypeObj ctobj = new CarTypeObj();
                    ctobj.CarTypeName = carTypeName;
                    ctobj.CarList = CarList;

                    CarTypeNameDictTemp.Add(carTypeName, ctobj);
                }
                else
                {
                    List<CarObj> CarList = CarTypeNameDictTemp[carTypeName].CarList;
                    CarObj carobj = new CarObj()
                    {
                        CarID = carID,
                        CarTypeName = carTypeName,
                        CarName = carName
                    };
                    CarList.Add(carobj);
                    CarTypeNameDictTemp[carTypeName].CarList = CarList;

                    CarTypeNameDictTemp[carTypeName] = CarTypeNameDictTemp[carTypeName];
                }
            }

            foreach (string key in CarTypeNameDictTemp.Keys)
            {
                result.Add(CarTypeNameDictTemp[key]);
            }
        }
        else
        {
            result = null;
        }



        return result;
    }

    public static List<CarImgObj> GetIndexCarImage()
    {
        List<CarImgObj> result = new List<CarImgObj>();


        Database db = DBHelper.GetDatabase();
        string sql = @"    SELECT top 2 ct.CarTypeName,c.CarName,c.CarID,c.CarImg
                          FROM [dbo].[Car_online] c
                          left join [CarType] ct
                          on c.CarTypeID=ct.CarTypeID
                          where c.showhomepage='1' and c.StockStatus='S' order by c.mdate";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {

                string carID = dr["CarID"].ToString();
                string carTypeName = dr["CarTypeName"].ToString();
                string carImg = dr["CarImg"].ToString();
                string carName = dr["CarName"].ToString();

                CarImgObj carimg = new CarImgObj();
                carimg.CarID = carID;
                carimg.CarImg = carImg;
                carimg.CarName = carName;
                carimg.CarTypeName = carTypeName;
                result.Add(carimg);

            }


        }
        else
        {
            result = null;
        }



        return result;
    }



    public static void GenIndexCarTypeJson()
    {
        List<CarTypeObj> result = GetIndexCarType();
        string Json = JsonConvert.SerializeObject(result, Formatting.Indented);
        FileHelper.saveDataToJsonFile(result, "index_cartype.json");
    }

    public static void GenIndexCarImageJson()
    {
        List<CarImgObj> result = GetIndexCarImage();
        string Json = JsonConvert.SerializeObject(result, Formatting.Indented);
        FileHelper.saveDataToJsonFile(result, "index_carImage.json");
    }


}