﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.UI;

/// <summary>
/// BasePage 的摘要描述
/// </summary>
public class BasePage : System.Web.UI.Page
{
    public BasePage()
    {
        this.PreRender += new EventHandler(Page_PreRender);
    }


    private void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetActionStamp();
        }

        ClientScript.RegisterHiddenField("actionStamp", Session["actionStamp"] as string);
    }

    protected override void OnLoad(EventArgs e)
    {
        //相容性設定，若前端有設定則會依照前端的設定
        Response.AddHeader("X-UA-Compatible", "IE=edge,chrome=1");

        //確認來源
        if (Request.ServerVariables["HTTP_HOST"] + "/" == Config.Domain || HttpContext.Current.Request.IsLocal.Equals(true))
        {
            //網站是否要啟用SSL
            if (Config.IsSSL == Value_Y_N.Y)
            {
                if (HttpContext.Current.Request.IsSecureConnection.Equals(false) && HttpContext.Current.Request.IsLocal.Equals(false))
                {
                    Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
                }
            }
        }
        else if (Request.ServerVariables["HTTP_HOST"] + "/" == "www.volkswagentaiwan.com.tw/")
        {

            Response.Redirect("http://" + Config.Domain);

        }
        else {

            Response.End();
        }

        if (!IsUUIDSet())
        {
            uuid = Guid.NewGuid().ToString();
            Response.Cookies["uuid"].Value = uuid;
            Response.Cookies["uuid"].Expires = DateTime.Now.AddYears(1);
        }
        else
        {
            uuid = Request.Cookies["uuid"].Value.ToString();
            Response.Cookies["uuid"].Value = uuid;
            Response.Cookies["uuid"].Expires = DateTime.Now.AddYears(1);
        }

        if (!string.IsNullOrEmpty(Request["utm_source"]))
        {
            Response.Cookies["UtmSource"].Value = Request["utm_source"];
            Response.Cookies["UtmSource"].Expires = DateTime.Now.AddHours(8);
        }

        if (!string.IsNullOrEmpty(Request["utm_medium"]))
        {
            Response.Cookies["UtmMedium"].Value = Request["utm_medium"];
            Response.Cookies["UtmMedium"].Expires = DateTime.Now.AddHours(8);
        }

        try
        {
            UtmSource = string.IsNullOrEmpty(Request.Cookies["UtmSource"].Value) ? "" : Request.Cookies["UtmSource"].Value;
        }
        catch
        {
            UtmSource = "";
        }
        try
        {
            UtmMedium = string.IsNullOrEmpty(Request.Cookies["UtmMedium"].Value) ? "" : Request.Cookies["UtmMedium"].Value;
        }
        catch
        {
            UtmMedium = "";
        }

        //如果 client想訪問 / backend 目錄
        // 會先檢查該ip是否為許可ip 若非則導向首頁

        if (Request.Path.Contains("backend/"))
        {

            string client_ip = Request.ServerVariables["REMOTE_ADDR"];
            //string test = client_ip.Substring(0, 8);
            string allow_ip = "122.147.";
            string allow_ip2 = "122.146."; //wifi
            string allow_ip3 = "::1";
            string allow_ip4 = "202.136.166.173";

            if (!client_ip.Contains(allow_ip) && !client_ip.Contains(allow_ip2) && !client_ip.Contains(allow_ip3) && !client_ip.Contains(allow_ip4))
            {
                Response.Redirect("../index.html");
            }
            else
            {
                base.OnLoad(e);
            }
        }

        //base.OnLoad(e);
    }

    /// <summary>
    /// 設置戳記
    /// </summary>
    private void SetActionStamp()
    {
        Session["actionStamp"] = Server.UrlEncode(DateTime.Now.ToString());
    }

    /// <summary>
    /// 取得值，指出網頁是否經由重新整理動作回傳 (PostBack)
    /// </summary>
    protected bool IsRefresh
    {
        get
        {
            if (HttpContext.Current.Request["actionStamp"] as string == Session["actionStamp"] as string)
            {
                SetActionStamp();
                return false;
            }
            return true;
        }
    }

    public bool IsUUIDSet()
    {
        bool result = false;
        string t = "";
        try
        {
            t = Request.Cookies["uuid"].Value.ToString();
            result = true;
        }
        catch
        {
            result = false;
        }
        return result;
    }

    public void OutPut(string info)
    {
        string strScripts;
        strScripts = "<script language=javascript>\n";
        strScripts += "alert('" + info + "');";
        strScripts += "</script>";

        ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", strScripts);
    }

    public void RunJS(string script)
    {
        string strScripts;
        strScripts = "<script language=javascript>";
        strScripts += script;
        strScripts += "</script>";

        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "RunScript_" + Guid.NewGuid(), strScripts, true);
    }

    public void OutPutAndRedirect(string info, string url)
    {
        string strScripts;
        strScripts = "<script language=javascript>\n";
        strScripts += "window.alert('" + info + "');";
        strScripts += "top.location.href=\"" + url + "\"";
        strScripts += "</script>";
        ClientScript.RegisterClientScriptBlock(GetType(), "", strScripts);
        //Page.RegisterClientScriptBlock(System.DateTime.Now.ToString(), strScripts);
    }

    public string GetLoginBefore()
    {
        string result = "";

        try
        {
            result = Request.Cookies["login_before"].Value.ToString();
        }
        catch
        {
            result = "";
        }

        return result;
    }

    public string uuid { get; set; }
    public string UtmSource { get; set; }
    public string UtmMedium { get; set; }
}