﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

/// <summary>
/// Retail 的摘要描述
/// </summary>
public partial class Retail : BasePage
{
    public Retail()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
	}

    protected DataTable GetStoreList(int AreaType, int type, string city, string area, string keyword, string country)
    {
        string sSql = "SELECT DS.Serno, DS.Country, DS.AreaType, DS.StoreType, DS.Name, DS.Phone, DS.City, DS.Area, DS.Addr, DS.Time FROM data_store DS ";
        sSql += "LEFT JOIN data_country DC on DC.Serno=DS.Country WHERE Valid=1 AND AreaType=@AreaType ";
        sSql += (country == "" ? "" : " And " + "DC.Serno=@country");
        sSql += (type == 0 ? "" : " And " + "StoreType=@type");
        sSql += (city == "" ? "" : " And " + "city=@city");
        sSql += (area == "" ? "" : " And " + "area=@area");
        sSql += (keyword == "" ? "" : " And " + "(Name like N'%" + @keyword + "%' Or Addr like N'%" + @keyword + "%' Or city like N'%" + @keyword + "%' Or area like N'%" + @keyword + "%')");

        Database db = DBHelper.GetDatabase();
        DbCommand dbComm = db.GetSqlStringCommand(sSql);
        db.AddInParameter(dbComm, "AreaType", DbType.Int32, AreaType);
        db.AddInParameter(dbComm, "country", DbType.String, country);
        db.AddInParameter(dbComm, "type", DbType.Int32, type);
        db.AddInParameter(dbComm, "city", DbType.String, city);
        db.AddInParameter(dbComm, "area", DbType.String, area);
        db.AddInParameter(dbComm, "keyword", DbType.String, keyword);
        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        return dt;
    }

    public class Store
    {
        public int Serno { get; set; }
        public string Country { get; set; }
        public int AreaType { get; set; }
        public int StoreType { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
        public string Addr { get; set; }
        public string Time { get; set; }
    }
}