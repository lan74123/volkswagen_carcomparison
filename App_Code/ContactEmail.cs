﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// ContactEmail 的摘要描述
/// </summary>
public class ContactEmail
{
	public ContactEmail()
	{
		//
		// TODO: 在這裡新增建構函式邏輯
		//
	}

	public static bool SendValuation(int dataid, int showroomid)
	{
		bool result = false;

		StringBuilder builder = new StringBuilder();
		DataTable EmailList = GetEmailList(showroomid);
		DataTable ValuationData = GetValuationData(dataid);

		MailMessage message = new MailMessage();
		//message.From = new MailAddress(ValuationData.Rows[0]["CustEmail"].ToString());
		message.From = new MailAddress("no-reply@mercedes-benz-select.com.tw");
		message.Subject = "賓士精選中古車 車輛鑑價服務留言 客戶:" + ValuationData.Rows[0]["CustName"].ToString();
		message.BodyEncoding = Encoding.UTF8;

		for (int i = 0; i < EmailList.Rows.Count; i++)
		{
			if (!string.IsNullOrEmpty(EmailList.Rows[i]["Email"].ToString()))
			{
				if (EmailList.Rows[i]["Email"].ToString().IndexOf(',') > 0)
				{
					string[] m = EmailList.Rows[i]["Email"].ToString().Split(',');
					foreach (var k in m)
					{
						if (!string.IsNullOrEmpty(k))
						{
							message.To.Add(k);
							builder.Append(k + ",");
						}
					}
				}
				else
				{
					message.To.Add(EmailList.Rows[i]["Email"].ToString().Trim());
					builder.Append(EmailList.Rows[i]["Email"].ToString().Trim() + ",");
				}
			}
		}

		builder.Remove(builder.Length - 1, 1);

		MailDefinition definition = new MailDefinition();
		definition.BodyFileName = @"C:\inetpub\wwwroot\Select\template\ValuationMailTemplate.html";
		definition.IsBodyHtml = true;
		//definition.From = ValuationData.Rows[0]["CustEmail"].ToString();
		definition.From = "no-reply@mercedes-benz-select.com.tw";
		definition.Subject = "賓士精選中古車 車輛鑑價服務留言 客戶:" + ValuationData.Rows[0]["CustName"].ToString();
		ListDictionary replacements = new ListDictionary();

		replacements.Add("<%CustName%>", ValuationData.Rows[0]["CustName"].ToString());
		replacements.Add("<%CustAge%>", ValuationData.Rows[0]["CustBirthday"].ToString());
		replacements.Add("<%CustGender%>", ValuationData.Rows[0]["CustGender"].ToString());
		replacements.Add("<%CustPhone1%>", ValuationData.Rows[0]["CustPhone1"].ToString());
		replacements.Add("<%CustPhone2%>", ValuationData.Rows[0]["CustPhone2"].ToString());
		replacements.Add("<%CustAddr%>", ValuationData.Rows[0]["CustAddr"].ToString());
		replacements.Add("<%CustEmail%>", ValuationData.Rows[0]["CustEmail"].ToString());
		replacements.Add("<%CustContactTime%>", ValuationData.Rows[0]["CustContactTime"].ToString());
		replacements.Add("<%CustContactFunc%>", ValuationData.Rows[0]["CustContactFunc"].ToString());
		replacements.Add("<%CustMessage%>", ValuationData.Rows[0]["CustMessage"].ToString());
		replacements.Add("<%ValuCarType%>", ValuationData.Rows[0]["CarType"].ToString());
		replacements.Add("<%ValuCarFactoryDate%>", ValuationData.Rows[0]["FactoryDate"].ToString());
		replacements.Add("<%ValuCarPublishDate%>", ValuationData.Rows[0]["PublishDate"].ToString());
		replacements.Add("<%ValuCarMiles%>", ValuationData.Rows[0]["Miles"].ToString());
		replacements.Add("<%ValuCarColorOut%>", ValuationData.Rows[0]["ColorOut"].ToString());
		replacements.Add("<%ValuCarColorIn%>", ValuationData.Rows[0]["ColorIn"].ToString());
		replacements.Add("<%ValuCarSource%>", ValuationData.Rows[0]["CarSource"].ToString());
		replacements.Add("<%ValuCarPreSaleTime%>", ValuationData.Rows[0]["PreSaleTime"].ToString());
		replacements.Add("<%ValuCarAccident%>", ValuationData.Rows[0]["Accident"].ToString());
		replacements.Add("<%ValuCarEquipExtra%>", ValuationData.Rows[0]["EquipExtra"].ToString());
		
		try
		{
            message.Bcc.Add("ogilvyonemis1@gmail.com");
			message.To.Add("mb_select_request@daimler.com");
			AlternateView item = AlternateView.CreateAlternateViewFromString(definition.CreateMailMessage(builder.ToString(), replacements, new LiteralControl()).Body, null, "text/html");
			message.AlternateViews.Add(item);

			SmtpClient client = new SmtpClient("127.0.0.1", 25);

			client.Send(message);

			return true;
		}
		catch (Exception ex)
		{
			return false;
		}
	}

	public static bool SendContactUs(int dataid, int showroomid)
	{
		bool result = false;

		StringBuilder builder = new StringBuilder();
		DataTable EmailList = GetEmailList(showroomid);
		DataTable Data = GetContactUsData(dataid);

		MailMessage message = new MailMessage();
		//message.From = new MailAddress(Data.Rows[0]["CustEmail"].ToString());
		message.From = new MailAddress("no-reply@mercedes-benz-select.com.tw");
		message.Subject = "賓士精選中古車 尋車服務留言 客戶:" + Data.Rows[0]["CustName"].ToString();
		message.BodyEncoding = Encoding.UTF8;

		for (int i = 0; i < EmailList.Rows.Count; i++)
		{
			if (!string.IsNullOrEmpty(EmailList.Rows[i]["Email"].ToString()))
			{
				if (EmailList.Rows[i]["Email"].ToString().IndexOf(',') > 0)
				{
					string[] m = EmailList.Rows[i]["Email"].ToString().Split(',');
					foreach (var k in m)
					{
						if (!string.IsNullOrEmpty(k))
						{
							message.To.Add(k);
							builder.Append(k + ",");
						}
					}
				}
				else
				{
					message.To.Add(EmailList.Rows[i]["Email"].ToString().Trim());
					builder.Append(EmailList.Rows[i]["Email"].ToString().Trim() + ",");
				}

			}
		}

		builder.Remove(builder.Length - 1, 1);

		MailDefinition definition = new MailDefinition();
		definition.BodyFileName = @"C:\inetpub\wwwroot\Select\template\ContactUsMailTemplate.html";
		definition.IsBodyHtml = true;
		//definition.From = Data.Rows[0]["CustEmail"].ToString();
		definition.From = "no-reply@mercedes-benz-select.com.tw";
		definition.Subject = "賓士精選中古車 尋車服務留言 客戶:" + Data.Rows[0]["CustName"].ToString();
		ListDictionary replacements = new ListDictionary();

		replacements.Add("<%CustName%>", Data.Rows[0]["CustName"].ToString());
		replacements.Add("<%CustAge%>", Data.Rows[0]["CustBirthday"].ToString());
		replacements.Add("<%CustGender%>", Data.Rows[0]["CustGender"].ToString());
		replacements.Add("<%CustPhone1%>", Data.Rows[0]["CustPhone1"].ToString());
		replacements.Add("<%CustPhone2%>", Data.Rows[0]["CustPhone2"].ToString());
		replacements.Add("<%CustAddr%>", Data.Rows[0]["CustAddr"].ToString());
		replacements.Add("<%CustEmail%>", Data.Rows[0]["CustEmail"].ToString());
		replacements.Add("<%CustContactTime%>", Data.Rows[0]["CustContactTime"].ToString());
		replacements.Add("<%CustContactFunc%>", Data.Rows[0]["CustContactFunc"].ToString());
		replacements.Add("<%CustMessage%>", Data.Rows[0]["CustMessage"].ToString());
		replacements.Add("<%ContCarType%>", Data.Rows[0]["CarType"].ToString());
		replacements.Add("<%ContCarClass%>", Data.Rows[0]["ClassName"].ToString());
		replacements.Add("<%ContCarAgeStart%>", Data.Rows[0]["CarAgeStart"].ToString());
		replacements.Add("<%ContCarAgeEnd%>", Data.Rows[0]["CarAgeEnd"].ToString());
		replacements.Add("<%ContCarPriceStart%>", Data.Rows[0]["CarPriceStart"].ToString() == "600" ? "500萬" : Data.Rows[0]["CarPriceStart"].ToString());
		replacements.Add("<%ContCarPriceEnd%>", Data.Rows[0]["CarPriceEnd"].ToString() == "600" ? "500萬以上" : Data.Rows[0]["CarPriceEnd"].ToString());
		
		try
		{
            message.Bcc.Add("ogilvyonemis1@gmail.com");
            message.To.Add("mb_select_request@daimler.com");  // 20140708 ADD
			AlternateView item = AlternateView.CreateAlternateViewFromString(definition.CreateMailMessage(builder.ToString(), replacements, new LiteralControl()).Body, null, "text/html");
			message.AlternateViews.Add(item);

			SmtpClient client = new SmtpClient("127.0.0.1", 25);

			client.Send(message);

			return true;
		}
		catch (Exception ex)
		{
			return false;
		}
	}

	public static bool SendInterest(int dataid, int showroomid)
	{
		bool result = false;

		StringBuilder builder = new StringBuilder();
		DataTable EmailList = GetEmailList(showroomid);
		DataTable Data = GetInterestData(dataid);

		MailMessage message = new MailMessage();
		//message.From = new MailAddress(Data.Rows[0]["CustEmail"].ToString());
		message.From = new MailAddress("no-reply@mercedes-benz-select.com.tw");
		message.Subject = "賓士精選中古車 展售車輛詢問留言 客戶:" + Data.Rows[0]["CustName"].ToString();
		message.BodyEncoding = Encoding.UTF8;

		for (int i = 0; i < EmailList.Rows.Count; i++)
		{
			if (!string.IsNullOrEmpty(EmailList.Rows[i]["Email"].ToString()))
			{
				if (EmailList.Rows[i]["Email"].ToString().IndexOf(',') > 0)
				{
					string[] m = EmailList.Rows[i]["Email"].ToString().Split(',');
					foreach (var k in m)
					{
						if (!string.IsNullOrEmpty(k))
						{
							message.To.Add(k);
							builder.Append(k + ",");
						}
					}
				}
				else
				{
					message.To.Add(EmailList.Rows[i]["Email"].ToString().Trim());
					builder.Append(EmailList.Rows[i]["Email"].ToString().Trim() + ",");
				}
				
			}
		}

		builder.Remove(builder.Length - 1, 1);
		MailDefinition definition = new MailDefinition();
		definition.BodyFileName = @"C:\inetpub\wwwroot\Select\template\InterestMailTemplate.html";
		definition.IsBodyHtml = true;
		//definition.From = Data.Rows[0]["CustEmail"].ToString();
		definition.From = "no-reply@mercedes-benz-select.com.tw";
		definition.Subject = "賓士精選中古車 展售車輛詢問留言 客戶:" + Data.Rows[0]["CustName"].ToString();
		ListDictionary replacements = new ListDictionary();

		

		replacements.Add("<%CustName%>", Data.Rows[0]["CustName"].ToString());
		replacements.Add("<%CustAge%>", Data.Rows[0]["CustBirthday"].ToString());
		replacements.Add("<%CustGender%>", Data.Rows[0]["CustGender"].ToString());
		replacements.Add("<%CustPhone1%>", Data.Rows[0]["CustPhone1"].ToString());
		replacements.Add("<%CustPhone2%>", Data.Rows[0]["CustPhone2"].ToString());
		replacements.Add("<%CustAddr%>", Data.Rows[0]["CustAddr"].ToString());
		replacements.Add("<%CustEmail%>", Data.Rows[0]["CustEmail"].ToString());
		replacements.Add("<%CustContactTime%>", Data.Rows[0]["CustContactTime"].ToString());
		replacements.Add("<%CustContactFunc%>", Data.Rows[0]["CustContactFunc"].ToString());
		replacements.Add("<%CustMessage%>", Data.Rows[0]["CustMessage"].ToString());

		replacements.Add("<%ContCarClass%>", Data.Rows[0]["ClassName"].ToString());
		replacements.Add("<%ContCarType%>", Data.Rows[0]["CarTypeName"].ToString());
		replacements.Add("<%ContCarUrl%>", "http://" + Config.Domain + "car/" + Data.Rows[0]["CarID"].ToString());

		try
		{
            message.Bcc.Add("ogilvyonemis1@gmail.com");
            message.To.Add("mb_select_request@daimler.com");
			AlternateView item = AlternateView.CreateAlternateViewFromString(definition.CreateMailMessage(builder.ToString(), replacements, new LiteralControl()).Body, null, "text/html");
			message.AlternateViews.Add(item);

			SmtpClient client = new SmtpClient("127.0.0.1", 25);

			client.Send(message);

			return true;
		}
		catch (Exception ex)
		{
			return false;
		}
	}

	public static bool SendForgetPassword(string Email)
	{
		StringBuilder builder = new StringBuilder();
		MailMessage message = new MailMessage();

		DataTable Data = GetMemberData(Email);

		message.From = new MailAddress("no-reply@mercedes-benz-select.com.tw");
		message.Subject = "Benz Select Website 密碼通知信函";
		message.BodyEncoding = Encoding.UTF8;
		message.To.Add(Email);
		builder.Append(Email);

		MailDefinition definition = new MailDefinition();
		definition.BodyFileName = @"C:\inetpub\wwwroot\Select\template\ForgetPasswordMailTemplate.html";
		definition.IsBodyHtml = true;
		definition.From = "no-reply@mail.mercedes-benz-select.com.tw";
		definition.Subject = "Benz Select Website 密碼通知信函";
		ListDictionary replacements = new ListDictionary();

		replacements.Add("<%uName%>", Data.Rows[0]["Name"].ToString());
		replacements.Add("<%uid%>", Data.Rows[0]["LoginID"].ToString());
		replacements.Add("<%upw%>", ValidateHelper.DecryptString(Data.Rows[0]["LoginPWD"].ToString()));
		
		try
		{
            message.Bcc.Add("ogilvyonemis1@gmail.com");
            AlternateView item = AlternateView.CreateAlternateViewFromString(definition.CreateMailMessage(builder.ToString(), replacements, new LiteralControl()).Body, null, "text/html");
			message.AlternateViews.Add(item);

			SmtpClient client = new SmtpClient("127.0.0.1", 25);

			client.Send(message);

			return true;
		}
		catch (Exception ex)
		{
			throw (ex);
			return false;
		}
	}

    public static bool SendEquipImportIssue(string username,string content,string emails)
    {
        StringBuilder builder = new StringBuilder();
        MailMessage message = new MailMessage();

        string EmailList = "eellen.yu@daimler.com,liwen.chang@daimler.com,";
       // string EmailList = "leocc.lan@ogilvy.com,leocc.lan@ogilvy.com,";

        message.From = new MailAddress("no-reply@mercedes-benz-select.com.tw");
        message.Subject = "Benz Select Website "+ username.ToString() + " 車款配備EXCEL匯入 問題通知";
        message.BodyEncoding = Encoding.UTF8;
        if (!string.IsNullOrEmpty(EmailList.ToString()))
        {
            if (EmailList.ToString().IndexOf(',') > 0)
            {
                string[] m = EmailList.ToString().Split(',');
                foreach (var k in m)
                {
                    if (!string.IsNullOrEmpty(k))
                    {
                        message.To.Add(k);
                        builder.Append(k + ",");
                    }
                }
            }
            else
            {
                message.To.Add(EmailList.ToString().Trim());
                builder.Append(EmailList.ToString().Trim() + ",");
            }

        }
        builder.Remove(builder.Length - 1, 1);

        MailDefinition definition = new MailDefinition();
        definition.BodyFileName = @"C:\inetpub\wwwroot\Select\template\EquipImportIssueTemplate.html";
        definition.IsBodyHtml = true;
        definition.From = "no-reply@mail.mercedes-benz-select.com.tw";
        definition.Subject = "Benz Select Website " + username.ToString() + " 車款配備EXCEL匯入 問題通知";
        ListDictionary replacements = new ListDictionary();

        replacements.Add("<%uName%>", username.ToString());
        replacements.Add("<%equips%>", content.ToString());
        replacements.Add("<%emails%>", emails.ToString());

        try
        {
            message.Bcc.Add("ogilvyonemis1@gmail.com");
            AlternateView item = AlternateView.CreateAlternateViewFromString(definition.CreateMailMessage(builder.ToString(), replacements, new LiteralControl()).Body, null, "text/html");
            message.AlternateViews.Add(item);

            SmtpClient client = new SmtpClient("127.0.0.1", 25);

            client.Send(message);

            return true;
        }
        catch (Exception ex)
        {
            throw (ex);
            return false;
        }
    }

    private static DataTable GetEmailList(int showroomid)
	{
		DataTable result = new DataTable();
		Database db = DBHelper.GetDatabase();
		string sql = "SELECT Email FROM funoperator WHERE CHARINDEX(',' + CONVERT(VARCHAR, @id) + ',' , ',' + Showrooms + ',') > 0 AND RecieveEmail like (CASE WHEN Mtype = 'R' THEN '%%' ELSE 'Y' END)";
		DbCommand dbComm = db.GetSqlStringCommand(sql);
		db.AddInParameter(dbComm, "id", DbType.Int32, showroomid);
		DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
		if (dt.Rows.Count > 0)
		{
			return dt;
		}
		else
		{
			return result;
		}
	}

	private static DataTable GetValuationData(int id)
	{
		DataTable result = new DataTable();
		Database db = DBHelper.GetDatabase();
		string sql = "SELECT * FROM Valuation WHERE ValuationID = @id";
		DbCommand dbComm = db.GetSqlStringCommand(sql);
		db.AddInParameter(dbComm, "id", DbType.Int32, id);
		DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
		if (dt.Rows.Count > 0)
		{
			return dt;
		}
		else
		{
			return result;
		}
	}

	private static DataTable GetContactUsData(int id)
	{
		DataTable result = new DataTable();
		Database db = DBHelper.GetDatabase();
		string sql = "SELECT * FROM ContactUs WHERE ContactUsID = @id";
		DbCommand dbComm = db.GetSqlStringCommand(sql);
		db.AddInParameter(dbComm, "id", DbType.Int32, id);
		DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
		if (dt.Rows.Count > 0)
		{
			return dt;
		}
		else
		{
			return result;
		}
	}

	private static DataTable GetInterestData(int id)
	{
		DataTable result = new DataTable();
		Database db = DBHelper.GetDatabase();
		string sql = "SELECT a.*, b.ClassID, b.CarTypeID, c.ClassName, d.CarTypeName FROM Interest a LEFT JOIN Car b ON a.CarID = b.CarID LEFT JOIN CarClass c ON b.ClassID = c.ClassID LEFT JOIN CarType d ON b.CarTypeID = d.CarTypeID WHERE a.InterestID = @id";
		DbCommand dbComm = db.GetSqlStringCommand(sql);
		db.AddInParameter(dbComm, "id", DbType.Int32, id);
		DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
		if (dt.Rows.Count > 0)
		{
			return dt;
		}
		else
		{
			return result;
		}
	}

	private static DataTable GetMemberData(string email)
	{
		DataTable result = new DataTable();
		Database db = DBHelper.GetDatabase();
		string sql = "SELECT Name, LoginID, LoginPWD FROM Member WHERE LoginID = @email";
		DbCommand dbComm = db.GetSqlStringCommand(sql);
		db.AddInParameter(dbComm, "email", DbType.String, email);
		DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
		if (dt.Rows.Count > 0)
		{
			return dt;
		}
		else
		{
			return result;
		}
	}
}