﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Web;
using System.ComponentModel;

public class Backend_DBCar
{
    #region backend/Car/list
    /// <summary>
    /// 取得列表
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <param name="startrows"></param>
    /// <param name="maxrows"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetList(string CarTypeId, string StockStatus,string Status,string ShowHomePage, int startrows, int maxrows)
    {

        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = "SELECT  ROW_NUMBER() OVER (ORDER BY c.CarId desc) AS RowNum, ct.[CarTypeName] ,c.CarId,c.CarName,c.CarImg,c.StockStatus,c.Status,c.ShowHomePage, c.COrder  FROM [Car] c left join [CarType] ct on c.[CarTypeID] = ct.[CarTypeID] ";


        if (!string.IsNullOrEmpty(CarTypeId))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.[CarTypeId] = @CarTypeId";
        }

        if (!string.IsNullOrEmpty(StockStatus))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.StockStatus=@StockStatus";
        }
        if (!string.IsNullOrEmpty(Status))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.Status=@Status";
        }
        if (!string.IsNullOrEmpty(ShowHomePage))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.ShowHomePage=@ShowHomePage";
        }


        conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.Valid='1' ";

        sql += String.Format(@"SELECT *
                 FROM ({0})
                 AS NewTable
                 WHERE RowNum >= {1} AND RowNum <= {2}", conditionSQL, (startrows + 1).ToString(), (maxrows + startrows).ToString());

        sql += " order by ShowHomePage desc,[CarTypeName] desc";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        if (!string.IsNullOrEmpty(CarTypeId)) db.AddInParameter(dbComm, "CarTypeId", DbType.String, Convert.ToString(CarTypeId));
        if (!string.IsNullOrEmpty(StockStatus)) db.AddInParameter(dbComm, "StockStatus", DbType.String, Convert.ToString(StockStatus));
        if (!string.IsNullOrEmpty(Status)) db.AddInParameter(dbComm, "Status", DbType.String, Convert.ToString(Status));
        if (!string.IsNullOrEmpty(ShowHomePage)) db.AddInParameter(dbComm, "ShowHomePage", DbType.String, Convert.ToString(ShowHomePage));
        

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetFinanceList(string CarTypeId, string StockStatus, string ShowHomePage, int startrows, int maxrows)
    {

        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        //string conditionSQL = "SELECT  ROW_NUMBER() OVER (ORDER BY c.CarId desc) AS RowNum, ct.[CarTypeName] ,c.CarId,c.CarName,c.CarImg,c.StockStatus,c.Status,c.ShowHomePage, c.COrder  FROM [Car] c left join [CarType] ct on c.[CarTypeID] = ct.[CarTypeID] ";
        string conditionSQL = @"SELECT  ROW_NUMBER() OVER (ORDER BY c.CarId desc) AS RowNum,
                                ct.[CarTypeName] ,c.CarId,c.CarName,c.CarImg,c.StockStatus,c.Status,c.ShowHomePage, c.COrder 
                                  ,[CarFinance].[MSRP]
                                  ,[CarFinance].[Downpayment]
                                  ,[CarFinance].[InterestRate]
                                  ,[CarFinance].[PromoteText]
                                  ,[CarFinance].[PromoteStart]
                                  ,[CarFinance].[PromoteEnd]
                                  ,[CarFinance].[PromoteInterestRate]
                                  ,[CarFinance].[FinanceStatus]
                                  ,[CarFinance].[CDate]
                                  ,[CarFinance].[MDate] 
                                 FROM [Car] c left join [CarType] ct on c.[CarTypeID] = ct.[CarTypeID]
                                 LEFT JOIN [CarFinance] on [CarFinance].CarID=c.CarID
                                 where StockStatus = 'S'";

        if (!string.IsNullOrEmpty(CarTypeId))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.[CarTypeId] = @CarTypeId";
        }

        if (!string.IsNullOrEmpty(StockStatus))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.StockStatus=@StockStatus";
        }
        
        if (!string.IsNullOrEmpty(ShowHomePage))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.ShowHomePage=@ShowHomePage";
        }


        conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.Valid='1' ";

        sql += String.Format(@"SELECT *
                 FROM ({0})
                 AS NewTable
                 WHERE RowNum >= {1} AND RowNum <= {2}", conditionSQL, (startrows + 1).ToString(), (maxrows + startrows).ToString());

        sql += " order by ShowHomePage desc,[CarTypeName] desc";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        if (!string.IsNullOrEmpty(CarTypeId)) db.AddInParameter(dbComm, "CarTypeId", DbType.String, Convert.ToString(CarTypeId));
        if (!string.IsNullOrEmpty(StockStatus)) db.AddInParameter(dbComm, "StockStatus", DbType.String, Convert.ToString(StockStatus));
        if (!string.IsNullOrEmpty(ShowHomePage)) db.AddInParameter(dbComm, "ShowHomePage", DbType.String, Convert.ToString(ShowHomePage));


        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// 取得Select總筆數
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetFinanceCounts(string CarTypeId, string StockStatus, string ShowHomePage)
    {
        Database db = DBHelper.GetDatabase();

        string conditionSQL = "  SELECT  Count(1) FROM [Car] c where c.Valid='1' and StockStatus = 'S' ";


        if (!string.IsNullOrEmpty(CarTypeId))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.[CarTypeId] = @CarTypeId";
        }

        if (!string.IsNullOrEmpty(StockStatus))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.StockStatus=@StockStatus";
        }
        if (!string.IsNullOrEmpty(ShowHomePage))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.ShowHomePage=@ShowHomePage";
        }


        conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.Valid='1' ";

        DbCommand dbComm = db.GetSqlStringCommand(conditionSQL);


        if (!string.IsNullOrEmpty(CarTypeId)) db.AddInParameter(dbComm, "CarTypeId", DbType.String, Convert.ToString(CarTypeId));
        if (!string.IsNullOrEmpty(StockStatus)) db.AddInParameter(dbComm, "StockStatus", DbType.String, Convert.ToString(StockStatus));
        if (!string.IsNullOrEmpty(ShowHomePage)) db.AddInParameter(dbComm, "ShowHomePage", DbType.String, Convert.ToString(ShowHomePage));

        DataTable dt = new DataTable();

        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return Convert.ToInt32(dt.Rows[0][0]);
    }

    /// <summary>
    /// 取得Select總筆數
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCounts(string CarTypeId, string StockStatus,string ShowHomePage, string Status)
    {
        Database db = DBHelper.GetDatabase();

        string conditionSQL = "  SELECT  Count(1) FROM [Car] c where c.Valid='1' ";


        if (!string.IsNullOrEmpty(CarTypeId))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.[CarTypeId] = @CarTypeId";
        }

        if (!string.IsNullOrEmpty(StockStatus))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.StockStatus=@StockStatus";
        }
        if (!string.IsNullOrEmpty(Status))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.Status=@Status";
        }
        if (!string.IsNullOrEmpty(ShowHomePage))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.ShowHomePage=@ShowHomePage";
        }


        conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " c.Valid='1' ";

        DbCommand dbComm = db.GetSqlStringCommand(conditionSQL);


        if (!string.IsNullOrEmpty(CarTypeId)) db.AddInParameter(dbComm, "CarTypeId", DbType.String, Convert.ToString(CarTypeId));
        if (!string.IsNullOrEmpty(StockStatus)) db.AddInParameter(dbComm, "StockStatus", DbType.String, Convert.ToString(StockStatus));
        if (!string.IsNullOrEmpty(Status)) db.AddInParameter(dbComm, "Status", DbType.String, Convert.ToString(Status));
        if (!string.IsNullOrEmpty(ShowHomePage)) db.AddInParameter(dbComm, "ShowHomePage", DbType.String, Convert.ToString(ShowHomePage));

        DataTable dt = new DataTable();

        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return Convert.ToInt32(dt.Rows[0][0]);
    }

    /// <summary>
    /// operatorlist.aspx 更新有效狀態
    /// </summary>
    /// <param name="serno"></param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void UpdateValid(int CarId)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "update [Car] set [Valid]=@Valid,[mdate]=@mdate,[oid]=@oid where CarId=@CarId";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarId", DbType.Int32, CarId);
        db.AddInParameter(dbComm, "Valid", DbType.String, 0);
        db.AddInParameter(dbComm, "mdate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }


    /// <summary>
    /// 取得車型列表
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <param name="startrows"></param>
    /// <param name="maxrows"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetCarTypeList()
    {
        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = "SELECT  * FROM [CarType] where valid='1'";

        

        sql += conditionSQL;

        sql += " ORDER BY COrder ";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
       


        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// 資料狀態
    /// </summary>
    /// <param name="ContactUsID"></param>
    ///  <param name="value"></param>
    ///   <param name="field"></param>
    public static void UpdateStatusBtn(string CarId, string value, string field)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "update [Car] set [" + field + "]=@value,[Status]=@Status,[mdate]=@mdate,[oid]=@oid where CarId=@CarId";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarId", DbType.Int32, Convert.ToInt32(CarId));
        db.AddInParameter(dbComm, "value", DbType.String, value);
        db.AddInParameter(dbComm, "Status", DbType.String, "T");
        db.AddInParameter(dbComm, "mdate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 資料狀態
    /// </summary>
    /// <param name="ContactUsID"></param>
    ///  <param name="value"></param>
    ///   <param name="field"></param>
    public static void UpdateStatus(string CarId, string value, string field)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "update [Car] set [" + field + "]=@value,[mdate]=@mdate,[oid]=@oid where CarId=@CarId";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarId", DbType.Int32, Convert.ToInt32(CarId));
        db.AddInParameter(dbComm, "value", DbType.String, value);
        db.AddInParameter(dbComm, "mdate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }



    #endregion

    #region backend/Car/edit
    /// <summary>
    /// operatoredit.aspx 取得詳細資訊
    /// </summary>
    /// <param name="serno"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetListBySerno(string CarId)
    {
        Database db = DBHelper.GetDatabase();

        string sql = "select * , (SELECT dbo.fn_Get_OperatorName(" + BackendBasePage.GetUserSerno() + ")) as Username from [car]";

        if (!string.IsNullOrEmpty(CarId))
        {
            sql += DBHelper.GetSqlWhereOrAnd(sql) + " [CarId]=@CarId";
        }


        DbCommand dbComm = db.GetSqlStringCommand(sql);
        if (!string.IsNullOrEmpty(CarId))
        {
            db.AddInParameter(dbComm, "CarId", DbType.Int32, Convert.ToInt32(CarId));
        }

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }


    /// <summary>
    /// 新增
    /// </summary>
    /// <param name="name"></param>
    /// CarName, CarSize, Wheelbase, Track, Luggage, Weight, Type, CC, Hp, Kgm, Compression, Stroke, Acceleration, ExtremeSpeed, Oil, FuelTank, Transfer, Transmission, Steering, Brake, Suspension, MinSlewing, AnnualFuel, NonUrbanFuel, UrbanFuel, TestValue, EfficiencyRating, CarTypeID, StockStatus, CarImg
    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static string Insert(string CarTypeID, string ShowHomePage, string CarName, string StockStatus, string CarSize, string Wheelbase, string Track,
        string Luggage, string Weight, string Type, string CC, string Hp, string Kgm, string Compression, string Stroke, string Acceleration,
        string ExtremeSpeed, string Oil, string FuelTank, string Transfer, string Transmission, string Steering, string Brake, string Suspension,string Tire,
        string MinSlewing, string AnnualFuel, string NonUrbanFuel, string UrbanFuel, string TestValue, string EfficiencyRating,string CarProtectText, string CarImg,string COrder)
    {
        //處理預設值不能為Null的問題       
        CarImg = (string.IsNullOrEmpty(CarImg)) ? "" : CarImg;
        string returnId = "";

        Database db = DBHelper.GetDatabase();
        string sql = "insert into [Car] ( [CarTypeID],[ShowHomePage],[CarName], [StockStatus],[Status], [CarSize], [Wheelbase], [Track], [Luggage], [Weight], [Type], [CC], [Hp], [Kgm], [Compression], [Stroke], [Acceleration], [ExtremeSpeed], [Oil], [FuelTank], [Transfer], [Transmission], [Steering], [Brake], [Suspension],[Tire], [MinSlewing], [AnnualFuel], [NonUrbanFuel], [UrbanFuel], [TestValue], [EfficiencyRating],[CarProtectText], [CarImg],[COrder],[CDate],[MDate],[Oid])"
                    + " values ( @CarTypeID,@ShowHomePage,@CarName, @StockStatus,@Status, @CarSize, @Wheelbase, @Track, @Luggage, @Weight, @Type, @CC, @Hp, @Kgm, @Compression, @Stroke, @Acceleration, @ExtremeSpeed, @Oil, @FuelTank, @Transfer, @Transmission, @Steering, @Brake, @Suspension,@Tire, @MinSlewing, @AnnualFuel, @NonUrbanFuel, @UrbanFuel, @TestValue, @EfficiencyRating,@CarProtectText, @CarImg,@COrder,@CDate,@MDate,@Oid) "
                    + " SET @id= IDENT_CURRENT ('Car')";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        db.AddInParameter(dbComm, "CarTypeID", DbType.String, CarTypeID);
        db.AddInParameter(dbComm, "ShowHomePage", DbType.String, ShowHomePage);
        db.AddInParameter(dbComm, "CarName", DbType.String, CarName);
        db.AddInParameter(dbComm, "CarImg", DbType.String, CarImg);

        db.AddInParameter(dbComm, "StockStatus", DbType.String, StockStatus);
        db.AddInParameter(dbComm, "Status", DbType.String, "T");
        db.AddInParameter(dbComm, "CarSize", DbType.String, CarSize);
        db.AddInParameter(dbComm, "Wheelbase", DbType.String, Wheelbase);
        db.AddInParameter(dbComm, "Track", DbType.String, Track);

        db.AddInParameter(dbComm, "Luggage", DbType.String, Luggage);
        db.AddInParameter(dbComm, "Weight", DbType.String, Weight);
        db.AddInParameter(dbComm, "Type", DbType.String, Type);
        db.AddInParameter(dbComm, "CC", DbType.String, CC);
        db.AddInParameter(dbComm, "Hp", DbType.String, Hp);
        db.AddInParameter(dbComm, "Kgm", DbType.String, Kgm);
        db.AddInParameter(dbComm, "Compression", DbType.String, Compression);
        db.AddInParameter(dbComm, "Stroke", DbType.String, Stroke);
        db.AddInParameter(dbComm, "Acceleration", DbType.String, Acceleration);

        db.AddInParameter(dbComm, "ExtremeSpeed", DbType.String, ExtremeSpeed);
        db.AddInParameter(dbComm, "Oil", DbType.String, Oil);
        db.AddInParameter(dbComm, "FuelTank", DbType.String, FuelTank);
        db.AddInParameter(dbComm, "Transfer", DbType.String, Transfer);
        db.AddInParameter(dbComm, "Transmission", DbType.String, Transmission);
        db.AddInParameter(dbComm, "Steering", DbType.String, Steering);
        db.AddInParameter(dbComm, "Brake", DbType.String, Brake);
        db.AddInParameter(dbComm, "Suspension", DbType.String, Suspension);
        db.AddInParameter(dbComm, "Tire", DbType.String, Tire);       

        db.AddInParameter(dbComm, "MinSlewing", DbType.String, MinSlewing);
        db.AddInParameter(dbComm, "AnnualFuel", DbType.String, AnnualFuel);
        db.AddInParameter(dbComm, "NonUrbanFuel", DbType.String, NonUrbanFuel);
        db.AddInParameter(dbComm, "UrbanFuel", DbType.String, UrbanFuel);
        db.AddInParameter(dbComm, "TestValue", DbType.String, TestValue);
        db.AddInParameter(dbComm, "EfficiencyRating", DbType.String, EfficiencyRating);
        db.AddInParameter(dbComm, "CarProtectText", DbType.String, CarProtectText);        

        db.AddInParameter(dbComm, "COrder", DbType.Int32, Int32.Parse(COrder));
        db.AddInParameter(dbComm, "CDate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "MDate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "Oid", DbType.Int32, BackendBasePage.GetUserSerno());

        db.AddOutParameter(dbComm, "id", DbType.Int32, 8);
        try
        {
            db.ExecuteNonQuery(dbComm);
            returnId = db.GetParameterValue(dbComm, "id").ToString();

        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return returnId;
    }



    /// <summary>
    /// 修改
    /// </summary>    
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void Update(int CarID, string CarTypeID, string ShowHomePage, string CarName, string StockStatus, string CarSize, string Wheelbase, string Track,
        string Luggage, string Weight, string Type, string CC, string Hp, string Kgm, string Compression, string Stroke, string Acceleration,
        string ExtremeSpeed, string Oil, string FuelTank, string Transfer, string Transmission, string Steering, string Brake, string Suspension,string Tire,
        string MinSlewing, string AnnualFuel, string NonUrbanFuel, string UrbanFuel, string TestValue, string EfficiencyRating,string CarProtectText, string CarImg,string COrder)
    {

        CarImg = (string.IsNullOrEmpty(CarImg)) ? "" : CarImg;

        Database db = DBHelper.GetDatabase();
        string sql = "update [car] set [CarName]=@CarName,[CarTypeID]=@CarTypeID,[ShowHomePage]=@ShowHomePage, [CarImg]=@CarImg,[StockStatus]=@StockStatus,[Status]=@Status,[CarSize]=@CarSize,[Wheelbase]=@Wheelbase,[Track]=@Track," +
            "[Luggage]=@Luggage,[Weight]=@Weight,[Type]=@Type,[CC]=@CC,[Hp]=@Hp,[Kgm]=@Kgm,[Compression]=@Compression,[Stroke]=@Stroke,[Acceleration]=@Acceleration," +
            "[ExtremeSpeed]=@ExtremeSpeed,[Oil]=@Oil,[FuelTank]=@FuelTank,[Transfer]=@Transfer , [Transmission]=@Transmission, [Steering]=@Steering, [Brake]=@Brake,[Suspension]=@Suspension," +
            "[Tire]=@Tire,[MinSlewing]=@MinSlewing,[AnnualFuel]=@AnnualFuel,[NonUrbanFuel]=@NonUrbanFuel,[UrbanFuel]=@UrbanFuel , [TestValue]=@TestValue, [EfficiencyRating]=@EfficiencyRating, [CarProtectText]=@CarProtectText," +
            "[COrder]=@COrder,[MDate]=@MDate, [Oid]=@Oid where CarID=@CarID";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarID", DbType.Int32, CarID);
        db.AddInParameter(dbComm, "CarTypeID", DbType.String, CarTypeID);
        db.AddInParameter(dbComm, "ShowHomePage", DbType.String, ShowHomePage);
        db.AddInParameter(dbComm, "CarName", DbType.String, CarName);
        db.AddInParameter(dbComm, "CarImg", DbType.String, CarImg);


        db.AddInParameter(dbComm, "StockStatus", DbType.String, StockStatus);
        db.AddInParameter(dbComm, "Status", DbType.String, "T");
        db.AddInParameter(dbComm, "CarSize", DbType.String, CarSize);
        db.AddInParameter(dbComm, "Wheelbase", DbType.String, Wheelbase);
        db.AddInParameter(dbComm, "Track", DbType.String, Track);

        db.AddInParameter(dbComm, "Luggage", DbType.String, Luggage);
        db.AddInParameter(dbComm, "Weight", DbType.String, Weight);
        db.AddInParameter(dbComm, "Type", DbType.String, Type);
        db.AddInParameter(dbComm, "CC", DbType.String, CC);
        db.AddInParameter(dbComm, "Hp", DbType.String, Hp);
        db.AddInParameter(dbComm, "Kgm", DbType.String, Kgm);
        db.AddInParameter(dbComm, "Compression", DbType.String, Compression);
        db.AddInParameter(dbComm, "Stroke", DbType.String, Stroke);
        db.AddInParameter(dbComm, "Acceleration", DbType.String, Acceleration);

        db.AddInParameter(dbComm, "ExtremeSpeed", DbType.String, ExtremeSpeed);
        db.AddInParameter(dbComm, "Oil", DbType.String, Oil);
        db.AddInParameter(dbComm, "FuelTank", DbType.String, FuelTank);
        db.AddInParameter(dbComm, "Transfer", DbType.String, Transfer);
        db.AddInParameter(dbComm, "Transmission", DbType.String, Transmission);
        db.AddInParameter(dbComm, "Steering", DbType.String, Steering);
        db.AddInParameter(dbComm, "Brake", DbType.String, Brake);
        db.AddInParameter(dbComm, "Suspension", DbType.String, Suspension);
        db.AddInParameter(dbComm, "Tire", DbType.String, Tire);

        db.AddInParameter(dbComm, "MinSlewing", DbType.String, MinSlewing);
        db.AddInParameter(dbComm, "AnnualFuel", DbType.String, AnnualFuel);
        db.AddInParameter(dbComm, "NonUrbanFuel", DbType.String, NonUrbanFuel);
        db.AddInParameter(dbComm, "UrbanFuel", DbType.String, UrbanFuel);
        db.AddInParameter(dbComm, "TestValue", DbType.String, TestValue);
        db.AddInParameter(dbComm, "EfficiencyRating", DbType.String, EfficiencyRating);
        db.AddInParameter(dbComm, "CarProtectText", DbType.String, CarProtectText);        


        db.AddInParameter(dbComm, "COrder", DbType.Int32, Int32.Parse(COrder));
        db.AddInParameter(dbComm, "MDate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "Oid", DbType.Int32, BackendBasePage.GetUserSerno());

        try
        {
            db.ExecuteNonQuery(dbComm);

        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 新增上傳圖片
    /// </summary>
    public static void InsertUploadImg(string CarID, string field, string imgUrl)
    {
        Database db = DBHelper.GetDatabase();


        string sql = "update [car] set [" + field + "]=@imgUrl where CarID=@CarID";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "imgUrl", DbType.String, imgUrl);
        db.AddInParameter(dbComm, "CarID", DbType.String, CarID);
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }




    public static bool uploadToOnline(string carid)
    {
        bool result = false;
        try
        {
            //car
            uploadToCarOnline(carid);
            //carequip
            getCarEquipList(carid);

            //產生首頁用json
            Public.GenIndexCarTypeJson();
            Public.GenIndexCarImageJson();

            result = true;
        }
        catch (Exception ex) {
            throw (ex);           
        }

        return result;

    }

    public static void uploadToCarOnline(string carID)
    {
        DataTable dt = GetListBySerno(carID);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                string CarID = dr["CarID"].ToString();
                string CarTypeID = dr["CarTypeID"].ToString();
                string ShowHomePage = dr["ShowHomePage"].ToString();
                string CarName = dr["CarName"].ToString();
                string CarImg = dr["CarImg"].ToString();
                string CarSize = dr["CarSize"].ToString();
                string Wheelbase = dr["Wheelbase"].ToString();
                string Track = dr["Track"].ToString();
                string Luggage = dr["Luggage"].ToString();
                string Weight = dr["Weight"].ToString();
                string Type = dr["Type"].ToString();
                string CC = dr["CC"].ToString();
                string Hp = dr["Hp"].ToString();
                string Kgm = dr["Kgm"].ToString();
                string Compression = dr["Compression"].ToString();
                string Stroke = dr["Stroke"].ToString();
                string Acceleration = dr["Acceleration"].ToString();
                string ExtremeSpeed = dr["ExtremeSpeed"].ToString();
                string Oil = dr["Oil"].ToString();
                string FuelTank = dr["FuelTank"].ToString();
                string Transfer = dr["Transfer"].ToString();
                string Transmission = dr["Transmission"].ToString();
                string Steering = dr["Steering"].ToString();
                string Brake = dr["Brake"].ToString();
                string Suspension = dr["Suspension"].ToString();
                string Tire = dr["Tire"].ToString();
                string MinSlewing = dr["MinSlewing"].ToString();
                string AnnualFuel = dr["AnnualFuel"].ToString();
                string NonUrbanFuel = dr["NonUrbanFuel"].ToString();
                string UrbanFuel = dr["UrbanFuel"].ToString();
                string TestValue = dr["TestValue"].ToString();
                string EfficiencyRating = dr["EfficiencyRating"].ToString();
                string CarProtectText = dr["CarProtectText"].ToString();
                string StockStatus = dr["StockStatus"].ToString();
                string Status=dr["Status"].ToString();
                string Valid = dr["Valid"].ToString();
                string COrder = dr["COrder"].ToString();
                string CDate = dr["CDate"].ToString();
                string MDate = dr["MDate"].ToString();
                string Oid = dr["Oid"].ToString();


                Database db = DBHelper.GetDatabase();
                string sql =
               " begin tran " +
                " if exists (select * from Car_online  where CarID = @CarID ) " +
                " begin " +
                " update [car_online] set [CarName]=@CarName,[CarTypeID]=@CarTypeID,[ShowHomePage]=@ShowHomePage, [CarImg]=@CarImg,[StockStatus]=@StockStatus,[Status]=@Status,[Valid]=@Valid,[CarSize]=@CarSize,[Wheelbase]=@Wheelbase,[Track]=@Track," +
                "[Luggage]=@Luggage,[Weight]=@Weight,[Type]=@Type,[CC]=@CC,[Hp]=@Hp,[Kgm]=@Kgm,[Compression]=@Compression,[Stroke]=@Stroke,[Acceleration]=@Acceleration," +
                "[ExtremeSpeed]=@ExtremeSpeed,[Oil]=@Oil,[FuelTank]=@FuelTank,[Transfer]=@Transfer , [Transmission]=@Transmission, [Steering]=@Steering, [Brake]=@Brake,[Suspension]=@Suspension,[Tire]=@Tire," +
                "[MinSlewing]=@MinSlewing,[AnnualFuel]=@AnnualFuel,[NonUrbanFuel]=@NonUrbanFuel,[UrbanFuel]=@UrbanFuel , [TestValue]=@TestValue, [EfficiencyRating]=@EfficiencyRating,[CarProtectText]=@CarProtectText," +
                "[COrder]=@COrder,[MDate]=@MDate, [Oid]=@Oid where [CarID]=@CarID " +
                " end " +
                " else " +
                " begin " +
                 "insert into [car_online] ( [CarID],[CarTypeID],[ShowHomePage],[CarName], [StockStatus],[Status],[Valid], [CarSize], [Wheelbase], [Track], [Luggage], [Weight], [Type], [CC], [Hp], [Kgm], [Compression], [Stroke], [Acceleration], [ExtremeSpeed], [Oil], [FuelTank], [Transfer], [Transmission], [Steering], [Brake], [Suspension],[Tire], [MinSlewing], [AnnualFuel], [NonUrbanFuel], [UrbanFuel], [TestValue], [EfficiencyRating],[CarProtectText], [CarImg],[COrder],[CDate],[MDate],[Oid])"
                    + " values ( @CarID,@CarTypeID,@ShowHomePage,@CarName, @StockStatus,@Status,@Valid, @CarSize, @Wheelbase, @Track, @Luggage, @Weight, @Type, @CC, @Hp, @Kgm, @Compression, @Stroke, @Acceleration, @ExtremeSpeed, @Oil, @FuelTank, @Transfer, @Transmission, @Steering, @Brake, @Suspension,@Tire, @MinSlewing, @AnnualFuel, @NonUrbanFuel, @UrbanFuel, @TestValue, @EfficiencyRating,@CarProtectText, @CarImg,@COrder,@CDate,@MDate,@Oid) " +
                " end " +
                " commit tran ";
              

                DbCommand dbComm = db.GetSqlStringCommand(sql);
                db.AddInParameter(dbComm, "CarID", DbType.Int32, CarID);
                db.AddInParameter(dbComm, "CarTypeID", DbType.String, CarTypeID);
                db.AddInParameter(dbComm, "ShowHomePage", DbType.String, ShowHomePage);
                db.AddInParameter(dbComm, "CarName", DbType.String, CarName);
                db.AddInParameter(dbComm, "CarImg", DbType.String, CarImg);


                db.AddInParameter(dbComm, "StockStatus", DbType.String, StockStatus);
                db.AddInParameter(dbComm, "Status", DbType.String, "O");
                db.AddInParameter(dbComm, "Valid", DbType.String, Valid);                
                db.AddInParameter(dbComm, "CarSize", DbType.String, CarSize);
                db.AddInParameter(dbComm, "Wheelbase", DbType.String, Wheelbase);
                db.AddInParameter(dbComm, "Track", DbType.String, Track);

                db.AddInParameter(dbComm, "Luggage", DbType.String, Luggage);
                db.AddInParameter(dbComm, "Weight", DbType.String, Weight);
                db.AddInParameter(dbComm, "Type", DbType.String, Type);
                db.AddInParameter(dbComm, "CC", DbType.String, CC);
                db.AddInParameter(dbComm, "Hp", DbType.String, Hp);
                db.AddInParameter(dbComm, "Kgm", DbType.String, Kgm);
                db.AddInParameter(dbComm, "Compression", DbType.String, Compression);
                db.AddInParameter(dbComm, "Stroke", DbType.String, Stroke);
                db.AddInParameter(dbComm, "Acceleration", DbType.String, Acceleration);

                db.AddInParameter(dbComm, "ExtremeSpeed", DbType.String, ExtremeSpeed);
                db.AddInParameter(dbComm, "Oil", DbType.String, Oil);
                db.AddInParameter(dbComm, "FuelTank", DbType.String, FuelTank);
                db.AddInParameter(dbComm, "Transfer", DbType.String, Transfer);
                db.AddInParameter(dbComm, "Transmission", DbType.String, Transmission);
                db.AddInParameter(dbComm, "Steering", DbType.String, Steering);
                db.AddInParameter(dbComm, "Brake", DbType.String, Brake);
                db.AddInParameter(dbComm, "Suspension", DbType.String, Suspension);
                db.AddInParameter(dbComm, "Tire", DbType.String, Tire); 
                db.AddInParameter(dbComm, "MinSlewing", DbType.String, MinSlewing);
                db.AddInParameter(dbComm, "AnnualFuel", DbType.String, AnnualFuel);
                db.AddInParameter(dbComm, "NonUrbanFuel", DbType.String, NonUrbanFuel);
                db.AddInParameter(dbComm, "UrbanFuel", DbType.String, UrbanFuel);
                db.AddInParameter(dbComm, "TestValue", DbType.String, TestValue);
                db.AddInParameter(dbComm, "EfficiencyRating", DbType.String, EfficiencyRating);
                db.AddInParameter(dbComm, "CarProtectText", DbType.String, CarProtectText); 

                db.AddInParameter(dbComm, "COrder", DbType.Int32, Int32.Parse(COrder));
                db.AddInParameter(dbComm, "CDate", DbType.DateTime, DateTime.Now);
                db.AddInParameter(dbComm, "MDate", DbType.DateTime, DateTime.Now);
                db.AddInParameter(dbComm, "Oid", DbType.Int32, BackendBasePage.GetUserSerno());


                try
                {
                    db.ExecuteNonQuery(dbComm);
                    //更新狀態
                    UpdateStatus(CarID,"O","Status");
                }
                catch (Exception ex)
                {
                    throw (ex);
                }



            }

        }
    }

    //carequip
    public static void getCarEquipList(string CarID)
    {

        Database db = DBHelper.GetDatabase();
        string sql =
        @" Delete from [CarEquip_online] where [CarId]=@DeleteCarId ;
            SELECT [Serno],[CarID],[EquipID],[EquipType],[EquipExtraDetail]  FROM [CarEquip] where CarID=@CarID ;";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "DeleteCarId", DbType.String, CarID);
        db.AddInParameter(dbComm, "CarID", DbType.Int32, Convert.ToInt32(CarID));

        try
        {
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count > 0)
            {
                uploadToCarEquipOnline(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    string carID = dr["CarID"].ToString();
                    string equipID = dr["EquipID"].ToString();
                    string equipType = dr["EquipType"].ToString();
                    string equipExtraDetail = dr["EquipExtraDetail"].ToString();
                   // uploadToCarEquipOnline(carID, equipID, equipType, equipExtraDetail);
                   
                }
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }
    public static void uploadToCarEquipOnline(DataTable dt)
    {
        string sql = "";
        if (dt.Rows.Count > 0)
        {
            Database db = DBHelper.GetDatabase();
            foreach (DataRow dr in dt.Rows)
            {
                string Serno = dr["Serno"].ToString();
                string CarID = dr["CarID"].ToString();
                string EquipID = dr["EquipID"].ToString();
                string EquipType = dr["EquipType"].ToString();
                string EquipExtraDetail = dr["EquipExtraDetail"].ToString();
                sql += "insert into CarEquip_online ([Serno],[CarID],[EquipID],[EquipType],[equipExtraDetail])  values ('"+ Serno + "','" + CarID + "','" + EquipID + "','" + EquipType + "','" + EquipExtraDetail + "') ;";
            }

            DbCommand dbComm = db.GetSqlStringCommand(sql);
            try
            {
                db.ExecuteNonQuery(dbComm);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
    public static void uploadToCarEquipOnline(string CarID, string EquipID, string EquipType, string equipExtraDetail)
    {
        Database db = DBHelper.GetDatabase();
        string sql =
       " begin tran " +
        " if exists (select * from CarEquip_online  where CarID = @CarID and EquipID = @EquipID) " +
        " begin " +
         " update CarEquip_online set EquipType = @EquipType,[equipExtraDetail]=@EquipExtraDetail " +
         " where CarID = @CarID and EquipID = @EquipID " +
        " end " +
        " else " +
        " begin " +
        " insert into CarEquip_online ([CarID],[EquipID],[EquipType],[equipExtraDetail]) " +
         " values (@CarID,@EquipID,@EquipType,@EquipExtraDetail) " +
        " end " +
        " commit tran ";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarID", DbType.Int32, Convert.ToInt32(CarID));
        db.AddInParameter(dbComm, "EquipID", DbType.Int32, Convert.ToInt32(EquipID));
        db.AddInParameter(dbComm, "EquipType", DbType.String, EquipType);
        db.AddInParameter(dbComm, "EquipExtraDetail", DbType.String, equipExtraDetail);


        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }


    #endregion



    #region Common Block (共用方法)

    /// <summary>
    /// 取得使用者名字
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public static string GetNameBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select name from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["name"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 取得使用者帳號
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public string GetAccountBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select Username from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["Username"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 取得使用者Email
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public string GetEmailBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select Email from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["Email"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 判斷是否帳號已經存在了
    /// </summary>
    /// <param name="_username"></param>
    /// <param name="_serno">空值或0代表新增</param>
    /// <returns></returns>
    public bool IsExistAccount(string _username, string _serno)
    {
        bool returnValue = true;

        if (!string.IsNullOrEmpty(_username))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select username from [funoperator] where [username]=@username";

            if (string.IsNullOrEmpty(_serno) || _serno == "0")
            {
                //新增
            }
            else
            {
                //修改
                sql += " and serno<>@serno";
            }

            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "username", DbType.String, _username);
            if (!string.IsNullOrEmpty(_serno)) db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count == 0)
            {
                returnValue = false;
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 是否為有效
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public bool IsValid(string _serno)
    {
        Database db = DatabaseFactory.CreateDatabase("ConnString");
        string sql = "select valid from [funoperator] where [serno]=@serno and valid=1";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        if (dt.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 驗證使用者
    /// </summary>
    /// <param name="_account"></param>
    /// <param name="_password"></param>
    /// <param name="_sysmsg"></param>
    /// <param name="_operator"></param>
    /// <returns></returns>
    public static bool VerifyUser(string _account, string _password, out string _sysmsg, out COperator _operator)
    {
        bool returnValue = false;
        _sysmsg = string.Empty;
        _operator = new COperator() { Serno = 0, Name = string.Empty };

        Database db = DBHelper.GetDatabase();
        string sql = "select * from funoperator where username=@username";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "username", DbType.String, _account);

        try
        {
            DataSet ds = db.ExecuteDataSet(dbComm);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count <= 0)
            {
                _sysmsg = "錯誤!!『帳號』不正確!!";
            }
            else
            {
                DataRow dr = dt.Rows[0];
                string pwd = ValidateHelper.DecryptString(dr["password"].ToString());

                if (_password != pwd.Trim())
                {
                    _sysmsg = "Error!! Password is not correct!!";
                }
                else
                {
                    if (dr["valid"].ToString() == "0")
                    {
                        _sysmsg = "注意!! 此帳號已被停用!!";
                    }
                    else
                    {
                        _operator.Serno = Convert.ToInt32(dr["Serno"].ToString());
                        _operator.Name = dr["Name"].ToString();
                        returnValue = true;
                    }
                }
            }
        }
        catch { }

        return returnValue;
    }
    #endregion

}
