﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Web;
using System.ComponentModel;
using System.Data.SqlClient;


public class Backend_DBEquip
{

    #region backend/Equip/list
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetList(string EquipCategoryID, int startrows, int maxrows)
    {

        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = "SELECT  ROW_NUMBER() OVER (ORDER BY e.[EquipID] desc) AS RowNum,  e.[EquipID]	 ,ec.[EquipCategoryName],e.[EquipCategoryID],e.[EquipName],e.[EquipProtectText],e.[Valid],e.[COrder]  FROM [Equip] e    left join [EquipCategory] ec  on e.EquipCategoryID=ec.EquipCategoryID ";


        if (!string.IsNullOrEmpty(EquipCategoryID))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " e.[EquipCategoryID] = @EquipCategoryID";
        }


        conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " e.Valid='1' ";

        sql += String.Format(@"SELECT *
                 FROM ({0})
                 AS NewTable
                 WHERE RowNum >= {1} AND RowNum <= {2}", conditionSQL, (startrows + 1).ToString(), (maxrows + startrows).ToString());

        sql += "  order by [COrder] ";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        if (!string.IsNullOrEmpty(EquipCategoryID)) db.AddInParameter(dbComm, "EquipCategoryID", DbType.String, Convert.ToString(EquipCategoryID));

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// 取得Select總筆數
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCounts(string EquipCategoryID)
    {
        Database db = DBHelper.GetDatabase();

        string conditionSQL = "  SELECT  Count(1) FROM [Equip] e where e.Valid='1' ";


        if (!string.IsNullOrEmpty(EquipCategoryID))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " e.[EquipCategoryID] = @EquipCategoryID";
        }

        conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " e.Valid='1' ";

        DbCommand dbComm = db.GetSqlStringCommand(conditionSQL);


        if (!string.IsNullOrEmpty(EquipCategoryID)) db.AddInParameter(dbComm, "EquipCategoryID", DbType.String, Convert.ToString(EquipCategoryID));


        DataTable dt = new DataTable();

        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return Convert.ToInt32(dt.Rows[0][0]);
    }

    /// <summary>
    /// 取得配備類別列表
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <param name="startrows"></param>
    /// <param name="maxrows"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetEquipCategoryList()
    {
        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = "SELECT  * FROM [EquipCategory] ";



        sql += conditionSQL;

        sql += " ORDER BY EquipCategoryID ";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// operatorlist.aspx 更新有效狀態
    /// </summary>
    /// <param name="serno"></param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void UpdateValid(int EquipId)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "update [Equip] set [Valid]=@Valid where EquipId=@EquipId";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "EquipId", DbType.Int32, EquipId);
        db.AddInParameter(dbComm, "Valid", DbType.Int32, 0);
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    public static void uploadToOnline()
    {
        DataTable dt = GetAllList();
        if (dt.Rows.Count > 0)
        {
            Database db = DBHelper.GetDatabase();
            string sql = " truncate table [Equip_online] ;";

            foreach (DataRow dr in dt.Rows)
            {
                string EquipID = dr["EquipID"].ToString();
                string EquipCategoryID = dr["EquipCategoryID"].ToString();
                string EquipName = dr["EquipName"].ToString();
                string EquipProtectText = dr["EquipProtectText"].ToString();
                string Valid = dr["Valid"].ToString();
                string COrder = dr["COrder"].ToString();

                sql += " insert into [Equip_online] ( [EquipID],[EquipCategoryID],[EquipName],[EquipProtectText],[Valid],[COrder]) values ( '" + EquipID + "','" + EquipCategoryID + "','" + EquipName + "','" + EquipProtectText + "','" + Valid + "','" + COrder + "') ;";

            }

            DbCommand dbComm = db.GetSqlStringCommand(sql);
            try
            {
                db.ExecuteNonQuery(dbComm);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }



    }

    public static DataTable GetAllList()
    {

        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = @" SELECT  e.[EquipID]	 ,e.[EquipCategoryID],e.[EquipName],e.[EquipProtectText],e.[Valid],e.[COrder] 
                                FROM [Equip] e where  e.valid='1'";


        sql += conditionSQL;

        DbCommand dbComm = db.GetSqlStringCommand(sql);



        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    #endregion
    #region backend/Equip/edit
    /// <summary>
    /// operatoredit.aspx 取得詳細資訊
    /// </summary>
    /// <param name="serno"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetListBySerno(string EquipID)
    {
        Database db = DBHelper.GetDatabase();

        string sql = @"select  [EquipID]
                      ,[EquipCategoryID]
                      ,[EquipName]
                      ,[EquipProtectText]
                      ,[Valid]
                      ,[COrder]
                  FROM [dbo].[Equip]";

        if (!string.IsNullOrEmpty(EquipID))
        {
            sql += DBHelper.GetSqlWhereOrAnd(sql) + " [EquipID]=@EquipID";
        }


        DbCommand dbComm = db.GetSqlStringCommand(sql);
        if (!string.IsNullOrEmpty(EquipID))
        {
            db.AddInParameter(dbComm, "EquipID", DbType.Int32, Convert.ToInt32(EquipID));
        }

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }


    /// <summary>
    /// 新增
    /// </summary>
    /// <param name="name"></param>
    /// CarName, CarSize, Wheelbase, Track, Luggage, Weight, Type, CC, Hp, Kgm, Compression, Stroke, Acceleration, ExtremeSpeed, Oil, FuelTank, Transfer, Transmission, Steering, Brake, Suspension, MinSlewing, AnnualFuel, NonUrbanFuel, UrbanFuel, TestValue, EfficiencyRating, CarTypeID, StockStatus, CarImg
    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static string Insert(string EquipName, string EquipProtectText, string EquipCategoryID)
    {
        //處理預設值不能為Null的問題       

        string returnId = "";

        Database db = DBHelper.GetDatabase();
        string sql = "insert into [Equip] ([EquipCategoryID],[EquipName],[EquipProtectText])"
                    + " values ( @EquipCategoryID,@EquipName,@EquipProtectText) "
                    + " SET @id= IDENT_CURRENT ('Equip')";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        db.AddInParameter(dbComm, "EquipName", DbType.String, EquipName);
        db.AddInParameter(dbComm, "EquipProtectText", DbType.String, EquipProtectText);
        db.AddInParameter(dbComm, "EquipCategoryID", DbType.String, EquipCategoryID);

        db.AddOutParameter(dbComm, "id", DbType.Int32, 8);
        try
        {
            db.ExecuteNonQuery(dbComm);
            returnId = db.GetParameterValue(dbComm, "id").ToString();

        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return returnId;
    }



    /// <summary>
    /// 修改 EquipName, EquipProtectText, EquipCategoryID, EquipID
    /// </summary>    
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void Update(string EquipName, string EquipProtectText, string EquipCategoryID, int EquipID)
    {

        Database db = DBHelper.GetDatabase();
        string sql = "update [equip] set [EquipName]=@EquipName,[EquipProtectText]=@EquipProtectText,[EquipCategoryID]=@EquipCategoryID " +
            " where EquipID=@EquipID ";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "EquipID", DbType.Int32, EquipID);
        db.AddInParameter(dbComm, "EquipName", DbType.String, EquipName);
        db.AddInParameter(dbComm, "EquipProtectText", DbType.String, EquipProtectText);
        db.AddInParameter(dbComm, "EquipCategoryID", DbType.String, EquipCategoryID);


        try
        {
            db.ExecuteNonQuery(dbComm);

        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    #endregion
    #region backend/Car/edit
    /// <summary>
    /// operatoredit.aspx 取得詳細資訊 
    /// </summary>
    /// <param name="serno"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetListBySerno(string CarId, string EquipCategorId)
    {
        Database db = DBHelper.GetDatabase();

        string sql = "SELECT e.*, ec.EquipCategoryName,ce.CarId as CarId, ce.Equipid as CarEquipId  , ce.EquipType as EquipType ,ce.EquipExtraDetail " +
            "FROM (select * FROM [dbo].[Equip] e where e.EquipCategoryid = @EquipCategorId AND e.valid='1' ) e " +
            "left join  EquipCategory ec  on   e.EquipCategoryID=ec.EquipCategoryID " +
            "left join  dbo.CarEquip  ce  on   e.Equipid=ce.Equipid  AND ce.CarId=@CarId ";


        DbCommand dbComm = db.GetSqlStringCommand(sql);
        if (!string.IsNullOrEmpty(CarId))
        {
            db.AddInParameter(dbComm, "CarId", DbType.Int32, Convert.ToInt32(CarId));
        }
        else
        {
            db.AddInParameter(dbComm, "CarId", DbType.Int32, -1);
        }
        if (!string.IsNullOrEmpty(EquipCategorId))
        {
            db.AddInParameter(dbComm, "EquipCategorId", DbType.Int32, Convert.ToInt32(EquipCategorId));
        }
        else
        {
            db.AddInParameter(dbComm, "EquipCategorId", DbType.Int32, Convert.ToInt32("1"));
        }

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    public static DataTable GetEquipTypeList()
    {
        Database db = DBHelper.GetDatabase();

        string sql = "SELECT * from EquipCategory";

        sql += DBHelper.GetSqlWhereOrAnd(sql) + " Valid = '1'";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// 修改
    /// </summary>
    ///
    public static void InsertOrUpdate(string CarID, string EquipID, string EquipType, string equipExtraDetail)
    {
        Database db = DBHelper.GetDatabase();
        string sql =
       " begin tran " +
        " if exists (select * from CarEquip  where CarID = @CarID and EquipID = @EquipID) " +
        " begin " +
         " update CarEquip set EquipType = @EquipType ,[equipExtraDetail]=@EquipExtraDetail" +
         " where CarID = @CarID and EquipID = @EquipID " +
        " end " +
        " else " +
        " begin " +
        " insert into CarEquip ([CarID],[EquipID],[EquipType],[equipExtraDetail]) " +
         " values (@CarID,@EquipID,@EquipType,@EquipExtraDetail) " +
        " end " +
        " commit tran ";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarID", DbType.Int32, Convert.ToInt32(CarID));
        db.AddInParameter(dbComm, "EquipID", DbType.Int32, Convert.ToInt32(EquipID));
        db.AddInParameter(dbComm, "EquipType", DbType.String, EquipType);
        db.AddInParameter(dbComm, "EquipExtraDetail", DbType.String, equipExtraDetail);


        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 修改
    /// </summary>
    ///
    public static void UpdateTempSelect(string Serno, string EquipID, string EquipType, string EquipTempImport, string EquipExtraDetail)
    {
        Database db = DBHelper.GetDatabase();

        EquipID = (string.IsNullOrEmpty(EquipID)) ? "0" : EquipID;

        string sql =
         " update EquipImportTemp set EquipType=@EquipType,EquipTempSelect = @EquipTempSelect , EquipTempImport=@EquipTempImport, EquipExtraDetail=@EquipExtraDetail ,IsUpdate=1 " +

         " where Serno = @Serno ";



        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "Serno", DbType.Int32, Convert.ToInt32(Serno));
        db.AddInParameter(dbComm, "EquipTempSelect", DbType.Int32, Convert.ToInt32(EquipID));
        db.AddInParameter(dbComm, "EquipType", DbType.String, EquipType);
        db.AddInParameter(dbComm, "EquipTempImport", DbType.String, EquipTempImport);
        db.AddInParameter(dbComm, "EquipExtraDetail", DbType.String, EquipExtraDetail);

        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 刪除
    /// </summary>
    /// <param name="serno"></param>
    public static void Delete(string EquipID, string CarID)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "delete from [CarEquip] where EquipID=@EquipID AND CarID = @CarID";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarID", DbType.Int32, Convert.ToInt32(CarID));
        db.AddInParameter(dbComm, "EquipID", DbType.Int32, Convert.ToInt32(EquipID));


        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 匯入整批刪除
    /// </summary>
    /// <param name="serno"></param>
    public static void DeleteFromBatch(string CarID)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "delete from [CarEquip] where  CarID = @CarID";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarID", DbType.Int32, Convert.ToInt32(CarID));

        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 取得Temp資料
    /// </summary>
    ///
    public static int CountEquipTempByCarID(string CarID)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "select Count(1)  from [EquipImportTemp] where CarID = @CarID";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarID", DbType.Int32, Convert.ToInt32(CarID));

        DataTable dt = new DataTable();

        try
        {
            db.ExecuteNonQuery(dbComm);
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return Convert.ToInt32(dt.Rows[0][0]);
    }

    /// <summary>
    /// 刪除Temp資料
    /// </summary>
    ///
    public static void DeleteEquipTempByCarID(string CarID)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "delete from [EquipImportTemp] where CarID = @CarID";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "CarID", DbType.Int32, Convert.ToInt32(CarID));

        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }


    /// <summary>
    /// 大量匯入資料
    /// </summary>
    ///
    public static string ImportEquipTemp(DataTable dt)
    {
        string errMsg = "";
        try
        {
            using (SqlConnection destinationConnection = new SqlConnection(Config.ConnString))
            {
                destinationConnection.Open();

                using (SqlTransaction transaction = destinationConnection.BeginTransaction())
                {
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destinationConnection, SqlBulkCopyOptions.KeepIdentity,
                               transaction))
                    {
                        bulkCopy.BatchSize = dt.Rows.Count;
                        bulkCopy.BulkCopyTimeout = 60;
                        bulkCopy.DestinationTableName = "dbo.EquipImportTemp";

                        //column對應
                        bulkCopy.ColumnMappings.Add("CarID", "CarID");
                        bulkCopy.ColumnMappings.Add("EquipType", "EquipType");
                        bulkCopy.ColumnMappings.Add("EquipName", "EquipName");
                        bulkCopy.ColumnMappings.Add("EquipTempSelect", "EquipTempSelect");
                        bulkCopy.ColumnMappings.Add("EquipTempImport", "EquipTempImport");
                        bulkCopy.ColumnMappings.Add("EquipExtraDetail", "EquipExtraDetail");
                        bulkCopy.ColumnMappings.Add("IsUpdate", "IsUpdate");

                        try
                        {
                            bulkCopy.WriteToServer(dt);
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            transaction.Rollback();
                            throw (ex);
                            errMsg = "資料錯誤，請重新匯入";
                        }
                        finally
                        {
                            destinationConnection.Close();
                        }
                    }
                }

                if (destinationConnection.State == ConnectionState.Open)
                {
                    destinationConnection.Close();
                }
            }
        }
        catch (Exception ex)
        {
            errMsg = "資料錯誤，請重新匯入";
            throw (ex);

        }

        return errMsg;
    }


    #endregion
    #region backend/Car/editequip
    /// <summary>
    /// operatoredit.aspx 取得詳細資訊 
    /// </summary>
    /// <param name="serno"></param>
    /// <returns></returns>
    public static DataTable GetEquipTempListByCarId(string CarId)
    {
        Database db = DBHelper.GetDatabase();

        string sql = @"   SELECT et.serno , et.CarID,ec.EquipCategoryID,e.EquipID, ec.EquipCategoryName +'_'+ e.EquipName as 'EquiplistName' , LTRIM(RTRIM(e.EquipName)) as 'EquipNameFormPM'
              , et.EquipType,et.EquipName,et.EquipTempSelect,et.EquipTempImport,et.EquipExtraDetail,et.IsUpdate from EquipImportTemp et 
    
               left join  Equip e
               on 
               Lower(REPLACE(REPLACE(REPLACE(CAST(et.EquipName as NVARCHAR(200)),CHAR(13),' '), CHAR(10), ' '),' ',''))=
			   Lower(REPLACE(REPLACE(REPLACE(CAST(e.EquipName as NVARCHAR(200)),CHAR(13),' '), CHAR(10), ' '),' ',''))

               left join  EquipCategory ec
               on ec.EquipCategoryID=e.EquipCategoryID

               where CarId=@CarId order by et.EquipName ";


        DbCommand dbComm = db.GetSqlStringCommand(sql);
        if (!string.IsNullOrEmpty(CarId))
        {
            db.AddInParameter(dbComm, "CarId", DbType.Int32, Convert.ToInt32(CarId));
        }
        else
        {
            db.AddInParameter(dbComm, "CarId", DbType.Int32, -1);
        }

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    #endregion

    #region Common Block (共用方法)

    /// <summary>
    /// 取得使用者名字
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public static string GetNameBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select name from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["name"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 取得使用者帳號
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public string GetAccountBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select Username from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["Username"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 取得使用者Email
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public string GetEmailBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select Email from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["Email"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 判斷是否帳號已經存在了
    /// </summary>
    /// <param name="_username"></param>
    /// <param name="_serno">空值或0代表新增</param>
    /// <returns></returns>
    public bool IsExistAccount(string _username, string _serno)
    {
        bool returnValue = true;

        if (!string.IsNullOrEmpty(_username))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select username from [funoperator] where [username]=@username";

            if (string.IsNullOrEmpty(_serno) || _serno == "0")
            {
                //新增
            }
            else
            {
                //修改
                sql += " and serno<>@serno";
            }

            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "username", DbType.String, _username);
            if (!string.IsNullOrEmpty(_serno)) db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count == 0)
            {
                returnValue = false;
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 是否為有效
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public bool IsValid(string _serno)
    {
        Database db = DatabaseFactory.CreateDatabase("ConnString");
        string sql = "select valid from [funoperator] where [serno]=@serno and valid=1";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        if (dt.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 驗證使用者
    /// </summary>
    /// <param name="_account"></param>
    /// <param name="_password"></param>
    /// <param name="_sysmsg"></param>
    /// <param name="_operator"></param>
    /// <returns></returns>
    public static bool VerifyUser(string _account, string _password, out string _sysmsg, out COperator _operator)
    {
        bool returnValue = false;
        _sysmsg = string.Empty;
        _operator = new COperator() { Serno = 0, Name = string.Empty };

        Database db = DBHelper.GetDatabase();
        string sql = "select * from funoperator where username=@username";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "username", DbType.String, _account);

        try
        {
            DataSet ds = db.ExecuteDataSet(dbComm);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count <= 0)
            {
                _sysmsg = "錯誤!!『帳號』不正確!!";
            }
            else
            {
                DataRow dr = dt.Rows[0];
                string pwd = ValidateHelper.DecryptString(dr["password"].ToString());

                if (_password != pwd.Trim())
                {
                    _sysmsg = "Error!! Password is not correct!!";
                }
                else
                {
                    if (dr["valid"].ToString() == "0")
                    {
                        _sysmsg = "注意!! 此帳號已被停用!!";
                    }
                    else
                    {
                        _operator.Serno = Convert.ToInt32(dr["Serno"].ToString());
                        _operator.Name = dr["Name"].ToString();
                        returnValue = true;
                    }
                }
            }
        }
        catch { }

        return returnValue;
    }
    #endregion

}