﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;

public class Backend_DBFunctionMenu
{
    /// <summary>
    /// 取得選單
    /// </summary>
    /// <param name="_serno">用戶流水號</param>
    /// <returns></returns>
    public static DataTable GetMain(int _serno)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "";

        sql += @"
            -- 6. 接著用預先建立好的選單 view (包含大類名稱、功能編號、功能名稱、程式path、有效否、顯示順序等等)過濾出只有目前 user 可用的所有功能
            SELECT 
	            * 
            FROM 
	            [v_menu_backend]
            WHERE 
	            [FunSerno] IN (
		            -- 5. 如以一來就可以看到：目前 user 可用的所有功能編號列表，已經變成rows了，沒有逗號了
		            SELECT 
			            [Serno] 
		            FROM 
			            [functiontable] AS [functiontable_1] 
		            WHERE 
			            -- 4. 接著比對功能表中的所有功能編號，若目前這個 [,編號,] 出現在 user 的功能編號串中，也就是如下的文字出現位置大於0的時候，才過濾這個功能編號出來
			            CHARINDEX(
				            -- 3. 這樣無論如何都可以找到類似 [,某功能編號,] 這樣的字串了，不會在前或後缺少逗點
				            (
					            ',' + 
					            CAST([Serno] as varchar(10)) + 
					            ','
				            ),
				            -- 2. 因為功能編號串的頭尾可能有逗號，所以不失一般性，還是在前後各加一個逗號
				            (
					            ',' + 
					            (
						            -- 1. 顯示左側選單的功能依據是，利用目前 user 的 session id 取得多筆功能編號串
						            -- 1. 由逗號分隔，且一個user可對應多個群組，所以可能會有類似 [2,1,3,4,5,2,] 這樣重複編號出現，但之後會過濾所以沒關係
						            -- 1. 若在 MYSQL 中會有 GROUP_CONCAT 輕易統整某個 key 下面的所有列，但在此不行
						            SELECT
							            [submenuname]
						            FROM
							            (
								            SELECT
									            SUBSTRING(
										            (',' + [UsrList] + ','),
										            CHARINDEX((',' + CAST(@oid as varchar(10)) + ','), (',' + [UsrList] + ',')),
										            LEN((',' + CAST(@oid as varchar(10)) + ','))
									            ) AS [current_user],
									            (
										            SELECT
											            [FunList] + ''
										            FROM
											            [funoperatorgroup]
										            AS
											            [INNER]
										            WHERE
											            SUBSTRING(
												            (',' + [INNER].[UsrList] + ','),
												            CHARINDEX((',' + CAST(@oid as varchar(10)) + ','), (',' + [INNER].[UsrList] + ',')),
												            LEN((',' + CAST(@oid as varchar(10)) + ','))
											            )
											            =
											            SUBSTRING(
												            (',' + [OUTER].[UsrList] + ','),
												            CHARINDEX((',' + CAST(@oid as varchar(10)) + ','), (',' + [OUTER].[UsrList] + ',')),
												            LEN((',' + CAST(@oid as varchar(10)) + ','))
											            )
										            FOR
											            XML PATH('')
									            ) AS [submenuname]
								            FROM
									            [funoperatorgroup]
								            AS
									            [OUTER]
								            WHERE
									                CHARINDEX((',' + CAST(@oid as varchar(10)) + ','), (',' + [UsrList] + ',')) > 0
									            AND [Valid] = 1
								            GROUP BY
									            SUBSTRING(
										            (',' + [UsrList] + ','),
										            CHARINDEX((',' + CAST(@oid as varchar(10)) + ','), (',' + [UsrList] + ',')),
										            LEN((',' + CAST(@oid as varchar(10)) + ','))
									            )
								            -- ;
							            )
						            AS
							            [FIN]
						            -- ;
					            ) + 
					            ','
				            )
			            ) > 0 
		            -- ;
	            )
                ORDER BY FunMenuCorder, FunMenuDetailCOrder
            ;
        ";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "oid", DbType.Int32, _serno);
        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        return dt;
    }    
}