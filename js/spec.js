﻿
var _DW,_lg=992,_isMobile=false;
var _CarID = ["39"];
var _Preview = "";
var _carDefaultData;
var _carTypeData;
var _carData;
var _carDefaultDataUrl;
var _carTypeDataUrl;
var _carCompareUrl;

//if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
//    _carDefaultDataUrl = "js/index_carImage.json";
//    _carTypeDataUrl = "js/index_carType.json";
//    _carCompareUrl = "js/carcomparison.json";
//}else {
//    _carDefaultDataUrl = "json/index_carImage.json";
//    _carTypeDataUrl = "json/index_carType.json";
//    _carCompareUrl = "api/carcomparison.ashx";
//}

_carDefaultDataUrl = "json/index_carImage.json";
_carTypeDataUrl = "json/index_carType.json";
_carCompareUrl = "api/carcomparison.ashx";

$(function () {
    init();
});

function init(){
    // 車款 : The polo
    var _urlCarID = getParam('CarID',true);
    var _urlPreview = getParam('preview', true);    
    if(_urlCarID) {
        _CarID = _urlCarID.split(',');
        _Preview = _urlPreview;
        getCarCompareData();
    }else{
        getCarCompareData();
    }
}

// popupCarImg
function popupCarImg(){
    $('.list__car-btn').on('click',function(){
        $('#popupImg .modal-body img').attr('src',$(this).attr('data-img'));
        $('#popupImg').modal('show');
    })
}

// createlist
function createList(){
    $('.list__content').empty();
    // list__heard
    for(var i=1;i<=1;i++){
        if((i-1)<_carData.Car.length) {
            $('.list__heard td:nth-child(' + (i) + ') .list__car-type').html(_carData.Car[i - 1].CarTypeName);
            $('.list__heard td:nth-child(' + (i) + ') .list__car-name').html(_carData.Car[i - 1].CarName);
            $('.list__heard td:nth-child(' + (i) + ') .list__car-btn').attr('data-img',_carData.Car[i - 1].CarImg);
            $('.list__heard td:nth-child(' + (i+1) + ')').show();
        }else{
            $('.list__heard td:nth-child(' + (i+1) + ')').hide();
        }
    }

    // list__type
    for(var i=0;i<_carData.Compare.length;i++) {

        var _html = $('.list__template').html();
        _html = findAndReplace(_html,'@@@@@@','C'+i);
        _html = findAndReplace(_html,'@@EquipCategoryName@@',_carData.Compare[i].EquipCategoryName);
        $('#list').append(_html);
        $('#list #C'+i+' .list__table tbody').empty();

        // list__td
        for(var ii=1;ii>0;ii--) {
            if((ii)>_CarID.length) {
                $('#list .C'+i+' .category td:nth-child('+ii+')').remove();
            }
        }

        // list__table
        for(var j=0;j<_carData.Compare[i].EquipNameList.length;j++) {
            var _thtml = $('.list__template .equip tbody').html().toString();

            _thtml = findAndReplace(_thtml,'@@EquipName@@',_carData.Compare[i].EquipNameList[j].EquipName);
            $('#list #C'+i+' .equip tbody').append(_thtml);

            // list__td
            var _EquipTypeTemp = _carData.Compare[i].EquipNameList[j].CarEquipList[0].EquipType;
            for(var k=1;k>0;k--) {
                if((k)<=_CarID.length) {
                    var _EquipType = _carData.Compare[i].EquipNameList[j].CarEquipList[(k-1)].EquipType;
                    if(_EquipTypeTemp!=_EquipType){
                        $('#list #C'+i+' .equip tr:last-child td:first-child').addClass('different');
                    }
                    // S:標配 E:選配 “”:無 C:技術規格
                    switch(_EquipType){
                        case 'S' :
                            // $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .fa').addClass('fa-circle');
                            $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .icon').html('●');
                            break;
                        case 'E' :
                            // $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .fa').addClass('fa-dot-circle-o');
                            $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .icon').html('○');
                            break;
                        case 'C' :
                            // $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .fa').addClass('fa-dot-circle-o');
                            $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .icon').remove();
                            break;
                        default:
                            // $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .fa').addClass('fa-minus');
                            $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .icon').html('-');
                    }
                    $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .EquipText').html(
                        _carData.Compare[i].EquipNameList[j].CarEquipList[(k-1)].EquipText
                    );
                }else{
                    var _td = k+1;
                    $('#list #C'+i+' .equip tr:last-child td:nth-child('+ _td +')').remove();
                }
            }
        }
    }

    //CarProtectText
    var _cHtml ='';
    _cHtml+= '<div class="text-left p-5" style="border-bottom: 1px solid rgba(0, 0, 0, 0.2);">';
    for (var l = 0; l < _carData.CarProtectText.length; l++) {
        _cHtml += _carData.CarProtectText[l].CarName +'<br>';
        _cHtml += '<div class="px-3">'+_carData.CarProtectText[l].CarProtectText +'</div><br>';
    }
    _cHtml+= '</div>';
    $('.list__content .list__type:last-child .collapse').append(_cHtml);

    // EquipProtextText
    var _lHtml='';
    _lHtml += '<ul class="text-left p-5">';
    for(var l=0;l<_carData.EquipProtextText.length;l++){
        _lHtml += '<li>';
        _lHtml += _carData.EquipProtextText[l].EquipProtectText;
        _lHtml += '</li>';
    }
    _lHtml += '<ul>';
    $('#list').append(_lHtml);


    $('#list').append('<a href="index.html" class="spec__btn">觀看其他車款比較</a>');

    $('#list .spec__btn').on('click',function(){
        ga('gtag_UA_63182225_47.send', 'event', 'btn', 'click', 'specGoToCarComparison');
    });

    //btns
    $('.tool__compare').fadeOut();
    $('.tool__detail').fadeOut();
    $('.tool__add').fadeOut();

    $('.tool__back').fadeIn();
    $('.tool__different').fadeIn();

    $('.list').fadeIn();
    $('.list').css('display','flex');
    $('#list .list__type:first-child .list__type-name').removeClass('collapsed');
    $('#list .list__type:first-child .collapse').addClass('show');
    // popupCarImg
    popupCarImg();
}

// DATA____________________________________________________
// getCarCompareData
function getCarCompareData() {
    $.ajax({
        url: _carCompareUrl,
        data: {
            CarID: _CarID.toString(),
            Preview: _Preview
        },
        type: "GET",
        dataType: "json",
        success: function(Jdata) {
            console.log('get '+ _carCompareUrl +' SUCCESS !!');
            _carData = Jdata;
            createList();
        },
        error: function() {
            alert("Get index_carType ERROR !!");
        }
    });
}

// TOOLS___________________________________________________
// URL parameter
function getParam(name, casesensitive) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    href = window.location.href;
    if (!casesensitive) name = name.toLowerCase();
    if (!casesensitive) href = href.toLowerCase();
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(href);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

// URL updateQueryStringParam
function updateQueryStringParam(key, value) {

    var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
        urlQueryString = document.location.search,
        newParam = key + '=' + value,
        params = '?' + newParam;

    // If the "search" string exists, then build params from it
    if (urlQueryString) {

        updateRegex = new RegExp('([\?&])' + key + '[^&]*');
        removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

        if( typeof value == 'undefined' || value == null || value == '' ) { // Remove param if value is empty

            params = urlQueryString.replace(removeRegex, "$1");
            params = params.replace( /[&;]$/, "" );

        } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it

            params = urlQueryString.replace(updateRegex, "$1" + newParam);

        } else { // Otherwise, add it to end of query string

            params = urlQueryString + '&' + newParam;

        }

    }
    window.history.replaceState({}, "", baseUrl + params);
};

// findAndReplace
function findAndReplace(string, target, replacement) {
    var i = 0, length = string.length;
    for (i; i < length; i++) {
        string = string.replace(target, replacement);
    }
    return string;
}