﻿var _DW, _lg = 992, _isMobile = false;
var _CarID = [""];
var _carDefaultData;
var _carTypeData;
var _carData;
var _carDefaultDataUrl;
var _carTypeDataUrl;
var _carCompareUrl;

if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
    _carDefaultDataUrl = "js/index_carImage.json";
    _carTypeDataUrl = "js/index_carType.json";
    _carCompareUrl = "js/carcomparison.json";
} else {
    _carDefaultDataUrl = "json/index_carImage.json";
    _carTypeDataUrl = "json/index_carType.json";
    _carCompareUrl = "api/carcomparison.ashx";
}

$(function () {

    // 車款 : The polo
    var _urlCarID = getParam('CarID', true);

    if (_urlCarID) {
        _CarID = _urlCarID.split(',');
        getCarTypeData();
    } else {
        getCarDefaultData();
    }

});

function init() {
    TweenMax.to($('.aniArr'), 0.3, {yoyo: true, repeat: -1, y: -10, ease: Power2.easeIn});
    // appendCar
    appendCar();

    // windowRisize
    $(window).on('resize', checkCarDiv);
    // checkCarDiv
    checkCarDiv();

    // addCarBtn
    addCarBtn();

    setCarType('#popupAdd');
    setCarName('#popupAdd', 0);

    $('.tool__add').on('click', function () {
        $('.tool__add-title span').css('z-index', 'auto');
    });
    $('.close').on('click', function () {
        $('.tool__add-title span').css('z-index', '99999');
    });
}

// appendCar
function appendCar() {
    // $('.area').fadeOut();

    console.log(_CarID);

    $('.area').empty();
    for (var i = 0; i < _CarID.length; i++) {
        // console.log(i,_CarID[i]);
        appendCarDiv(_CarID[i]);
    }
    checkCarDiv();
    // $('.area').fadeIn();
}

// appendCarDiv
function appendCarDiv(n) {
    var _carType;
    var _carTypeNum;
    var _carName;
    var _carNameNum;

    for (var j = 0; j < _carTypeData.length; j++) {
        for (var i = 0; i < _carTypeData[j].CarList.length; i++) {
            if (_carTypeData[j].CarList[i].CarID == n) {
                // console.log(_carTypeData[j].CarList[i].CarID);
                // console.log(_carTypeData[j].CarList[i].CarName);
                // console.log(_carTypeData[j].CarList[i].CarTypeName);
                _carType = _carTypeData[j].CarList[i].CarTypeName;
                _carTypeNum = j;
                _carName = _carTypeData[j].CarList[i].CarName;
                _carNameNum = i;
            }
        }
    }

    setCarType('.template');
    setCarName('.template', _carTypeNum);

    $('.area').append($('.template').html());
    $('.area .car:last-child .car__name').html(_carName);
    $('.area .car:last-child .car__type').html(_carType);
    $('.area .car:last-child').attr('style', 'background:#000 url(images/car/' + findAndReplace(_carType, ' ', '') + '.jpg) no-repeat center;background-size: 100% auto;');

    // console.log(_carTypeNum,_carNameNum);
    // console.log( $('.area .car:last-child .car__select-type').html());

    $('.area .car:last-child .car__select-type option[value=' + _carTypeNum + ']').attr("selected", true);
    $('.area .car:last-child .car__select-name option[value=' + n + ']').attr("selected", true);
}

// addCarBtn
function addCarBtn() {
    $('.car__select-submit').bind('click', function () {
        _CarID.push($('#popupAdd .car__select-name').val());
        $('#popupAdd').modal('hide');
        appendCar();
    })
}

// deleteCarBtn
function deleteCarBtn() {
    // console.log($(this).parent().index());
    _CarID.splice($(this).parent().index(), 1);
    appendCar();
}

// changeCarType
function changeCarType() {
    // console.log($(this).val());
    // console.log($(this).find("option:selected").text());

    var n = $(this).val();
    var _index = $(this).parent().parent().index();
    var _carType = $(this).find("option:selected").text();
    var _carName = _carTypeData[n].CarList[0].CarName;
    var _carNameNum = _carTypeData[n].CarList[0].CarID;


    $(this).parent().find('.car__select-name').empty();
    $(this).parent().parent().find('.car__type').html(_carType);
    $(this).parent().parent().find('.car__name').html(_carName);
    for (var i = 0; i < _carTypeData[n].CarList.length; i++) {
        var _option = '<option value="' + _carTypeData[n].CarList[i].CarID + '">' + _carTypeData[n].CarList[i].CarName + '</option>';
        $(this).parent().find('.car__select-name').append(_option);
    }

    if ($(this).attr('data-toggle') == 'add') {
        $(this).parent().parent().find('.car__img img').attr('src', 'images/car/' + findAndReplace(_carType, ' ', '') + '.jpg');
    } else {
        _CarID[_index] = _carNameNum;
        $(this).parent().parent().attr('style', 'background:#000 url(images/car/' + findAndReplace(_carType, ' ', '') + '.jpg) no-repeat center;background-size: 100% auto;');
    }

    // console.log(_CarID);
}

// changeCarName
function changeCarName() {
    var _carNameNum = $(this).val();
    var _carName = $(this).find("option:selected").text();
    var _index = $(this).parent().parent().index();

    $(this).parent().parent().find('.car__name').html(_carName);

    if ($(this).attr('data-toggle') != 'add') {
        _CarID[_index] = _carNameNum;
        updateQueryStringParam('CarID', _CarID);
    }
    // console.log(_CarID);
}

// checkCarDiv
function checkCarDiv() {

    _DW = window.innerWidth;
    var carTotal = $('.area .car').length;

    if (_DW < _lg) {
        _isMobile = true;
    } else {
        _isMobile = false;
    }
    if (_isMobile) {

        switch (carTotal) {
            case 1:
                $('.car__delete').fadeOut();
                $('.tool__compare').fadeOut();
                $('.tool__detail').fadeIn();
                $('.tool__add').fadeIn();
                break;
            case 2:
                $('.car__delete').fadeIn();
                $('.tool__compare').fadeIn();
                $('.tool__detail').fadeOut();
                $('.tool__add').fadeOut();
                break;
            case 3:
                _CarID.pop();
                $('.area .car:nth-child(3)').remove();
                $('.car__delete').fadeIn();
                $('.tool__compare').fadeIn();
                $('.tool__detail').fadeOut();
                $('.tool__add').fadeOut();
                break;
        }
    } else {
        switch (carTotal) {
            case 1:
                $('.car__delete').fadeOut();
                $('.tool__compare').fadeOut();
                $('.tool__detail').fadeIn();
                $('.tool__add').fadeIn();
                break;
            case 2:
                $('.car__delete').fadeIn();
                $('.tool__compare').fadeIn();
                $('.tool__detail').fadeOut();
                $('.tool__add').fadeIn();
                break;
            case 3:
                $('.car__delete').fadeIn();
                $('.tool__compare').fadeIn();
                $('.tool__detail').fadeOut();
                $('.tool__add').fadeOut();
                break;
        }
    }

    $('.tool__back').fadeOut();
    $('.tool__different').fadeOut();

    // deleteCarBtn
    $('.car__delete').unbind('click', deleteCarBtn);
    $('.car__delete').bind('click', deleteCarBtn);

    // changeCarType
    $('.car__select-type').unbind('change', changeCarType);
    $('.car__select-type').bind('change', changeCarType);

    // changeCarName
    $('.car__select-name').unbind('change', changeCarName);
    $('.car__select-name').bind('change', changeCarName);

    // compareCar
    $('.tool__compare').unbind('click', getCarCompareData);
    $('.tool__compare').bind('click', getCarCompareData);

    // detailCar
    $('.tool__detail').unbind('click', getCarCompareData);
    $('.tool__detail').bind('click', getCarCompareData);

    // addBtn
    $('.tool__add').on('click', function () {
        ga('gtag_UA_63182225_46.send', 'event', 'btn', 'click', 'add');
    });


    // backBtn
    $('.tool__back').on('click', function () {
        ga('gtag_UA_63182225_46.send', 'event', 'btn', 'click', 'back');
        $('.list').fadeOut();
        $('.tool').removeClass('tool--back');
        checkCarDiv();
    });

    // tool__different
    $('.tool__different').unbind('click');
    $('.tool__different').bind('click', function () {
        ga('gtag_UA_63182225_46.send', 'event', 'btn', 'click', 'different');
        $('#list').toggleClass("active");
    });

    // popupCarImg
    popupCarImg();

    // updateQueryStringParam
    updateQueryStringParam('CarID', _CarID);
}

// popupCarImg
function popupCarImg() {
    $('.list__car-btn').on('click', function () {
        $('#popupImg .modal-body img').attr('src', $(this).attr('data-img'));
        $('#popupImg').modal('show');
    })
}

// createlist
function createList() {
    $('.list__content').empty();
    // list__heard
    for (var i = 1; i <= 3; i++) {
        if ((i - 1) < _carData.Car.length) {
            $('.list__heard td:nth-child(' + (i + 1) + ') .list__car-type').html(_carData.Car[i - 1].CarTypeName);
            $('.list__heard td:nth-child(' + (i + 1) + ') .list__car-name').html(_carData.Car[i - 1].CarName);
            $('.list__heard td:nth-child(' + (i + 1) + ') .list__car-btn').attr('data-img', _carData.Car[i - 1].CarImg);
            if (_carData.Car[i - 1].CarImg == '') {
                $('.list__heard td:nth-child(' + (i + 1) + ') .list__car-btn').hide();
            } else {
                $('.list__heard td:nth-child(' + (i + 1) + ') .list__car-btn').show();
            }
            $('.list__heard td:nth-child(' + (i + 1) + ')').show();
        } else {
            $('.list__heard td:nth-child(' + (i + 1) + ')').hide();
        }
    }

    // list__type
    for (var i = 0; i < _carData.Compare.length; i++) {

        var _html = $('.list__template').html();
        _html = findAndReplace(_html, '@@@@@@', 'C' + i);
        _html = findAndReplace(_html, '@@EquipCategoryName@@', _carData.Compare[i].EquipCategoryName);
        $('#list').append(_html);
        $('#list #C' + i + ' .list__table tbody').empty();

        // list__td
        for (var ii = 3; ii > 0; ii--) {
            if ((ii) > _CarID.length) {
                $('#list .C' + i + ' .category td:nth-child(' + ii + ')').remove();
            }
        }

        // list__table
        for (var j = 0; j < _carData.Compare[i].EquipNameList.length; j++) {
            var _thtml = $('.list__template .equip tbody').html().toString();

            _thtml = findAndReplace(_thtml, '@@EquipName@@', _carData.Compare[i].EquipNameList[j].EquipName);
            $('#list #C' + i + ' .equip tbody').append(_thtml);

            // list__td
            var _EquipTypeTemp = _carData.Compare[i].EquipNameList[j].CarEquipList[0].EquipType;
            var _EquipTypeText = _carData.Compare[i].EquipNameList[j].CarEquipList[0].EquipText;
            _EquipTypeText = findAndReplace(_EquipTypeText, ' ', '');


            for (var k = 3; k > 0; k--) {
                if ((k) <= _CarID.length) {
                    var _EquipType = _carData.Compare[i].EquipNameList[j].CarEquipList[(k - 1)].EquipType;
                    var _EquipText = _carData.Compare[i].EquipNameList[j].CarEquipList[(k - 1)].EquipText;
                    _EquipText = findAndReplace(_EquipText, ' ', '');

                    if ((_EquipTypeTemp != _EquipType)||(_EquipTypeText != _EquipText)) {
                        $('#list #C' + i + ' .equip tr:last-child').addClass('different');
                    }
                    // S:標配 E:選配 “”:無 C:技術規格
                    switch (_EquipType) {
                        case 'S' :
                            // $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .fa').addClass('fa-circle');
                            $('#list #C' + i + ' .equip tr:last-child td:nth-child(' + (k + 1) + ') .icon').html('●');
                            break;
                        case 'E' :
                            // $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .fa').addClass('fa-dot-circle-o');
                            $('#list #C' + i + ' .equip tr:last-child td:nth-child(' + (k + 1) + ') .icon').html('○');
                            break;
                        case 'C' :
                            // $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .fa').addClass('fa-dot-circle-o');
                            $('#list #C' + i + ' .equip tr:last-child td:nth-child(' + (k + 1) + ') .icon').remove();
                            break;
                        default:
                            // $('#list #C'+i+' .equip tr:last-child td:nth-child('+(k+1)+') .fa').addClass('fa-minus');
                            $('#list #C' + i + ' .equip tr:last-child td:nth-child(' + (k + 1) + ') .icon').html('-');
                    }
                    $('#list #C' + i + ' .equip tr:last-child td:nth-child(' + (k + 1) + ') .EquipText').html(
                        _carData.Compare[i].EquipNameList[j].CarEquipList[(k - 1)].EquipText
                    );
                } else {

                    var _td = k + 1;

                    $('#list #C' + i + ' .equip tr:last-child td:nth-child(' + _td + ')').remove();
                }
            }
        }
    }

    //CarProtectText
    var _cHtml ='';
    _cHtml+= '<div class="text-left p-5" style="border-bottom: 1px solid rgba(255, 255, 255, 0.5);">';
    for (var l = 0; l < _carData.CarProtectText.length; l++) {
        _cHtml += _carData.CarProtectText[l].CarName +'<br>';
        _cHtml += '<div class="px-3">'+_carData.CarProtectText[l].CarProtectText +'</div><br>';
    }
    _cHtml+= '</div>';
    $('.list__content .list__type:last-child .collapse').append(_cHtml);

    // EquipProtextText
    var _lHtml = '';
    _lHtml += '<ul class="text-left p-5">';
    for (var l = 0; l < _carData.EquipProtextText.length; l++) {
        _lHtml += '<li>';
        _lHtml += _carData.EquipProtextText[l].EquipProtectText;
        _lHtml += '</li>';
    }
    _lHtml += '<ul>';
    $('#list').append(_lHtml);

    //btns
    $('.tool__compare').fadeOut();
    $('.tool__detail').fadeOut();
    $('.tool__add').fadeOut();

    $('.tool__back').fadeIn();
    $('.tool__different').fadeIn();

    $('.tool').addClass('tool--back');
    $('.list').fadeIn();
    $('.list').css('display', 'flex');
    // $('#list .list__type:first-child .list__type-name').removeClass('collapsed');
    //$('#list .list__type:first-child .collapse').addClass('show');
}

// setCarType
function setCarType(div) {
    // console.log(_carTypeData);
    // console.log(_carTypeData.length,_carTypeData[0].CarTypeName);

    var _carType = _carTypeData[0].CarTypeName;

    $(div + ' .car__select-type').empty();
    for (var i = 0; i < _carTypeData.length; i++) {
        var _option = '<option value="' + i + '">' + _carTypeData[i].CarTypeName + '</option>';
        $(div + ' .car__select-type').append(_option);
    }

    if (div == '#popupAdd') {
        $(div + ' .car__img img').attr('src', 'images/car/' + findAndReplace(_carType, ' ', '') + '.jpg');
        $(div + ' .car__type').html(_carType);
    }
}

// setCarName
function setCarName(div, n) {
    // console.log(_carTypeData[n].CarList.length,_carTypeData[n].CarList);
    var _carName = _carTypeData[n].CarList[0].CarName;

    $(div + ' .car__select-name').empty();
    for (var i = 0; i < _carTypeData[n].CarList.length; i++) {
        var _option = '<option value="' + _carTypeData[n].CarList[i].CarID + '">' + _carTypeData[n].CarList[i].CarName + '</option>';
        $(div + ' .car__select-name').append(_option);
    }
    if (div == '#popupAdd') {
        $(div + ' .car__name').html(_carName);
    }
}

// DATA____________________________________________________
// getCarDefaultData
function getCarDefaultData() {
    $.ajax({
        url: _carDefaultDataUrl,
        type: "GET",
        dataType: "json",
        success: function (Jdata) {
            console.log('get ' + _carDefaultDataUrl + ' SUCCESS !!');
            _carDefaultData = Jdata;
            for (var i = 0; i < _carDefaultData.length; i++) {
                _CarID[i] = _carDefaultData[i].CarID;
            }
            getCarTypeData();
        },
        error: function () {
            alert("Get index_carType ERROR !!");
        }
    });
}

// getCarTypeData
function getCarTypeData() {
    $.ajax({
        url: _carTypeDataUrl,
        type: "GET",
        dataType: "json",
        success: function (Jdata) {
            console.log('get ' + _carTypeDataUrl + ' SUCCESS !!');
            _carTypeData = Jdata;
            init();
        },

        error: function () {
            alert("Get index_carType ERROR !!");
        }
    });
}

// getCarCompareData
function getCarCompareData() {
    $.ajax({
        url: _carCompareUrl,
        data: {
            CarID: _CarID.toString()
        },
        type: "GET",
        dataType: "json",
        success: function (Jdata) {
            console.log('get ' + _carCompareUrl + ' SUCCESS !!');
            _carData = Jdata;
            ga('gtag_UA_63182225_46.send', 'event', 'btn', 'CarCompare', _CarID.toString());
            createList();
        },
        error: function () {
            alert("Get index_carType ERROR !!");
        }
    });
}

// TOOLS___________________________________________________
// URL parameter
function getParam(name, casesensitive) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    href = window.location.href;
    if (!casesensitive) name = name.toLowerCase();
    if (!casesensitive) href = href.toLowerCase();
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(href);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

// URL updateQueryStringParam
function updateQueryStringParam(key, value) {

    var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
        urlQueryString = document.location.search,
        newParam = key + '=' + value,
        params = '?' + newParam;

    // If the "search" string exists, then build params from it
    if (urlQueryString) {

        updateRegex = new RegExp('([\?&])' + key + '[^&]*');
        removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

        if (typeof value == 'undefined' || value == null || value == '') { // Remove param if value is empty

            params = urlQueryString.replace(removeRegex, "$1");
            params = params.replace(/[&;]$/, "");

        } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it

            params = urlQueryString.replace(updateRegex, "$1" + newParam);

        } else { // Otherwise, add it to end of query string

            params = urlQueryString + '&' + newParam;

        }

    }
    window.history.replaceState({}, "", baseUrl + params);
};

// findAndReplace
function findAndReplace(string, target, replacement) {
    var i = 0, length = string.length;
    for (i; i < length; i++) {
        string = string.replace(target, replacement);
    }
    return string;
}